-- STEP 5 OF 5: Extraction of events of interest for the cohort,
-- each event being associated with a concept set, a visit occurrence
-- (if available) and a meta-visit occurrence (if available)

SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.events_of_interest'

CREATE TEMPORARY TABLE _events_of_interest
  (
             person_id BIGINT NOT NULL,
            event_type TEXT NOT NULL,
          event_period DATERANGE NOT NULL,
      concept_set_name TEXT,
           concept_ids INTEGER[] NOT NULL,
   visit_occurrence_id BIGINT,
           provider_id BIGINT,
       event_source_id BIGINT
  );

\echo '  importing `condition_occurrence` events'

INSERT INTO _events_of_interest
     SELECT DISTINCT
            cohort.person_id,
            'condition_occurrence'::TEXT,
            codex_period(
              event.condition_start_date,
              event.condition_end_date),
            concept_set.concept_set_name,
            array_distinct(array_remove(ARRAY[
              nullif(event.condition_type_concept_id, 0),
              nullif(event.condition_concept_id, 0),
              nullif(event.condition_source_concept_id, 0)
              ]::INTEGER[], NULL)),
            event.visit_occurrence_id,
            event.provider_id,
            event.condition_occurrence_id
       FROM {{cdm_schema}}.condition_occurrence AS event
       JOIN cohort_of_interest AS cohort USING (person_id)
  LEFT JOIN concept_sets_of_interest AS concept_set
         ON (
                (concept_set.concept_id = event.condition_concept_id)
             OR (concept_set.concept_id = event.condition_source_concept_id)
            );

\echo '  importing `drug_exposure` events'

INSERT INTO _events_of_interest
     SELECT DISTINCT
            cohort.person_id,
            'drug_exposure'::TEXT,
            codex_period(
              event.drug_exposure_start_date,
              coalesce(
                event.drug_exposure_end_date,
                event.drug_exposure_start_date
                  + abs(coalesce(event.days_supply, 1)))),
                  -- * (abs(coalesce(event.refills, 0)) + 1))
            concept_set.concept_set_name,
            array_distinct(array_remove(ARRAY[
              nullif(event.drug_type_concept_id, 0),
              nullif(event.drug_concept_id, 0)
              ]::INTEGER[], NULL)),
            event.visit_occurrence_id,
            event.provider_id,
            event.drug_exposure_id
       FROM {{cdm_schema}}.drug_exposure AS event
       JOIN cohort_of_interest AS cohort USING (person_id)
  LEFT JOIN concept_sets_of_interest AS concept_set
         ON (concept_set.concept_id = event.drug_concept_id);
  -- Note: if not provided, the drug exposure end date is estimated
  -- by adding days of supply (minimum one) to the start date
  -- Note: for some reason `day_supply` and `refills` sometimes have
  -- negative values; we correct for this by taking the absolute value

\echo '  importing `procedure_occurrence` events'

INSERT INTO _events_of_interest
     SELECT DISTINCT
            cohort.person_id,
            'procedure_occurrence'::TEXT,
            codex_instant(event.procedure_date),
            concept_set.concept_set_name,
            array_distinct(array_remove(ARRAY[
              nullif(event.procedure_type_concept_id, 0),
              nullif(event.procedure_concept_id, 0),
              nullif(event.procedure_source_concept_id, 0)
              ]::INTEGER[], NULL)),
            event.visit_occurrence_id,
            event.provider_id,
            event.procedure_occurrence_id
       FROM {{cdm_schema}}.procedure_occurrence AS event
       JOIN cohort_of_interest AS cohort USING (person_id)
  LEFT JOIN concept_sets_of_interest AS concept_set
         ON (
                (concept_set.concept_id = event.procedure_concept_id)
             OR (concept_set.concept_id = event.procedure_source_concept_id)
            );

\echo '  importing `observation` events'

INSERT INTO _events_of_interest
     SELECT DISTINCT
            cohort.person_id,
            'observation'::TEXT,
            codex_instant(event.observation_date),
            concept_set.concept_set_name,
            array_distinct(array_remove(ARRAY[
              nullif(event.observation_type_concept_id, 0),
              nullif(event.observation_concept_id, 0),
              nullif(event.observation_source_concept_id, 0)
              ]::INTEGER[], NULL)),
            event.visit_occurrence_id,
            event.provider_id,
            event.observation_id
       FROM {{cdm_schema}}.observation AS event
       JOIN cohort_of_interest AS cohort USING (person_id)
  LEFT JOIN concept_sets_of_interest AS concept_set
         ON (
                (concept_set.concept_id = event.observation_concept_id)
             OR (concept_set.concept_id = event.observation_source_concept_id)
            );

\echo '  importing `measurement` events'

INSERT INTO _events_of_interest
     SELECT DISTINCT
            cohort.person_id,
            'measurement'::TEXT,
            codex_instant(event.measurement_date),
            concept_set.concept_set_name,
            array_distinct(array_remove(ARRAY[
              nullif(event.measurement_type_concept_id, 0),
              nullif(event.measurement_concept_id, 0),
              nullif(event.measurement_source_concept_id, 0)
              ]::INTEGER[], NULL)),
            event.visit_occurrence_id,
            event.provider_id,
            event.measurement_id
       FROM {{cdm_schema}}.measurement AS event
       JOIN cohort_of_interest AS cohort USING (person_id)
  LEFT JOIN concept_sets_of_interest AS concept_set
         ON (
                (concept_set.concept_id = event.measurement_concept_id)
             OR (concept_set.concept_id = event.measurement_source_concept_id)
            );

\echo '  importing `device_exposure` events'

INSERT INTO _events_of_interest
     SELECT DISTINCT
            cohort.person_id,
            'device_exposure'::TEXT,
            codex_period(
              event.device_exposure_start_date,
              event.device_exposure_end_date),
            concept_set.concept_set_name,
            array_distinct(array_remove(ARRAY[
              nullif(event.device_type_concept_id, 0),
              nullif(event.device_concept_id, 0),
              nullif(event.device_source_concept_id, 0)
              ]::INTEGER[], NULL)),
            event.visit_occurrence_id,
            event.provider_id,
            event.device_exposure_id
       FROM {{cdm_schema}}.device_exposure AS event
       JOIN cohort_of_interest AS cohort USING (person_id)
  LEFT JOIN concept_sets_of_interest AS concept_set
         ON (
                (concept_set.concept_id = event.device_concept_id)
             OR (concept_set.concept_id = event.device_source_concept_id)
            );

\echo '  linking events to metavisits'

CREATE INDEX ON _events_of_interest (visit_occurrence_id);

DROP TABLE IF EXISTS events_of_interest CASCADE;
CREATE TABLE events_of_interest AS
  WITH
    _metavisit_to_visits AS (
       SELECT metavisit_occurrence_id,
              unnest(visit_occurrence_ids
                ) AS visit_occurrence_id
         FROM metavisits_of_interest
      )
  SELECT event.person_id,
         event.event_type,
         event.event_period,
         event.concept_set_name,
         event.concept_ids,
         event.visit_occurrence_id,
         metavisit.metavisit_occurrence_id,
         event.provider_id,
         event.event_source_id
    FROM _events_of_interest AS event
    JOIN _metavisit_to_visits AS metavisit
   USING (visit_occurrence_id);

COMMENT ON TABLE events_of_interest IS '{{pipeline_version}}';

/******************************************************************************/

\echo '  importing drug eras'

\echo '    staging drug exposures'

CREATE TEMPORARY TABLE _cte_pre_drug_target AS
  -- retrieve drug_exposure start and end dates for any prescribed
  -- drug, cross-joining it with any ingredient(s) of that drug
  SELECT drug_exposure.drug_exposure_id,
         drug_exposure.person_id,
         concept.concept_id AS ingredient_concept_id,
         drug_exposure.drug_exposure_start_date,
         drug_exposure.days_supply,
         coalesce(
           drug_exposure.drug_exposure_end_date,
           drug_exposure.drug_exposure_start_date
             + abs(coalesce(drug_exposure.days_supply, 1))
           ) AS drug_exposure_end_date
    FROM {{cdm_schema}}.drug_exposure
    JOIN cohort_of_interest USING (person_id)
    JOIN {{cdm_schema}}.concept_ancestor
      ON (concept_ancestor.descendant_concept_id = drug_exposure.drug_concept_id)
    JOIN {{cdm_schema}}.concept
      ON (concept_ancestor.ancestor_concept_id = concept.concept_id)
   WHERE (concept.vocabulary_id = 'RxNorm')
     AND (concept.concept_class_id = 'Ingredient')
     AND (drug_exposure.drug_concept_id != 0)
     AND (drug_exposure.days_supply >= 0);

\echo '    importing stockpiling drug eras'
-- adapted from https://gist.github.com/taylordelehanty/41742e2eb7357099b66b

CREATE TEMPORARY TABLE _drug_eras_stockpiling AS
  WITH
    _cte_drug_target AS (
       SELECT drug_exposure_id,
              person_id,
              ingredient_concept_id,
              drug_exposure_start_date,
              days_supply,
              drug_exposure_end_date,
              (drug_exposure_end_date -
               drug_exposure_start_date
                ) AS days_of_exposure
         FROM _cte_pre_drug_target
      ),
    -- here be dragons
    _cte_end_dates AS (
       SELECT person_id,
              ingredient_concept_id,
              (event_date - 30) AS end_date
         FROM (
               SELECT person_id,
                      ingredient_concept_id,
                      event_date,
                      event_type,
                      max(start_ordinal) OVER (
                        PARTITION BY person_id, ingredient_concept_id
                            ORDER BY event_date, event_type
                                ROWS UNBOUNDED PRECEDING
                        ) AS start_ordinal,
                      row_number() OVER (
                        PARTITION BY person_id, ingredient_concept_id
                            ORDER BY event_date, event_type
                        ) AS overall_ord
                 FROM (
                          SELECT person_id,
                                 ingredient_concept_id,
                                 drug_exposure_start_date AS event_date,
                                 -1 AS event_type,
                                 row_number() OVER (
                                    PARTITION BY person_id, ingredient_concept_id
                                        ORDER BY drug_exposure_start_date
                                   ) AS start_ordinal
                            FROM _cte_drug_target
                       UNION ALL
                          SELECT person_id,
                                 ingredient_concept_id,
                                 drug_exposure_end_date + 30,
                                 1 AS event_type,
                                 NULL
                            FROM _cte_drug_target
                      ) AS rawdata
              ) AS e
        WHERE ((2 * e.start_ordinal) - e.overall_ord = 0)
      ),
    _cte_drug_exposure_ends AS (
         SELECT dt.person_id,
                dt.ingredient_concept_id AS drug_concept_id,
                dt.drug_exposure_start_date,
                min(e.end_date) AS drug_era_end_date,
                dt.days_of_exposure AS days_of_exposure
           FROM _cte_drug_target AS dt
           JOIN _cte_end_dates AS e
             ON (
                     (dt.person_id = e.person_id)
                 AND (dt.ingredient_concept_id = e.ingredient_concept_id)
                 AND (e.end_date >= dt.drug_exposure_start_date)
                )
       GROUP BY dt.drug_exposure_id,
                dt.person_id,
                dt.ingredient_concept_id,
                dt.drug_exposure_start_date,
                dt.days_of_exposure
      )
    SELECT person_id,
           drug_concept_id,
           min(drug_exposure_start_date) AS drug_era_start_date,
           drug_era_end_date,
           count(*) AS drug_exposure_count,
           (drug_era_end_date
             - min(drug_exposure_start_date)
             - sum(days_of_exposure)
             ) AS gap_days
      FROM _cte_drug_exposure_ends
  GROUP BY person_id, drug_concept_id, drug_era_end_date
  ORDER BY person_id, drug_concept_id;

CREATE INDEX ON _drug_eras_stockpiling (person_id);
CREATE INDEX ON _drug_eras_stockpiling (drug_concept_id);

INSERT INTO events_of_interest
     SELECT drug_era.person_id,
            'drug_era_stockpiling'::TEXT,
            codex_period(
              drug_era.drug_era_start_date,
              drug_era.drug_era_end_date
              ) AS event_period,
            concept_set.concept_set_name,
            array_remove(ARRAY[
              nullif(drug_era.drug_concept_id, 0)
              ]::INTEGER[], NULL),
            NULL,
            NULL,
            NULL,
            NULL
       FROM _drug_eras_stockpiling AS drug_era
  LEFT JOIN concept_sets_of_interest AS concept_set
         ON (concept_set.concept_id = drug_era.drug_concept_id);

\echo '    importing non-stockpiling drug eras'
-- adapted from https://gist.github.com/taylordelehanty/0f01e7d32dfe6faa27d1

CREATE TEMPORARY TABLE _drug_eras_nostockpiling AS
  WITH
    -- A preliminary sorting that groups all of the overlapping exposures
    -- into one exposure so that we don't double-count non-gap-days
    _cte_sub_exposure_end_dates AS (
       SELECT person_id,
              ingredient_concept_id,
              event_date AS end_date
         FROM (
                SELECT person_id,
                       ingredient_concept_id,
                       event_date,
                       event_type,
                       max(start_ordinal) OVER (
                         PARTITION BY person_id, ingredient_concept_id
                             ORDER BY event_date, event_type
                                 ROWS UNBOUNDED PRECEDING
                         ) AS start_ordinal,
                       row_number() OVER (
                         PARTITION BY person_id, ingredient_concept_id
                             ORDER BY event_date, event_type
                         ) AS overall_ord
                  FROM (
                           SELECT person_id,
                                  ingredient_concept_id,
                                  drug_exposure_start_date AS event_date,
                                  -1 AS event_type,
                                  row_number() OVER (
                                    PARTITION BY person_id, ingredient_concept_id
                                        ORDER BY drug_exposure_start_date
                                    ) AS start_ordinal
                             FROM _cte_pre_drug_target
                        UNION ALL
                           SELECT person_id,
                                  ingredient_concept_id,
                                  drug_exposure_end_date,
                                  1 AS event_type,
                                  NULL
                             FROM _cte_pre_drug_target
                       ) AS rawdata
              ) AS e
        WHERE ((2 * e.start_ordinal) - e.overall_ord = 0)
      ),
    _cte_drug_exposure_ends AS (
         SELECT dt.person_id,
                dt.ingredient_concept_id AS drug_concept_id,
                dt.drug_exposure_start_date,
                min(e.end_date) AS drug_sub_exposure_end_date
           FROM _cte_pre_drug_target AS dt
           JOIN _cte_sub_exposure_end_dates AS e
             ON (dt.person_id = e.person_id)
            AND (dt.ingredient_concept_id = e.ingredient_concept_id)
            AND (e.end_date >= dt.drug_exposure_start_date)
       GROUP BY dt.drug_exposure_id,
                dt.person_id,
                dt.ingredient_concept_id,
                dt.drug_exposure_start_date
      ),
    _cte_sub_exposures AS (
        SELECT row_number() OVER (
                 PARTITION BY person_id, drug_concept_id, drug_sub_exposure_end_date
                 ) AS row_number,
               person_id,
               drug_concept_id,
               min(drug_exposure_start_date) AS drug_sub_exposure_start_date,
               drug_sub_exposure_end_date,
               count(*) AS drug_exposure_count
          FROM _cte_drug_exposure_ends
      GROUP BY person_id,
               drug_concept_id,
               drug_sub_exposure_end_date
      ORDER BY person_id,
               drug_concept_id
      ),
    _cte_final_target AS (
       SELECT row_number,
              person_id,
              drug_concept_id AS ingredient_concept_id,
              drug_sub_exposure_start_date,
              drug_sub_exposure_end_date,
              drug_exposure_count,
              (drug_sub_exposure_end_date -
               drug_sub_exposure_start_date) AS days_exposed
         FROM _cte_sub_exposures
      ),
    _cte_end_dates AS (
        SELECT person_id,
               ingredient_concept_id,
               (event_date - 30) AS end_date
          FROM (
                SELECT person_id,
                       ingredient_concept_id,
                       event_date,
                       event_type,
                       max(start_ordinal) OVER (
                         PARTITION BY person_id, ingredient_concept_id
                             ORDER BY event_date, event_type
                                 ROWS UNBOUNDED PRECEDING
                         ) AS start_ordinal,
                       row_number() OVER (
                         PARTITION BY person_id, ingredient_concept_id
                             ORDER BY event_date, event_type
                         ) AS overall_ord
                  FROM (
                           SELECT person_id,
                                  ingredient_concept_id,
                                  drug_sub_exposure_start_date AS event_date,
                                  -1 AS event_type,
                                  row_number() OVER (
                                    PARTITION BY person_id, ingredient_concept_id
                                        ORDER BY drug_sub_exposure_start_date
                                    ) AS start_ordinal
                             FROM _cte_final_target
                        UNION ALL
                           SELECT person_id,
                                  ingredient_concept_id,
                                  drug_sub_exposure_end_date + 30,
                                  1 AS event_type,
                                  NULL
                             FROM _cte_final_target
                       ) AS rawdata
               ) AS e
         WHERE ((2 * e.start_ordinal) - e.overall_ord = 0)
      ),
    _cte_drug_era_ends AS (
         SELECT ft.person_id,
                ft.ingredient_concept_id AS drug_concept_id,
                ft.drug_sub_exposure_start_date,
                min(e.end_date) AS drug_era_end_date,
                drug_exposure_count,
                days_exposed
           FROM _cte_final_target AS ft
           JOIN _cte_end_dates AS e
             ON (ft.person_id = e.person_id)
            AND (ft.ingredient_concept_id = e.ingredient_concept_id)
            AND (e.end_date >= ft.drug_sub_exposure_start_date)
       GROUP BY ft.person_id,
                ft.ingredient_concept_id,
                ft.drug_sub_exposure_start_date,
                drug_exposure_count,
                days_exposed
      )
    SELECT person_id,
           drug_concept_id,
           min(drug_sub_exposure_start_date) AS drug_era_start_date,
           drug_era_end_date,
           sum(drug_exposure_count) AS drug_exposure_count,
           (drug_era_end_date
             - min(drug_sub_exposure_start_date)
             - sum(days_exposed)
             ) gap_days
      FROM _cte_drug_era_ends
  GROUP BY person_id, drug_concept_id, drug_era_end_date
  ORDER BY person_id, drug_concept_id;

CREATE INDEX ON _drug_eras_nostockpiling (person_id);
CREATE INDEX ON _drug_eras_nostockpiling (drug_concept_id);

INSERT INTO events_of_interest
     SELECT drug_era.person_id,
            'drug_era_nostockpiling'::TEXT,
            codex_period(
              drug_era.drug_era_start_date,
              drug_era.drug_era_end_date
              ) AS event_period,
            concept_set.concept_set_name,
            array_remove(ARRAY[
              nullif(drug_era.drug_concept_id, 0)
              ]::INTEGER[], NULL),
            NULL,
            NULL,
            NULL,
            NULL
       FROM _drug_eras_nostockpiling AS drug_era
  LEFT JOIN concept_sets_of_interest AS concept_set
         ON (concept_set.concept_id = drug_era.drug_concept_id);

-- note: the `gap_days` field in `_drug_eras_stockpiling` can have negative
-- values per its definition as seen here: `http://forums.ohdsi.org/t/
-- where-does-gap-days-come-from-when-building-cdm-v5-drug-era/661/12`

/******************************************************************************/

\echo '  indexing events'

CREATE INDEX ON events_of_interest (person_id);
CREATE INDEX ON events_of_interest (event_type);
CREATE INDEX ON events_of_interest USING GIST (event_period);
CREATE INDEX ON events_of_interest (concept_set_name);
CREATE INDEX ON events_of_interest (concept_set_name varchar_pattern_ops);
CREATE INDEX ON events_of_interest USING GIN (concept_ids);
CREATE INDEX ON events_of_interest (visit_occurrence_id);
CREATE INDEX ON events_of_interest (metavisit_occurrence_id);
CREATE INDEX ON events_of_interest (provider_id);
CREATE INDEX ON events_of_interest (event_source_id);

ANALYZE events_of_interest;
