-- STEP 4 OF 5: Extraction of visits and metavisits for the cohort

SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.visits_of_interest'

\echo '  listing visits'

DROP TABLE IF EXISTS visits_of_interest CASCADE;
CREATE TABLE visits_of_interest AS
  SELECT visit.visit_occurrence_id,
         visit.person_id,
         codex_period(
           visit.visit_start_date,
           visit.visit_end_date
           ) AS visit_period,
         (
          CASE visit.visit_concept_id
            WHEN 9201 THEN 'inpatient'   -- 'Inpatient Visit'
            WHEN 9202 THEN 'outpatient'  -- 'Outpatient Visit'
            WHEN 9203 THEN 'emergency'   -- 'Emergency Room Visit'
          END
         ) AS visit_type
    FROM {{cdm_schema}}.visit_occurrence AS visit
    JOIN cohort_of_interest AS cohort
   USING (person_id);

COMMENT ON TABLE visits_of_interest IS '{{pipeline_version}}';

\echo '  indexing visits'

ALTER TABLE visits_of_interest
  ADD PRIMARY KEY (visit_occurrence_id);

CREATE INDEX ON visits_of_interest (person_id);
CREATE INDEX ON visits_of_interest USING BTREE (visit_period);
CREATE INDEX ON visits_of_interest USING GIST (visit_period);
CREATE INDEX ON visits_of_interest (visit_type);

ANALYZE visits_of_interest;

\echo '* creating {{work_schema}}.metavisits_of_interest'

\echo '  staging metavisits'

CREATE TEMPORARY TABLE _metavisits_of_interest AS
    SELECT person_id,
           unnest(
             codex_period_union(
               array_agg(visit_period))
             ) AS metavisit_period
      FROM visits_of_interest
  GROUP BY person_id;

CREATE INDEX ON _metavisits_of_interest (person_id);
CREATE INDEX ON _metavisits_of_interest USING BTREE (metavisit_period);
CREATE INDEX ON _metavisits_of_interest USING GIST (metavisit_period);

\echo '  listing metavisits'

DROP TABLE IF EXISTS metavisits_of_interest CASCADE;
CREATE TABLE metavisits_of_interest AS
  SELECT array_min(visit_occurrence_ids) AS metavisit_occurrence_id, *
    FROM (
            SELECT metavisit.*,
                   array_agg(DISTINCT visit.visit_type
                     ) AS visit_types,
                   array_agg(DISTINCT visit.visit_occurrence_id
                     ) AS visit_occurrence_ids
              FROM visits_of_interest AS visit,
                   _metavisits_of_interest AS metavisit
             WHERE (visit.person_id = metavisit.person_id)
               AND (visit.visit_period && metavisit.metavisit_period)
          GROUP BY metavisit.person_id,
                   metavisit.metavisit_period
         ) AS metavisit_of_interest;

COMMENT ON TABLE metavisits_of_interest IS '{{pipeline_version}}';

\echo '  indexing metavisits'

ALTER TABLE metavisits_of_interest
  ADD PRIMARY KEY (metavisit_occurrence_id);

CREATE INDEX ON metavisits_of_interest (person_id);
CREATE INDEX ON metavisits_of_interest USING BTREE (metavisit_period);
CREATE INDEX ON metavisits_of_interest USING GIST (metavisit_period);
--CREATE INDEX ON metavisits_of_interest USING GIN (visit_types);
CREATE INDEX ON metavisits_of_interest USING GIN (visit_occurrence_ids);

ANALYZE metavisits_of_interest;
