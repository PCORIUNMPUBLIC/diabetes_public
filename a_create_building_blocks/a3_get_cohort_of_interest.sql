-- STEP 3 OF 5: Declaration of a cohort of interest for the study

SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.cohort_of_interest'

DROP TABLE IF EXISTS cohort_of_interest CASCADE;
CREATE TABLE cohort_of_interest AS
  SELECT person.person_id,
         to_date(
           concat_ws('-',
             person.year_of_birth,
             lpad(coalesce(person.month_of_birth, 1)::text, 2, '0'),
             lpad(coalesce(person.day_of_birth, 1)::text, 2, '0')),
           'YYYY-MM-DD'
           ) AS birth_date,
         (
          CASE person.gender_concept_id
            WHEN 8507 THEN 'male'
            WHEN 8532 THEN 'female'
            ELSE 'unknown'
          END
         ) AS gender
    FROM {{cdm_schema}}.person
    JOIN (
            SELECT person_id
              FROM concept_sets_of_interest AS concept,
                   {{cdm_schema}}.condition_occurrence AS diagnosis
             WHERE (
                       (concept.concept_id = diagnosis.condition_concept_id)
                    OR (concept.concept_id = diagnosis.condition_source_concept_id)
                   )
               AND (concept.concept_set_name LIKE
                     'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;%')
          GROUP BY person_id
            HAVING (count(*) >= 2)
         ) AS cohort
   USING (person_id);

COMMENT ON TABLE cohort_of_interest IS '{{pipeline_version}}';

\echo '  creating index on cohort_of_interest.person_id'

CREATE INDEX ON cohort_of_interest (person_id);
