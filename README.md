# Diabetes ETL pipeline

This repository contains the code of the  [ETL](https://en.wikipedia.org/wiki/Extract,_transform,_load) pipeline used in the paper "Diabetes mellitus risk on 102 bipolar disorder pharmacotherapies: evidence for protective regimens". 

The pipeline is a series of [bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) and [sql](https://en.wikipedia.org/wiki/SQL) scripts meant to be run in a Linux or macOS environment with access to a [PostgreSQL](https://www.postgresql.org/) database (version 9.3 or later) that contains a [CDMv5](https://github.com/OHDSI/CommonDataModel/wiki) dataset.

> **Note:** If not done already it is strongly recommended that you store your PostgreSQL connection information in environment variables so that **`psql`** can access them without having you to type them on the shell prompt. The relevant variables are `PGDATABASE`, `PGHOST` (should be `localhost` if you're working from a SSH session) and `PGUSER` (your user name within the PostgreSQL server). You can also use the `PGPASSWORD` variable for your password, although it is considered more secure to store it in a `${HOME}/.pgpass` file (see [documentation](https://www.postgresql.org/docs/9.3/static/libpq-pgpass.html)).

The pipeline will create various tables, views and functions in a schema that is distinct from the one containing the CDMv5 dataset. Every script that is part of this pipeline uses two variables, named `cdm_schema` and `work_schema`:

**`cdm_schema`** is the schema that contains the CDMv5 dataset; e.g., `mdcr5`

**`work_schema`** is the schema that will contain the results of the pipeline.

> **Note:** You need to create the work schema prior to running the pipeline; e.g., `psql -c 'CREATE SCHEMA mdcr5_johndoe_myproject'`

The pipeline is organized into two sets of scripts, covered in the next two sections. Each script should be run with the included utility **`psql_run`**, which takes three arguments: the name of the script, a value for `cdm_schema`, and a value for `work_schema` (in this order); e.g.,

```bash
$ psql_run a2_declare_concept_sets.sql mdcr5 mdcr5_johndoe_project
```

It is strongly advised to use **`psql_run`** rather than importing the SQL scripts from within a **`psql`** instance (e.g., using the `\include` command), except if you want to deal with the variable interpolation yourself.

## Phase 1: Dataset denormalization

The scripts in the `a_create_building_blocks` folder denormalize a CDMv5 dataset into a few tables suitable for longitudinal studies:

**`a1_declare_functions.sql`** adds various user-defined functions to PostgreSQL. They are used by other SQL scripts for temporal logic and for retrieval of concepts. Utility functions to handle arrays are also included.

**`a2_declare_concept_sets.sql`** creates the `concept_sets_of_interest` table, which will be used thoughout the pipeline. This table contains all concept identifiers defined by our team, grouped into named sets. The script imports other scripts in the `concept_sets/` folder that are updated from a Google Spreadsheet.

**`a3_get_cohort_of_interest.sql`** creates the `cohort_of_interest` table, which is a subset of all patients in the CDM dataset that is relevant to our study.

**`a4_get_visits_of_interest.sql`** creates the `visits_of_interest` and `metavisits_of_interest` tables. These last two tables record all visits and overlapping sets of visits relevant to any patient of the cohort.

**`a5_get_events_of_interest.sql`** creates the `events_of_interest` table, which aggregates and properly indexes the various events recorded in CDMv5: condition occurrences, drug exposures, procedure occurrences, observations, and measurements. Each event is linked to a visit and a metavisit. Also included in this table are drug eras of interest, as computed by OHDSI-based SQL code.

All the building blocks can be created with the **`psql_run`** tool:

```bash
$ psql_run a1_declare_functions.sql mdcr5 mdcr5_johndoe_myproject
$ psql_run a2_declare_concept_sets.sql mdcr5 mdcr5_johndoe_myproject
$ psql_run a3_get_cohort_of_interest.sql mdcr5 mdcr5_johndoe_myproject
$ psql_run a4_get_visits_of_interest.sql mdcr5 mdcr5_johndoe_myproject
$ psql_run a5_get_events_of_interest.sql mdcr5 mdcr5_johndoe_myproject
```

Alternatively, a script named **`a_create_building_blocks.sh`** is provided to do it for you; it takes two arguments, the `cdm_schema` followed by the `work_schema`:

```bash
$ ./a_create_building_blocks.sh mdcr5 mdcr5_johndoe_myproject
```

## Phase 2: Covariates extraction

Once the CDMv5 dataset denormalized we can query its content to generate covariates suitable for downstream analysis. To do this you can find the scripts in the `b_calculate_covariates` folder:

**`b1_get_sequences_of_interest.sql`** creates the `sequences_of_interest` table, which contains the dates of the various sequences of events that constitute the observation period for members of our cohort of interest.

**`b2_get_competing_risks.sql`** creates the `competing_risks` table, which contains the various competing risks identified for the members of our cohort of interest.

**`b3_get_variables.sql`** creates the `variables` table, which contains the outcome variables and covariates we calculate based on the observation periods. It also generates CSV-formatted files with the content of this table.

**`b4_get_ingredient_exposures.sql`** creates a CSV file with ingredient-level exposures for members of our cohort of interest. The CSV file is meant to be processed by the **`b3_get_ingredient_exposures.py`** script, which will aggregate these ingredient-level exposures into non-overlapping eras.

**`b5_get_therapy_eras.sql`** creates a CSV file with drug-level exposures for members of our cohort of interest. The CSV file can be optionally processed by the **`b5_get_therapy_eras.py`** script, which will pivot the corresponding table from a long form to a wide form.

All the building blocks can be created with the **`psql_run`** tool:

```bash
$ psql_run b1_get_sequences_of_interest.sql mdcr5 mdcr5_johndoe_myproject
$ psql_run b2_get_competing_risks.sql mdcr5 mdcr5_johndoe_myproject
$ psql_run b3_get_variables.sql mdcr5 mdcr5_johndoe_myproject
$ psql_run b4_get_ingredient_exposures.sql mdcr5 mdcr5_johndoe_myproject
$ psql_run b5_get_therapy_eras.sql mdcr5 mdcr5_johndoe_myproject
```

Alternatively, a script named **`b_calculate_covariates.sh `** is provided to do it for you; it takes two arguments, the `cdm_schema` followed by the `work_schema`:

```bash
$ ./b_calculate_covariates.sh mdcr5 mdcr5_johndoe_myproject
```
