#!/bin/bash
set -o errexit

steps=(
  "a_create_building_blocks/a1_declare_functions.sql"
  "a_create_building_blocks/a2_declare_concept_sets.sql"
  "a_create_building_blocks/a3_get_cohort_of_interest.sql"
  "a_create_building_blocks/a4_get_visits_of_interest.sql"
  "a_create_building_blocks/a5_get_events_of_interest.sql"
  )

for step in "${steps[@]}"; do
  ./psql_run ${step} "$@"; done
