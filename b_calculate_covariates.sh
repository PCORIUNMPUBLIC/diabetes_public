#!/bin/bash
set -o errexit

steps=(
  "b1_get_sequences_of_interest.sql"
  "b2_get_competing_risks.sql"
  "b3_get_variables.sql"
  "b4_get_ingredient_exposures.sql"
  "b5_get_therapy_eras.sql"
  )

for step in "${steps[@]}"; do
  ./psql_run $(basename $0 .sh)/${step} "$@"; done

b_calculate_covariates/b4_get_ingredient_exposures.py \
    $2.ingredient_exposures.csv.gz \
    $2.hospitalizations.csv.gz \
    $2.boundaries.csv.gz \
    > $2.ingredient_eras.csv

gzip -9 $2.ingredient_eras.csv

b_calculate_covariates/b5_get_therapy_eras.py \
    $2.ingredient_eras.csv.gz \
    $2.variables.csv.gz \
    > $2.therapy_eras.csv

gzip -9 $2.therapy_eras.csv
