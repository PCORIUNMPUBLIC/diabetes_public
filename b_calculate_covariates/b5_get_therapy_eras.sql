-- Identify drug eras of interest that follow index prescription dates

\echo '* linking sequences of interest to drug eras'

\echo '  creating _drug_exposures_per_concept_set'

CREATE TEMPORARY TABLE _drug_exposures_per_concept_set AS
  WITH
    -- (a)+(b) list drug exposures for each of the ingredient-level
    --         concept identifiers associated with a concept_set_name
    --         and happening after a patient's index prescription date
    _drug_exposures_per_ingredient AS (
       SELECT op_start.person_id,
              drug_era.concept_set_name,
              codex_period(
                greatest(op_start.period_3, lower(drug_era.event_period)),
                upper(drug_era.event_period) - 1 + 30
                ) AS period
         FROM sequences_of_interest AS op_start,
              events_of_interest AS drug_era
        WHERE (drug_era.person_id = op_start.person_id)
          AND (drug_era.event_type = 'drug_era_nostockpiling')
              -- we want a drug of interest
          AND (drug_era.concept_set_name LIKE 'Drugs;drug_of_interest;%')
              -- we want the exposure to this drug to have ended
              -- after the patient's index prescription date
          AND ((upper(drug_era.event_period) - 1)
                > op_start.period_3)
      ),
    -- (c) take the union of all ingredient-level drug
    --     exposures for each given concept_set_name
    _drug_exposures_per_concept_name AS (
         SELECT person_id,
                concept_set_name,
                unnest(codex_period_union(array_agg(
                  codex_period(
                    lower(period),
                    upper(period) - 1
                    )))) AS period
           FROM _drug_exposures_per_ingredient
       GROUP BY person_id,
                concept_set_name
      )
  -- (d) trim the resulting drug eras
    SELECT person_id,
           concept_set_name,
           array_agg(
             codex_period(
               lower(period),
               upper(period) - 1 - 30
               )) AS periods
      FROM _drug_exposures_per_concept_name
  GROUP BY person_id,
           concept_set_name;

-- Summary of what happened here for any given patient:
--  (a) we listed all drug eras at the ingredient concept_id level,
--      one set of drug eras per ingredient (ignoring dosage)
--  ==========           =======       =====      Benazepril (1335471)
--     =======   =========                        Captopril (1340128)
--
--  (b) we added 30 days at the end of each drug era
--  ==========...        =======...    =====...   Benazepril (1335471)
--     =======...=========...                     Captopril (1340128)
--
--  (c) we merged the drug eras at the concept_set_name level
--  ===============================    ========   drugs;concomitant;ace_inhibitor;
--
--  (d) we removed 30 days at the end of the drug eras
--  ============================       =====      drugs;concomitant;ace_inhibitor;

\echo '    indexing _drug_exposures_per_concept_set.person_id'

CREATE INDEX ON _drug_exposures_per_concept_set (person_id);

\echo '    indexing _drug_exposures_per_concept_set.concept_set_name'

CREATE INDEX ON _drug_exposures_per_concept_set (concept_set_name);
CREATE INDEX ON _drug_exposures_per_concept_set (concept_set_name varchar_pattern_ops);

\echo '  creating _drug_exposure_events'

DROP TABLE IF EXISTS _drug_exposure_events;
CREATE TEMPORARY TABLE _drug_exposure_events AS
  WITH
    _drug_exposure_periods AS (
       SELECT person_id,
              concept_set_name,
              unnest(periods) AS period
         FROM _drug_exposures_per_concept_set
       -- WHERE (concept_set_name LIKE '%s');
      ),
    _drug_exposure_individual_events AS (
       -- dates at which a drug was added
       SELECT person_id,
              lower(period) AS date,
              concept_set_name AS added,
              NULL AS removed
         FROM _drug_exposure_periods
        UNION ALL
       -- dates at which a drug was removed
       SELECT person_id,
              upper(period) AS date,
              NULL AS added,
              concept_set_name AS removed
         FROM _drug_exposure_periods
        UNION ALL
       -- starting date (period 3)
       SELECT person_id,
              period_3 AS date,
              NULL AS added,
              NULL AS removed
         FROM sequences_of_interest
      )
    SELECT person_id,
           date,
           array_remove(array_agg(added), NULL) AS added,
           array_remove(array_agg(removed), NULL) AS removed
      FROM _drug_exposure_individual_events
  GROUP BY person_id, date;

-- The _drug_exposure_events table contains, for every patient,
-- the dates at which at least one drug was added or removed plus
-- the list of all the drugs added and removed at that date
--
-- For example, considering the following events:
--   2016-01-01: drug 'A' added
--   2016-01-02: drug 'B' added
--   2016-01-03: drug 'A' removed
--
-- Then the resulting table entries for that patient would look like
--   date        added  removed
--   2016-01-01  {'A'}  {}
--   2016-01-02  {'B'}  {}
--   2016-01-03  {}     {'A'}

\echo '    indexing _drug_exposure_events.person_id'

CREATE INDEX ON _drug_exposure_events (person_id);

\echo '    indexing _drug_exposure_events.date'

CREATE INDEX ON _drug_exposure_events (date);

\echo '  creating custom aggregation functions'

DROP FUNCTION IF EXISTS pg_temp._update_drug_list_op(TEXT[], TEXT[], TEXT[]) CASCADE;
CREATE FUNCTION pg_temp._update_drug_list_op
  (current_list TEXT[], added TEXT[], removed TEXT[])
  RETURNS TEXT[] AS
  $$
    DECLARE i INTEGER;
    BEGIN
      IF (array_length(added, 1) > 0) THEN
        -- one or more drugs were added
        FOR i in 1..array_upper(added, 1)
          LOOP
            current_list := array_append(current_list, added[i]);
          END LOOP;
      END IF;

      IF (array_length(removed, 1) > 0) THEN
        -- one or more drugs were removed
        FOR i in 1..array_upper(removed, 1)
          LOOP
            current_list := array_remove(current_list, removed[i]);
          END LOOP;
      END IF;

      RETURN current_list;
    END;
  $$
  LANGUAGE PLPGSQL IMMUTABLE;

DROP AGGREGATE IF EXISTS pg_temp._update_drug_list(TEXT[], TEXT[]);
CREATE AGGREGATE pg_temp._update_drug_list(TEXT[], TEXT[])
  (
    SFUNC = pg_temp._update_drug_list_op,
    STYPE = TEXT[],
    INITCOND = '{}'
  );

\echo '  creating therapy_eras'

DROP TABLE IF EXISTS therapy_eras;
CREATE TABLE therapy_eras AS
  WITH
    _drug_lists AS (
       SELECT person_id,
              date,
              pg_temp._update_drug_list(added, removed) OVER (
                 PARTITION BY person_id
                 ORDER BY date
                ) AS drugs
         FROM _drug_exposure_events
      ),
    _drug_list_pairs AS (
       SELECT person_id,
              lag(date) OVER (
                 PARTITION BY person_id
                 ORDER BY date
                ) AS period_start,
              date AS period_stop,
              lag(drugs) OVER (
                 PARTITION BY person_id
                 ORDER BY date
                ) AS drugs
         FROM _drug_lists
      )
    SELECT person_id,
           codex_period(
              period_start,
              period_stop - 1
             ) AS period,
           drugs AS concept_set_names
      FROM _drug_list_pairs
     WHERE (drugs IS NOT NULL);

COMMENT ON TABLE therapy_eras IS '{{pipeline_version}}';

\echo '    indexing therapy_eras.person_id'

CREATE INDEX ON therapy_eras (person_id);

\echo '    indexing therapy_eras.period'

CREATE INDEX ON therapy_eras USING GIST (period);

\echo '* truncate therapy eras to first competing risk'

CREATE TEMPORARY VIEW _therapy_eras AS
  WITH
    _first_competing_risk AS (
         SELECT DISTINCT ON (person_id)
                person_id,
                lower(event_period) AS date
           FROM competing_risks
       ORDER BY person_id,
                lower(event_period) ASC
      )
     SELECT therapy_era.person_id,
            codex_period(
              lower(therapy_era.period),
              least(
                upper(therapy_era.period) -1,
                competing_risk.date
              )
            ) AS period,
            therapy_era.concept_set_names
       FROM therapy_eras AS therapy_era
       JOIN _first_competing_risk AS competing_risk
      USING (person_id)
      WHERE (lower(therapy_era.period) <= competing_risk.date)
   ORDER BY person_id,
            period;

\echo '* fill any gap between the last era and the first competing risk'

INSERT INTO therapy_eras
  WITH
    _last_therapy_era AS (
         SELECT DISTINCT ON (person_id)
                person_id,
                upper(period) AS date
           FROM therapy_eras
       ORDER BY person_id,
                lower(period) DESC
      ),
    _first_competing_risk AS (
         SELECT DISTINCT ON (person_id)
                person_id,
                lower(event_period) AS date
           FROM competing_risks
       ORDER BY person_id,
                lower(event_period) ASC
      )
  SELECT person_id,
         codex_period(
            last_therapy_era.date,
            first_competing_risk.date - 1
         ) AS period,
         '{}'::TEXT[] AS concept_set_names
    FROM _last_therapy_era AS last_therapy_era
    JOIN _first_competing_risk AS first_competing_risk
   USING (person_id)
   WHERE (last_therapy_era.date < first_competing_risk.date);

/* To get a list of concepts used for the units:

     SELECT concept.concept_id,
            concept.concept_name
       FROM (
             SELECT DISTINCT
                    amount_unit_concept_id AS concept_id
               FROM ccae2003_2015.drug_strength
            ) AS unit_concept
       JOIN ccae2003_2015.concept
      USING (concept_id)
   ORDER BY concept_id;
 */

\echo '* exporting {{work_schema}}.therapy_eras'

\! rm -f {{work_schema}}.therapy_eras.csv.gz

\copy (SELECT * FROM _therapy_eras) TO '{{work_schema}}.therapy_eras.csv' WITH CSV HEADER

\! gzip -9 {{work_schema}}.therapy_eras.csv
