#!/usr/bin/env python
"""
Convert the raw ingredient exposures to per-ingredient eras

Example of usage:
    b4_get_ingredient_exposures.py \
        my_work_schema.ingredient_exposures.csv.gz \
        my_work_schema.hospitalizations.csv.gz \
        my_work_schema.boundaries.csv.gz \
        | gzip -9 > ingredient_eras.csv.gz
"""

from __future__ import print_function

import csv
import datetime
import gzip
import itertools
import sys

import intervaltree
# see https://pypi.org/project/intervaltree/#description
# for a demonstration of how the tree can be manipulated

assert (len(sys.argv) == 4)

ingredient_exposures_fn = sys.argv[1]
assert (ingredient_exposures_fn is not None)

hospitalizations_fn = sys.argv[2]
assert (hospitalizations_fn is not None)

boundaries_fn = sys.argv[3]
assert (boundaries_fn is not None)

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

def open_input(fn):
    if (fn.lower().endswith(".gz")):
        fh = gzip.open(fn, "rb")
    else:
        fh = open(fn, "rU")

    return csv.DictReader(fh)

EPOCH = datetime.date(1970, 1, 1)
is_blank = lambda x: (x is None) or (len(x.strip()) == 0)

def parse_date(value):
    if (value.strip() == ""):
        return None
    else:
        return datetime.datetime.strptime(value, r"%Y-%m-%d").date()

write_date = lambda date: date.strftime("%Y-%m-%d")

def parse_period(value):
    # extract the period boundaries, converting them
    # into number of days since the Unix EPOCH
    period_start, period_end = value[1:-1].split(",")
    period_start = (parse_date(period_start) - EPOCH).days  # the lower bound is inclusive
    period_end = (parse_date(period_end) - EPOCH).days  # the upper bound is exclusive
    return period_start, period_end

UNITS = {
    8576: 1.,  # milligrams
}

def parse_dosage(**kwargs):   # dummy implementation for now
    # amount_value = kwargs["amount_value"]
    # if (not is_blank(amount_value)):
    #     amount_value = int(amount_value)
    #     amount_unit = UNITS[int(kwargs["amount_unit_concept_id"])]
    #     return amount_value * amount_unit
    return None

def info(msg):
    print(msg, file = sys.stderr)
    sys.stderr.flush()

def build_trees():
    info("* reading %s" % hospitalizations_fn)
    hospitalizations = {}
    for entry in open_input(hospitalizations_fn):
        person_id = entry["person_id"]
        (period_start, period_end) = parse_period(entry["metavisit_period"])

        if (not person_id in hospitalizations):
            hospitalizations[person_id] = []

        hospitalizations[person_id].append((period_start, period_end))

    info("  %d unique person_id processed" % len(hospitalizations))

    info("* reading %s" % boundaries_fn)
    boundaries = {}
    for entry in open_input(boundaries_fn):
        person_id = entry["person_id"]
        lower_date = parse_date(entry["lower_bound"])
        upper_date = parse_date(entry["upper_bound"])

        boundaries[person_id] = (lower_date, upper_date)

    info("  %d unique person_id processed" % len(boundaries))

    info("* reading %s" % ingredient_exposures_fn)
    current_person_id, current_trees = None, {}
    exposures = set()
    for entry in open_input(ingredient_exposures_fn):
        person_id = entry.pop("person_id")
        exposures.add(person_id)
        if (person_id != current_person_id) and (current_person_id is not None):
            for (period_start, period_end) in hospitalizations.get(current_person_id, []):
                if (not "hospitalization" in current_trees):
                    current_trees["hospitalization"] = intervaltree.IntervalTree()

                current_trees["hospitalization"].addi(period_start, period_end)

            yield (current_person_id, current_trees, boundaries[current_person_id])
            current_trees = {}

        ingredient = entry.pop("concept_set_name")
        if (not ingredient in current_trees):
            current_trees[ingredient] = intervaltree.IntervalTree()

        (period_start, period_end) = parse_period(entry.pop("period"))
        current_trees[ingredient].addi(period_start, period_end, {
            "dosage": parse_dosage(**entry)})

        current_person_id = person_id

    for (period_start, period_end) in hospitalizations.get(current_person_id, []):
        if (not "hospitalization" in current_trees):
            current_trees["hospitalization"] = intervaltree.IntervalTree()

        current_trees["hospitalization"].addi(period_start, period_end)

    if (len(current_trees) > 0):
        yield (current_person_id, current_trees, boundaries[current_person_id])

    info("  %d unique person_id processed" % len(exposures))

    info("* adding people with no drug exposures")

    missing = set(boundaries) - exposures
    for person_id in missing:
        yield (person_id, None, boundaries[person_id])

    info("  %d unique person_id processed" % len(missing))

def successive_pairs(iterable):
    """s -> (s0,s1), (s1,s2), (s2, s3), ..."""
    a, b = itertools.tee(iterable)
    next(b, None)
    return zip(a, b)

def walk_timeline(timeline):
    period_start, current_ingredients = 0, timeline[0]
    for (tick, ingredients) in enumerate(timeline):
        if (ingredients != current_ingredients):
            yield (period_start, tick, current_ingredients)
            period_start, current_ingredients = tick, ingredients

    if (len(current_ingredients) > 0):
        yield (period_start, tick + 1, current_ingredients)

#::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

MAX_GAP = 14  # rule 1 (maximum gap, in days, after we consider a patient dropped off a drug)

is_last = lambda interval: interval.data.get("is_last", True)

for (person_id, trees, (min_date, max_date)) in build_trees():
    # special case of people with no drug exposures
    if (trees is None):
        print("%s,\"[%s,%s)\",\"{}\"" % (person_id,
            write_date(min_date), write_date(max_date)))
        continue

    # rule 2 (hospitalizations)
    hospitalizations = trees.pop("hospitalization", intervaltree.IntervalTree())
    for hospitalization in hospitalizations.all_intervals:  # type: intervaltree.Interval
        for (ingredient, tree) in trees.iteritems():
            # list all exposures that started from the beginning
            # of the hospitalization up to MAX_GAP after its end
            period_start = hospitalization.begin
            period_end = hospitalization.end - 1 + MAX_GAP
            for exposure in tree.search(period_start, period_end):  # type: intervaltree.Interval
                if (exposure.begin <= period_start): continue
                # update these exposures so that they start
                # at the beginning of the hospitalization
                tree.discard(exposure)
                tree.addi(
                    period_start,
                    exposure.end,
                    exposure.data)

    # rule 3 (stockpiling)
    for (ingredient, tree) in trees.iteritems():
        excess = 0
        for (interval_a, interval_b) in successive_pairs(sorted(tree.all_intervals)):
            gap = interval_b.begin - interval_a.end - excess
            will_edit = (excess > 0) or (gap < 0)
            if (will_edit): tree.discard(interval_a)

            # if there is a running excess, lengthen interval_a
            if (excess > 0):
                interval_a = intervaltree.Interval(
                    interval_a.begin,
                    interval_a.end + excess,
                    interval_a.data)
                excess = 0

            # if there is an overlap, shorten interval_a
            if (gap < 0):
                # ... but only if that doesn't
                # reduces its length to zero
                if (interval_a.length() + gap > 0):
                    interval_a = intervaltree.Interval(
                        interval_a.begin,
                        interval_a.end + gap,
                        interval_a.data)
                else:
                    will_edit = False
                # set the overlap as the new excess
                excess = -gap

            if (will_edit): tree.add(interval_a)

        if (excess > 0) and ("interval_b" in vars()):
            tree.discard(interval_b)
            tree.addi(
                interval_b.begin,
                interval_b.end + excess,
                interval_b.data)

    # rules 4 and 5 (polypharmacy)

    # mark each ingredient exposure as being followed by MAX_GAP or not;
    # also look for the beginning and end of that patient's time line
    min_timepoint, max_timepoint = sys.maxint, -sys.maxint
    for tree in trees.itervalues():
        for (interval_a, interval_b) in successive_pairs(sorted(tree.all_intervals)):
            gap = interval_b.begin - interval_a.end
            assert (gap >= 0), (gap, interval_a, interval_b, tree)

            interval_a.data["is_last"] = (gap > MAX_GAP)
            interval_a.data["gap"] = gap

        min_timepoint = min(min_timepoint, tree.begin())
        max_timepoint = max(max_timepoint, tree.end())

    timeline = [set() for i in xrange(min_timepoint, max_timepoint + MAX_GAP)]

    # declare all ingredient eras, merging per-ingredient exposures
    # that are followed by another no more than MAX_GAP days after
    all_ingredients_tree = intervaltree.IntervalTree()
    for (ingredient, tree) in trees.iteritems():
        for interval in tree.all_intervals:  # type: intervaltree.Interval
            upper_bound = interval.end
            if (not is_last(interval)):
                upper_bound += interval.data.get("gap", 0)

            # add the augmented interval to the all-ingredients tree
            all_ingredients_tree.addi(interval.begin, upper_bound, ingredient)

            # add the augmented interval to the timeline
            for tick in xrange(interval.begin, upper_bound):
                timepoint = tick - min_timepoint  # timeline coordinate
                timeline[timepoint].add(ingredient)

    # truncate ingredient eras when a prescription switch is detected
    for (ingredient, tree) in trees.iteritems():
        for interval in tree.all_intervals:  # type: intervaltree.Interval
            # only consider last refills
            if (not is_last(interval)): continue

            # check if other ingredient era(s) started after the refill of
            # our ingredient, taking note of when that other refill started
            switch_at = None
            other_exposures = all_ingredients_tree.search(interval.begin + 1, interval.end)
            for other_exposure in sorted(other_exposures):
                if (other_exposure.data == ingredient): continue
                if (other_exposure.begin < interval.begin + 1): continue
                switch_at = other_exposure.begin
                break

            # if such event occur, record the switch
            if (switch_at is not None):
                for tick in xrange(switch_at, interval.end):
                    timepoint = tick - min_timepoint
                    timeline[timepoint] -= {ingredient}

    earliest_date, latest_date = None, None
    for (period_start, period_end, ingredients) in walk_timeline(timeline):
        period_start = EPOCH + datetime.timedelta(days = period_start + min_timepoint)
        period_end = EPOCH + datetime.timedelta(days = period_end + min_timepoint)

        # identify the earliest and latest dates
        if (earliest_date is None): earliest_date = period_start
        latest_date = period_end

        # truncate the ingredient era at the boundary dates
        if (min_date is not None):
            if (period_end <= min_date): continue
            period_start = max(period_start, min_date)

        if (max_date is not None):
            if (period_start >= max_date): continue
            period_end = min(period_end, max_date)

        print("%s,\"[%s,%s)\",\"{%s}\"" % (person_id,
            write_date(period_start), write_date(period_end), ",".join(sorted(ingredients))))

    # ensure the earliest and latest dates are bounded
    if (min_date is not None):
        earliest_date = max(earliest_date, min_date)
        latest_date = max(latest_date, min_date)

    if (max_date is not None):
        earliest_date = min(earliest_date, max_date)
        latest_date = min(latest_date, max_date)

    # create a no-drug era between min_date and the first ingredient era, if needed
    if (min_date is not None) and (min_date < earliest_date):
        print("%s,\"[%s,%s)\",\"{}\"" % (person_id,
            write_date(min_date), write_date(earliest_date)))

    # create a no-drug era between the last ingredient era and max_date, if needed
    if (max_date is not None) and (max_date > latest_date):
        print("%s,\"[%s,%s)\",\"{}\"" % (person_id,
            write_date(latest_date), write_date(max_date)))
