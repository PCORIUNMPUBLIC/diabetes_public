
SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.sequences_of_interest'

\echo '  staging events'

\echo '    staging bipolar disorder diagnoses'

DROP TABLE IF EXISTS _bipolar_diagnosis CASCADE;
CREATE TABLE _bipolar_diagnosis AS
  SELECT person_id,
         metavisit_period AS event_period
    FROM metavisits_of_interest AS metavisit
   WHERE (EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (event.concept_set_name LIKE
                   'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;%')
         ));

CREATE INDEX ON _bipolar_diagnosis (person_id);
CREATE INDEX ON _bipolar_diagnosis USING GIST (event_period);

\echo '    staging glucose-related disorder diagnoses'

-- we don't want them at any time before period 3
DROP TABLE IF EXISTS _glucose_disorder CASCADE;
CREATE TABLE _glucose_disorder AS
  SELECT person_id,
         metavisit_period AS event_period
    FROM metavisits_of_interest AS metavisit
   WHERE (EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (
                     (event.concept_set_name LIKE
                       'Diagnoses;somatic_comorb;glucose_metabolism_regulation;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;%')
                  OR (event.concept_set_name LIKE
                       'Drugs;exclusion;diabetes;hypoglycemic;insulin;%')
                  OR (event.concept_set_name LIKE
                       'Procedures;exclusion;diabetes;hypoglycemic;insulin;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;%')
                 )
         ));

CREATE INDEX ON _glucose_disorder (person_id);
CREATE INDEX ON _glucose_disorder USING GIST (event_period);

\echo '    staging mental disorders'

-- we don't want them at any time
DROP TABLE IF EXISTS _mental_disorder CASCADE;
CREATE TABLE _mental_disorder AS
  SELECT person_id,
         event_period
    FROM events_of_interest
   WHERE (concept_set_name LIKE 'Diagnoses;exclusion;mental;%');

\echo '    staging anti-dementia drug exposures'

-- we don't want them at any time
DROP TABLE IF EXISTS _antidementia_drug_exposure CASCADE;
CREATE TABLE _antidementia_drug_exposure AS
  SELECT person_id,
         event_period
    FROM events_of_interest
   WHERE (event_type = 'drug_era_stockpiling')
     AND (
             (concept_set_name LIKE 'Drugs;exclusion;memantine;%')
          OR (concept_set_name LIKE 'Drugs;exclusion;cholinesterase_inhibitor;%')
         );

\echo '    staging unwanted events before period 3'

CREATE TEMPORARY TABLE _unwanted_events_before_period_3 AS
  SELECT *
    FROM _glucose_disorder;

CREATE INDEX ON _unwanted_events_before_period_3 (person_id);
CREATE INDEX ON _unwanted_events_before_period_3 USING GIST (event_period);

\echo '    staging unwanted events at any time'

CREATE TEMPORARY TABLE _unwanted_events_at_any_time AS
  SELECT *
    FROM _mental_disorder
         UNION ALL
  SELECT *
    FROM _antidementia_drug_exposure;

CREATE INDEX ON _unwanted_events_at_any_time (person_id);
CREATE INDEX ON _unwanted_events_at_any_time USING GIST (event_period);

/******************************************************************************/

\echo '  finding dataset first recorded date'

CREATE TEMPORARY TABLE _first_recorded_date AS
    SELECT lower(event_period) AS date
      FROM events_of_interest
  ORDER BY event_period
     LIMIT 1;

\echo '  finding sequences of interest'

DROP TABLE IF EXISTS sequences_of_interest CASCADE;
CREATE TABLE sequences_of_interest AS
    SELECT DISTINCT ON (index_event.person_id)  -- we only keep the first
           index_event.*
      FROM (
            SELECT index_event.person_id,
                   -- period of 12 months prior to diagnosis
                   codex_period(
                     lower(index_event.event_period) - 365,
                     lower(index_event.event_period) - 1
                     ) AS period_1,
                   -- metavisit during which the diagnosis occurred
                   index_event.event_period AS period_2,
                   -- index exposure
                   upper(index_event.event_period) - 1 AS period_3
              FROM cohort_of_interest AS cohort,
                   _bipolar_diagnosis AS index_event,
                   _first_recorded_date AS first_recorded_date
             WHERE (cohort.person_id = index_event.person_id)
                   -- we want the index_event to happen
                   -- after at least one year of known events
               AND ((lower(index_event.event_period)
                     - first_recorded_date.date) >= 365)
                   -- we want period 3 to happen
                   -- between 18 and 64 years of age
               AND (date_part('year', age(
                     upper(index_event.event_period) - 1,
                     cohort.birth_date)) BETWEEN 18 AND 64)
           ) AS index_event
           -- we do not want some events prior to period 3
     WHERE (NOT EXISTS (
            SELECT true
              FROM _unwanted_events_before_period_3 AS event
             WHERE (event.person_id = index_event.person_id)
               AND (lower(event.event_period) < index_event.period_3)
           ))
           -- we do not want some events ever
       AND (NOT EXISTS (
            SELECT true
              FROM _unwanted_events_at_any_time AS event
             WHERE (event.person_id = index_event.person_id)
           ))
  ORDER BY index_event.person_id,
           index_event.period_3;

COMMENT ON TABLE sequences_of_interest IS '{{pipeline_version}}';

\echo '  indexing sequences of interest'

ALTER TABLE sequences_of_interest
  ADD PRIMARY KEY (person_id);

CREATE INDEX ON sequences_of_interest USING GIST (period_1);
CREATE INDEX ON sequences_of_interest USING GIST (period_2);
CREATE INDEX ON sequences_of_interest (period_3);

ANALYZE sequences_of_interest;
