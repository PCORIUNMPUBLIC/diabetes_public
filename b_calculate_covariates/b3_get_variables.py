#!/usr/bin/python
"""
Convert the compressed, long-formatted table produced by `b3_get_variables.sql`
into a wide-formatted table suitable for downstream processing.
"""

import csv
import gzip
import sys

i_fn = sys.argv[1]
if (i_fn.lower().endswith(".gz")):
    i_fh = gzip.open(i_fn, "rb")
else:
    i_fh = open(i_fn, "r")

columns, rows = set(), {}
for entry in csv.DictReader(i_fh):
    entry_key = int(entry["person_id"])

    key = entry["variable_name"]
    value = entry["variable_value"]

    columns.add(key)
    rows.setdefault(entry_key, {})
    rows[entry_key][key] = value

columns = sorted(columns)

o_fh = csv.writer(sys.stdout)
o_fh.writerow(["person_id"] + list(columns))
for person_id in sorted(rows):
    o_fh.writerow([person_id] + [rows[person_id].get(key, "") for key in columns])
