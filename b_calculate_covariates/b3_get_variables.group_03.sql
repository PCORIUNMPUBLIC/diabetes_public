
\echo '    staging events for covariates #3*'

\echo '      creating _events_during_period_2'

CREATE TEMPORARY TABLE _events_during_period_2 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM sequences_of_interest AS index_event,
         events_of_interest AS event
   WHERE (index_event.person_id = event.person_id)
     AND (index_event.period_2 && event.event_period)
     AND (event.concept_set_name LIKE 'Diagnoses;%');

CREATE INDEX ON _events_during_period_2 (person_id);
CREATE INDEX ON _events_during_period_2 (concept_set_name);
CREATE INDEX ON _events_during_period_2 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '    calculating covariate #3(a)(i), `diagnosis_of_bipolar_i_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'diagnosis_of_bipolar_i_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE
                    'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(a)(ii), `diagnosis_of_bipolar_ii_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'diagnosis_of_bipolar_ii_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE
                    'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_ii;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(a)(iii), `diagnosis_of_bipolar_nos_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'diagnosis_of_bipolar_nos_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE
                    'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(b)(i), `manic_episode_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'manic_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;polarity;manic;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;depressive;%')
                        OR (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;mixed;%')
                       )
                ))
         )::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(b)(ii), `depressive_episode_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'depressive_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;polarity;depressive;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;manic;%')
                        OR (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;mixed;%')
                       )
                ))
         )::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(b)(iii), `mixed_episode_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'mixed_episode_during_period_2'::TEXT,
         (
             (EXISTS (
               SELECT TRUE
                 FROM _events_during_period_2 AS visit
                WHERE (visit.person_id = op_start.person_id)
                  AND (visit.concept_set_name LIKE
                        'Diagnoses;episode;polarity;mixed;%')
               ))
          OR (
                  (EXISTS (
                    SELECT TRUE
                      FROM _events_during_period_2 AS visit
                     WHERE (visit.person_id = op_start.person_id)
                       AND (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;manic;%')
                    ))
              AND (EXISTS (
                    SELECT TRUE
                      FROM _events_during_period_2 AS visit
                     WHERE (visit.person_id = op_start.person_id)
                       AND (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;depressive;%')
                    ))
             )
         )::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(b)(iv), `unknown_polarity_episode_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'unknown_polarity_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;polarity;unknown;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;manic;%')
                        OR (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;depressive;%')
                        OR (visit.concept_set_name LIKE
                             'Diagnoses;episode;polarity;mixed;%')
                       )
                ))
         )::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(c)(i), `psychotic_features_present_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'psychotic_features_present_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE
                    'Diagnoses;episode;psychotic;present;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(c)(ii), `psychotic_features_absent_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'psychotic_features_absent_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;psychotic;absent;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;psychotic;present;%')
                ))
         )::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(c)(iii), `psychotic_features_unknown_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'psychotic_features_unknown_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;psychotic;unknown;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'Diagnoses;episode;psychotic;present;%')
                        OR (visit.concept_set_name LIKE
                             'Diagnoses;episode;psychotic;absent;%')
                       )
                ))
         )::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(d)(i), `mild_mood_episode_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'mild_mood_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;severity;mild;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'Diagnoses;episode;severity;moderate;%')
                        OR (visit.concept_set_name LIKE
                             'Diagnoses;episode;severity;severe;%')
                       )
                ))
         )::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(d)(ii), `moderate_mood_episode_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'moderate_mood_episode_during_period_2'::TEXT,
         (
              (EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;severity;moderate;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;severity;severe;%')
                ))
         )::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(d)(iii), `severe_mood_episode_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'severe_mood_episode_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _events_during_period_2 AS visit
            WHERE (visit.person_id = op_start.person_id)
              AND (visit.concept_set_name LIKE
                    'Diagnoses;episode;severity;severe;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #3(d)(iv), `unknown_mood_episode_during_period_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'unknown_mood_episode_during_period_2'::TEXT,
         (
              (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (visit.concept_set_name LIKE
                         'Diagnoses;episode;severity;mild;%')
                ))
          AND (NOT EXISTS (
                SELECT TRUE
                  FROM _events_during_period_2 AS visit
                 WHERE (visit.person_id = op_start.person_id)
                   AND (
                           (visit.concept_set_name LIKE
                             'Diagnoses;episode;severity;moderate;%')
                        OR (visit.concept_set_name LIKE
                             'Diagnoses;episode;severity;severe;%')
                       )
                ))
         )::INTEGER
    FROM sequences_of_interest AS op_start;

/**
Test: for any combination of patient_id and therapy_name,
      each of these covariates should have a value

  SELECT variable_name,
         count(CASE WHEN variable_value::INTEGER = 1 THEN 1 END) AS n_yes,
         count(CASE WHEN variable_value::INTEGER = 0 THEN 1 END) AS n_no,
         count(*) AS n_total
    FROM variables
   WHERE (variable_name LIKE '%_during_period_2')
GROUP BY variable_name
ORDER BY variable_name;
**/
