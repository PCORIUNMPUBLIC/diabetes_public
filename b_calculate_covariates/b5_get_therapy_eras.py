#!/usr/bin/python 

import sys
import gzip
if len(sys.argv) != 3:
    sys.stderr.write("Usage: " + sys.argv[0] + " <erasfile.csv.gz> <variablesfile.csv.gz>\n")
    sys.exit(1)
    
erasfile = sys.argv[1]
variablesfile = sys.argv[2]
sys.stderr.write("Eras file: " + erasfile + "\n")
sys.stderr.write("Variables file: " + variablesfile + "\n")
sys.stderr.write("Reading variables\n")
f1 = gzip.open(variablesfile,"r")
header1 = f1.readline().rstrip()

#Read in the covariates describing the time periods
#It is in long form, so build a dictionary on person_id of dictionaries containing covariates and values
varnames1 = {}
pcov = {}
for line in f1:
    v=line.rstrip().split(",")
    varnames1[v[1]] = True
    if not v[0] in pcov:
        pcov[v[0]] = {}
    pcov[v[0]][v[1]] = v[2]

f1.close()

varnames = varnames1.keys()
varnames.sort()

header1 = "person_id,period_start,period_stop," + ','.join(varnames)

sys.stderr.write("Scanning drugs\n")
f2 = gzip.open(erasfile,"r")

drugs1 = {}
for line in f2:
    v=line.translate(None,"\"[](){}\r\n").split(",")
    for x in v[3:]:
        drugs1[x] = True
f2.close()

drugs = drugs1.keys()
drugs.sort()
header2 =  ','.join(drugs)
print header1 + "," + header2

sys.stderr.write("Final pass -- writing results\n")
f2 = gzip.open(erasfile,"r")
for line in f2:
    v=line.translate(None,"\"[](){}\r\n").split(",")
    d = {}
    for x in v[3:]:
        d[x] = True
    print ','.join(v[0:3]) + "," +  ','.join([str(pcov[v[0]][x]) for x in varnames]) + "," + ','.join([str(int(d.has_key(x))) for x in drugs])
    
f2.close()

