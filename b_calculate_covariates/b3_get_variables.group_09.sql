
\echo '  staging events for covariates #9*'

-- list comorbidities that occurred during the period 1 or 2

\echo '    creating _comorbidities_during_period_1_or_2'

CREATE TEMPORARY TABLE _comorbidities_during_period_1_or_2 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM sequences_of_interest AS index_event,
         events_of_interest AS event
   WHERE (index_event.person_id = event.person_id)
     AND (upper(event.event_period) - 1
           BETWEEN lower(index_event.period_1)
               AND upper(index_event.period_2) - 1)
     AND (event.concept_set_name LIKE 'Diagnoses;%');

CREATE INDEX ON _comorbidities_during_period_1_or_2 (person_id);
CREATE INDEX ON _comorbidities_during_period_1_or_2 (concept_set_name);
CREATE INDEX ON _comorbidities_during_period_1_or_2 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '  calculating covariate #9(a)(i), `observation_of_bipolar_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_bipolar_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(ii), `observation_of_major_depressive_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_major_depressive_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mmi;bipolar_spectrum;major_depressive;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(iii), `observation_of_alcohol_dependence_or_abuse_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_alcohol_dependence_or_abuse_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;alcohol_depend_abus;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(iv), `observation_of_alcohol_induced_mental_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_alcohol_induced_mental_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;alcohol_induced;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(v), `observation_of_drug_dependence_or_abuse_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_drug_dependence_or_abuse_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;drug_depend_abus;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(vi), `observation_of_drug_induced_mental_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_drug_induced_mental_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;drug_induced;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(vii), `observation_of_opioid_dependence_or_abuse_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_opioid_dependence_or_abuse_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;opioid_abuse_depend;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(viii), `observation_of_delirium_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_delirium_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;delirium;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(ix), `observation_of_other_mood_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_other_mood_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;other_mood;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(x), `observation_of_acute_psychosis_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_acute_psychosis_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;acute_psychosis;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xi), `observation_of_anxiety_and_dissociative_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_anxiety_and_dissociative_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;anxiety_dissoc;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xii), `observation_of_generalized_anxiety_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_generalized_anxiety_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;gad;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xiii), `observation_of_panic_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_panic_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;panic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xiv), `observation_of_psychosomatic_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_psychosomatic_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;psychosomat;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xv), `observation_of_personality_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_personality_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;personality;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xvi), `observation_of_sexual_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_sexual_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;sexual;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xvii), `observation_of_motor_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_motor_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;motor;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xviii), `observation_of_eating_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_eating_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;eating;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xix), `observation_of_sleep_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_sleep_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;sleep;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xx), `observation_of_stress_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_stress_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;stress;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xxi), `observation_of_conduct_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_conduct_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;conduct_impuls;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xxii), `observation_of_youth_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_youth_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;child_adolesc;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xxiii), `observation_of_hyperkinetic_syndrome_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_hyperkinetic_syndrome_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;hyperkinet;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xxiv), `observation_of_development_delay_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_development_delay_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;development_delay;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xxv), `observation_of_pregnancy_related_mental_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_pregnancy_related_mental_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;pregnancy_delivery;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(a)(xxvi), `observation_of_intentional_self_harm_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_intentional_self_harm_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;mental_comorb;self_harm;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(i), `observation_of_autoimmune_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_autoimmune_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;autoimmune;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(ii), `observation_of_cardiovascular_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_cardiovascular_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;cardiovascular;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(iii), `observation_of_dermatological_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_dermatological_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;dermatological;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(iv), `observation_of_endocrinopathy_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_endocrinopathy_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;endocrinopathy;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(v), `observation_of_obesity_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_obesity_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;obesity_and_overweight;obesity;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(vi), `observation_of_overweight_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_overweight_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;obesity_and_overweight;overweight;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(vii), `observation_of_hypertension_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_hypertension_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;hypertension;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(viii), `observation_of_cardiac_arrhythmia_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_cardiac_arrhytmia_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;arrhytmia;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(ix), `observation_of_kidney_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_kidney_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;kidney;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(x), `observation_of_metabolic_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_metabolic_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;metabolic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xi), `observation_of_musculoskeletal_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_musculoskeletal_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;musculoskeletal;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xii), `observation_of_nervous_system_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_nervous_system_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;nerv_system;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xiii), `observation_of_cns_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_cns_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;central_nerv_system;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xiv), `observation_of_seizure_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_seizure_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;seizure;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xv), `observation_of_demyelinating_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_demyelinating_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;demyelinat;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xvi), `observation_of_neuromuscular_transmission_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_neuromuscular_transmission_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;neuromusc_transmission;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xvii), `observation_of_chronic_pain_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_chronic_pain_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;chronic_pain;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xviii), `observation_of_movement_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_movement_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;movement_medicat;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xix), `observation_of_pulmonary_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_pulmonary_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;pulmon;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xx), `observation_of_thyroid_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_thyroid_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;thyroid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxi), `observation_of_liver_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_liver_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;liver;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxii), `observation_of_digestive_tract_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_digestive_tract_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;digestive;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxiii), `observation_of_hiv_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_hiv_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;hiv_aids;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxiv), `observation_of_tuberculosis_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_tuberculosis_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;tuberculosis;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxv), `observation_of_pregnancy_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_pregnancy_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;currently_pregnant;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxvi), `observation_of_delivery_and_perinatal_care_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_delivery_and_perinatal_care_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;delivery_perinatal_care;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxvii), `observation_of_abortion_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_abortion_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;abortion;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxviii), `observation_of_chronic_infectious_disease_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_chronic_infectious_disease_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;chronic_infect_disease;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxix), `observation_of_neoplasm_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_neoplasm_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;neoplasm_hamartoma;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxx), `observation_of_poststreptococcal_disorder_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_poststreptococcal_disorder_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;post_streptococ;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxxi), `observation_of_rheumatism_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_rheumatism_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;rheumatism;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxxii), `observation_of_std_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_std_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;sexually_transmitted;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxxiii), `observation_of_urinary_incontinence_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_urinary_incontinence_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;urinary_incontinence;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxxiv), `observation_of_toxic_exposure_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_toxic_exposure_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;toxic_effect;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '  calculating covariate #9(b)(xxxv), `observation_of_metal_exposure_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'observation_of_metal_exposure_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _comorbidities_during_period_1_or_2 AS comorbidity
            WHERE (comorbidity.person_id = op_start.person_id)
              AND (comorbidity.concept_set_name LIKE
                    'Diagnoses;somatic_comorb;metal;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

/**
Test: for any combination of patient_id and therapy_name,
      each of these covariates should have a value

  SELECT variable_name,
         count(CASE WHEN variable_value::INTEGER = 1 THEN 1 END) AS n_yes,
         count(CASE WHEN variable_value::INTEGER = 0 THEN 1 END) AS n_no,
         count(*) AS n_total
    FROM variables
   WHERE (variable_name LIKE 'observation_of_%_during_period_1_or_2')
GROUP BY variable_name
ORDER BY variable_name;
**/
