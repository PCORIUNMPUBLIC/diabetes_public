
SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.variables'

DROP TABLE IF EXISTS variables CASCADE;
CREATE TABLE variables
  (
        person_id BIGINT NOT NULL,
    variable_name TEXT NOT NULL,
   variable_value TEXT NOT NULL
  );

CREATE INDEX ON variables (person_id, variable_name);

COMMENT ON TABLE variables IS '{{pipeline_version}}';

\echo '  calculating covariate #1, `age_at_exposure_date`'

INSERT INTO variables
  SELECT index_event.person_id,
         'age_at_exposure_date'::TEXT,
         (date_part('year', age(
           index_event.period_3,
           cohort.birth_date))) AS age
    FROM sequences_of_interest AS index_event
    JOIN cohort_of_interest AS cohort USING (person_id);

\echo '  calculating covariate #2, `sex`'

INSERT INTO variables
  SELECT index_event.person_id,
         'sex'::TEXT,
         cohort.gender
    FROM sequences_of_interest AS index_event
    JOIN cohort_of_interest AS cohort USING (person_id);

\echo '  calculating covariates #3*:'
\include 'b_calculate_covariates/b3_get_variables.group_03.sql'

\echo '  calculating covariates #4*:'
\include 'b_calculate_covariates/b3_get_variables.group_04.sql'

\echo '  calculating covariates #5*:'
\include 'b_calculate_covariates/b3_get_variables.group_05.sql'

\echo '  calculating covariate #6, `hospitalization_before_period_3`'

INSERT INTO variables
  SELECT person_id,
         'hospitalization_before_period_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM metavisits_of_interest AS metavisit
            WHERE (metavisit.person_id = index_event.person_id)
                  -- this metavisit ended between periods 1 and 3
              AND (upper(metavisit.metavisit_period) - 1
                    BETWEEN lower(index_event.period_1)
                        AND index_event.period_3 - 1)
                  -- this metavisit includes an hospitalization or an ER visit
              AND (metavisit.visit_types && '{inpatient,emergency}'::TEXT[])
           ))::INTEGER
    FROM sequences_of_interest AS index_event;

\echo '  calculating covariate #7, `psychiatric_hospitalization_before_period_3`'

INSERT INTO variables
  SELECT person_id,
         'psychiatric_hospitalization_before_period_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM metavisits_of_interest AS metavisit,
                  events_of_interest AS event
            WHERE (metavisit.person_id = index_event.person_id)
                  -- this metavisit ended between periods 1 and 3
              AND (upper(metavisit.metavisit_period) - 1
                    BETWEEN lower(index_event.period_1)
                        AND index_event.period_3 - 1)
                  -- this metavisit includes an hospitalization or an ER visit
              AND (metavisit.visit_types && '{inpatient,emergency}'::TEXT[])
                  -- this metavisit is associated to a concept set of interest
              AND (event.metavisit_occurrence_id
                    = metavisit.metavisit_occurrence_id)
              AND (event.concept_set_name LIKE
                    'Diagnoses;psychiatric_diagnoses;%')
           ))::INTEGER
    FROM sequences_of_interest AS index_event;

\echo '  calculating covariate #8(a), `outpatient_visit_during_period_2`'

INSERT INTO variables
  SELECT person_id,
         'outpatient_visit_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM metavisits_of_interest AS metavisit
            WHERE (metavisit.person_id = index_event.person_id)
                  -- this metavisit overlapped with period 2
              AND (metavisit.metavisit_period && index_event.period_2)
                  -- this metavisit if of a given type
              AND (metavisit.visit_types && '{outpatient}'::TEXT[])
           ))::INTEGER
    FROM sequences_of_interest AS index_event;

\echo '  calculating covariate #8(b), `er_visit_during_period_2`'

INSERT INTO variables
  SELECT person_id,
         'er_visit_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM metavisits_of_interest AS metavisit
            WHERE (metavisit.person_id = index_event.person_id)
                  -- this metavisit overlapped with period 2
              AND (metavisit.metavisit_period && index_event.period_2)
                  -- this metavisit if of a given type
              AND (metavisit.visit_types && '{emergency}'::TEXT[])
           ))::INTEGER
    FROM sequences_of_interest AS index_event;

\echo '  calculating covariate #8(c), `inpatient_visit_during_period_2`'

INSERT INTO variables
  SELECT person_id,
         'inpatient_visit_during_period_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM metavisits_of_interest AS metavisit
            WHERE (metavisit.person_id = index_event.person_id)
                  -- this metavisit overlapped with period 2
              AND (metavisit.metavisit_period && index_event.period_2)
                  -- this metavisit if of a given type
              AND (metavisit.visit_types && '{inpatient}'::TEXT[])
           ))::INTEGER
    FROM sequences_of_interest AS index_event;

\echo '  calculating covariates #9*:'
\include 'b_calculate_covariates/b3_get_variables.group_09.sql'

\echo '  staging first competing risks'

-- first competing risk occurring for any given patient
CREATE TEMPORARY TABLE _first_competing_risk AS
    SELECT DISTINCT ON (person_id)
           *
      FROM competing_risks
  ORDER BY person_id,
           lower(event_period) ASC;

CREATE INDEX ON _first_competing_risk (person_id);
CREATE INDEX ON _first_competing_risk USING GIST (event_period);
CREATE INDEX ON _first_competing_risk (event_type);

\echo '  calculating covariate #10, `is_diabetes_mellitus_drug_induced`'

INSERT INTO variables
  SELECT person_id,
         'is_diabetes_mellitus_drug_induced'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _first_competing_risk AS competing_risk,
                  events_of_interest AS event
            WHERE (competing_risk.person_id = index_event.person_id)
              AND (competing_risk.person_id = event.person_id)
              AND (competing_risk.event_type = 'diabetes_mellitus_diagnosis')
              AND (competing_risk.event_period && event.event_period)
              AND (event.concept_set_name LIKE
                    'Diagnoses;diabetes_mellitus;drug_induced;%')
         ))::INTEGER
    FROM sequences_of_interest AS index_event;

\echo '  calculating covariate #11, `mental_procedure_before_period_3`'

INSERT INTO variables
  SELECT person_id,
         'mental_procedure_before_period_3'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM events_of_interest AS event
            WHERE (event.person_id = index_event.person_id)
                  -- this event ended between periods 1 and 3
              AND (upper(event.event_period) - 1
                    BETWEEN lower(index_event.period_1)
                        AND index_event.period_3 - 1)
                  -- this event is associated to a concept set of interest
              AND (event.concept_set_name LIKE
                    'Procedures;mental_procedure;%')
           ))::INTEGER
    FROM sequences_of_interest AS index_event;

\echo '  calculating covariate #12, `type_of_first_competing_risk`'

INSERT INTO variables
  SELECT person_id,
         'type_of_first_competing_risk'::TEXT,
         event_type
    FROM _first_competing_risk;

/******************************************************************************/

\echo '* exporting {{work_schema}}.variables'

\! rm -f {{work_schema}}.variables.csv.gz

CREATE TEMPORARY VIEW _variables AS
    SELECT *
      FROM variables
  ORDER BY person_id,
           variable_name;

\copy (SELECT * FROM _variables) TO '{{work_schema}}.variables.csv' WITH CSV HEADER

\! gzip -9 {{work_schema}}.variables.csv
