-- Identify drug exposures of interest that follow index prescription dates

-- The difference between this script and `b5_get_therapy_eras.sql` is that we do not
-- attempt to merge drug exposures into eras, deferring this to non-SQL post-processing.
-- We merely list all the drug exposures, grouped by ingredient and dosage

\echo '* exporting hospitalization periods'

CREATE TEMPORARY TABLE _hospitalizations AS
  SELECT op_start.person_id,
         metavisit.metavisit_period
    FROM metavisits_of_interest AS metavisit,
         sequences_of_interest AS op_start
   WHERE (metavisit.person_id = op_start.person_id)
         -- this metavisit started after the
         -- patient's index prescription date
     AND (lower(metavisit.metavisit_period) >= op_start.period_3)
         -- this metavisit includes an hospitalization or an ER visit
     AND (metavisit.visit_types && '{inpatient,emergency}'::TEXT[]);

\! rm -f {{work_schema}}.hospitalizations.csv.gz

\copy (SELECT * FROM _hospitalizations) TO '{{work_schema}}.hospitalizations.csv' WITH CSV HEADER

\! pigz -9 {{work_schema}}.hospitalizations.csv

\echo '* exporting boundaries'

-- date of period 3
CREATE TEMPORARY TABLE _lower_bounds AS
  SELECT person_id,
         period_3 AS date
    FROM sequences_of_interest;

-- date of the first competing risk
CREATE TEMPORARY TABLE _upper_bounds AS
    SELECT DISTINCT ON (person_id)
           person_id,
           lower(event_period) AS date
      FROM competing_risks
  ORDER BY person_id,
           lower(event_period) ASC;

CREATE TEMPORARY VIEW _boundaries AS
     SELECT person_id,
            lower_bound.date AS lower_bound,
            upper_bound.date AS upper_bound
       FROM _lower_bounds AS lower_bound
  LEFT JOIN _upper_bounds AS upper_bound
      USING (person_id);

\! rm -f {{work_schema}}.boundaries.csv.gz

\copy (SELECT * FROM _boundaries) TO '{{work_schema}}.boundaries.csv' WITH CSV HEADER

\! pigz -9 {{work_schema}}.boundaries.csv

\echo '* staging drug exposures'

\echo '  creating _ingredient_exposures table'

-- list drug exposures for each of the ingredient-level
-- concept identifiers associated with a concept_set_name
-- and happening after a patient's index prescription date
DROP TABLE IF EXISTS _ingredient_exposures CASCADE;
CREATE TABLE _ingredient_exposures AS
  WITH
    -- list drug exposures (in a way similar to events_of_interest, but limiting
    -- ourselves to members of the 'sequences_of_interest' table and walking
    -- ancestor drug concept ids until we find the ingredient-level concept id)
    _per_ingredient_exposures AS (
       SELECT drug_exposure.person_id,
              drug_exposure.drug_concept_id,
              concept.concept_id AS ingredient_concept_id,
              codex_period(
                drug_exposure.drug_exposure_start_date,
                coalesce(
                  drug_exposure.drug_exposure_end_date,
                  drug_exposure.drug_exposure_start_date
                    + abs(coalesce(drug_exposure.days_supply, 1))
                  )) AS period,
              drug_exposure.drug_exposure_id
         FROM {{cdm_schema}}.drug_exposure
         JOIN sequences_of_interest USING (person_id)
         JOIN {{cdm_schema}}.concept_ancestor
           ON (concept_ancestor.descendant_concept_id = drug_exposure.drug_concept_id)
         JOIN {{cdm_schema}}.concept
           ON (concept_ancestor.ancestor_concept_id = concept.concept_id)
        WHERE (concept.vocabulary_id = 'RxNorm')
          AND (concept.concept_class_id = 'Ingredient')
          AND (drug_exposure.drug_concept_id != 0)
      ),
    -- map the ingredient-level concept ids to concept set names
    _per_concept_exposures AS (
          SELECT exposure.*,
                 concept_set.concept_set_name
            FROM _per_ingredient_exposures AS exposure
       LEFT JOIN concept_sets_of_interest AS concept_set
              ON (concept_set.concept_id = exposure.ingredient_concept_id)
                 -- we want a drug of interest
           WHERE (concept_set.concept_set_name LIKE 'Drugs;drug_of_interest;%')
      )
  SELECT op_start.person_id,
         drug_exposure.concept_set_name,
         drug_exposure.drug_concept_id,
         drug_exposure.ingredient_concept_id,
         -- we truncate the exposure period to
         -- make it start no earlier than period 3
         codex_period(
           greatest(op_start.period_3, lower(drug_exposure.period)),
           upper(drug_exposure.period) - 1
           ) AS period,
         drug_exposure.drug_exposure_id
    FROM sequences_of_interest AS op_start,
         _per_concept_exposures AS drug_exposure
   WHERE (drug_exposure.person_id = op_start.person_id)
         -- we want the exposure to this drug to have ended
         -- after the patient's index prescription date
     AND ((upper(drug_exposure.period) - 1) > op_start.period_3);

CREATE INDEX ON _ingredient_exposures (drug_exposure_id);

\echo '  creating _drug_strength_per_exposure table'

-- list drug strength for these ingredient exposures
DROP TABLE IF EXISTS _drug_strength_per_exposure CASCADE;
CREATE TABLE _drug_strength_per_exposure AS
    SELECT ingredient_exposure.*,
           drug_strength.amount_value,
           drug_strength.amount_unit_concept_id,
           drug_strength.numerator_value,
           drug_strength.numerator_unit_concept_id,
           drug_strength.denominator_unit_concept_id
      FROM _ingredient_exposures AS ingredient_exposure
 LEFT JOIN {{cdm_schema}}.drug_strength AS drug_strength
     USING (drug_concept_id)
  ORDER BY ingredient_exposure.person_id;

\echo '* exporting results'

\! rm -f {{work_schema}}.ingredient_exposures.csv.gz

\copy (SELECT * FROM _drug_strength_per_exposure) TO '{{work_schema}}.ingredient_exposures.csv' WITH CSV HEADER

\! gzip -9 {{work_schema}}.ingredient_exposures.csv

DROP TABLE _ingredient_exposures CASCADE;
DROP TABLE _drug_strength_per_exposure CASCADE;
