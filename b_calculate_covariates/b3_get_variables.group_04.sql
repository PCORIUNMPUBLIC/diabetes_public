
\echo '    staging events for covariates #4*'

\echo '      creating _drugs_during_periods_1_and_2'

DROP TABLE IF EXISTS _drugs_during_periods_1_and_2 CASCADE;
CREATE TEMPORARY TABLE _drugs_during_periods_1_and_2 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM sequences_of_interest AS index_event,
         events_of_interest AS event
   WHERE (index_event.person_id = event.person_id)
     AND (event.concept_set_name LIKE 'Drugs;concomitant;%')
     AND (lower(event.event_period) >= lower(index_event.period_1))
     AND (lower(event.event_period) < upper(index_event.period_2) - 1);

CREATE INDEX ON _drugs_during_periods_1_and_2 (person_id);
CREATE INDEX ON _drugs_during_periods_1_and_2 (concept_set_name);
CREATE INDEX ON _drugs_during_periods_1_and_2 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '    calculating covariate #4(a), `exposure_to_opioid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_opioid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;analgesic;opioid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(b), `exposure_to_nsaid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_nsaid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;nsaid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(c), `exposure_to_non_narcotic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_non_narcotic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;analgesic;non_narcotic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(d), `exposure_to_sedative_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_sedative_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;hypnotic_sedativ;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(e), `exposure_to_antiasthmatic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antiasthmatic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_asthma;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(f), `exposure_to_antianxiety_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antianxiety_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_anxiety;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(g), `exposure_to_benzodiazepine_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_benzodiazepine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;benzodiazepine;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(h), `exposure_to_anticonvulsant_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_anticonvulsant_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_convulsant;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(i), `exposure_to_antibacterial_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antibacterial_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_bacterial;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(j), `exposure_to_fluoroquinolon_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_fluoroquinolon_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;fluoroquinolon;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(k), `exposure_to_antiviral_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antiviral_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_viral;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(l), `exposure_to_anti_hiv_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_anti_hiv_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_hiv;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(m), `exposure_to_antitubercular_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antitubercular_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_tubercular;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(n), `exposure_to_ace_inhibitor_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_ace_inhibitor_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;ace_inhibitor;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(o), `exposure_to_diuretic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_diuretic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;diuretic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(p), `exposure_to_loop_diuretic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_loop_diuretic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;loop_diuretic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(q), `exposure_to_thiazide_diuretic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_thiazide_diuretic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;thiazid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(r), `exposure_to_beta_blocker_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_beta_blocker_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;beta_blocker;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(s), `exposure_to_anti_arrhythmia_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_anti_arrhythmia_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_arrhytmic;%') -- note the typo here
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(t), `exposure_to_calcium_channel_blocker_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_calcium_channel_blocker_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;calcium_channel;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(u), `exposure_to_cns_stimulant_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_cns_stimulant_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;cns_stimulant;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(v), `exposure_to_alcohol_deterrent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_alcohol_deterrent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;alcohol_deterrent;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(w), `exposure_to_narcotic_antagonist_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_narcotic_antagonist_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;narcotic_antagonist;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(x), `exposure_to_hormone_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_hormone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;hormone;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(y), `exposure_to_androgen_antagonist_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_androgen_antagonist_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;androgen_antagonist;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(z), `exposure_to_estrogen_antagonist_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_estrogen_antagonist_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;estrogen_antagonist;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(aa), `exposure_to_estrogen_receptor_modulator_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_estrogen_receptor_modulator_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;estrogen_receptor_modulator;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(bb), `exposure_to_prostaglandin_antagonist_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_prostaglandin_antagonist_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;prostaglandin_antagonist;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(cc), `exposure_to_androgen_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_androgen_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;androgen;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(dd), `exposure_to_estrogen_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_estrogen_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;estrogen;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ee), `exposure_to_progestin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_progestin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;progestin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ff), `exposure_to_reproductive_control_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_reproductive_control_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;reproductive_control;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(gg), `exposure_to_glucocorticoid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_glucocorticoid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;glucocorticoid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(hh), `exposure_to_thyroid_hormone_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_thyroid_hormone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;thyroid_hormone;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ii), `exposure_to_anti_thyroid_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_anti_thyroid_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_thyroid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(jj), `exposure_to_somatostatin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_somatostatin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;hormone;somatostatin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(kk), `exposure_to_growth_hormone_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_growth_hormone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;hormone;growth_hormone;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ll), `exposure_to_p450_inducer_or_inhibitor_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_p450_inducer_or_inhibitor_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (
                      (drug_exposure.concept_set_name LIKE
                        'Drugs;concomitant;cytochrome_p450_inducer;%')
                   OR (drug_exposure.concept_set_name LIKE
                        'Drugs;concomitant;cytochrome_p450_inhibitor;%')
                  )
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(mm), `exposure_to_antihistamine_h1_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antihistamine_h1_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_histamine_h1;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(nn), `exposure_to_antineoplastic_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antineoplastic_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;antineoplastic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(oo), `exposure_to_immunosuppressive_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_immunosuppressive_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;immunosuppressive;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(pp), `exposure_to_antihistamine_h2_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antihistamine_h2_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;anti_histamine_h2;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(qq), `exposure_to_bone_density_conservation_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_bone_density_conservation_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;bone_density_conservation;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(rr), `exposure_to_sulfa_drug_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_sulfa_drug_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;sulfa_drugs;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ss), `exposure_to_hypolipidemic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_hypolipidemic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;hypolipidemic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(tt), `exposure_to_statin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_statin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;hypolipidemic;statin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(uu), `exposure_to_niacin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_niacin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;hypolipidemic;niacin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(vv), `exposure_to_pentamidine_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_pentamidine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'Drugs;concomitant;pentamidine;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

/**
Test: for any combination of patient_id and therapy_name,
      each of these covariates should have a value

  SELECT variable_name,
         count(CASE WHEN variable_value::INTEGER = 1 THEN 1 END) AS n_yes,
         count(CASE WHEN variable_value::INTEGER = 0 THEN 1 END) AS n_no,
         count(*) AS n_total
    FROM variables
   WHERE (variable_name LIKE 'exposure_to_%_during_period_1_or_2')
GROUP BY variable_name
ORDER BY variable_name;
**/
