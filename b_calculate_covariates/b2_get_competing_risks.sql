
SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.competing_risks'

DROP TABLE IF EXISTS competing_risks CASCADE;
CREATE TABLE competing_risks
  (
      person_id BIGINT NOT NULL,
   event_period DATERANGE NOT NULL,
     event_type TEXT NOT NULL
  );

COMMENT ON TABLE competing_risks IS '{{pipeline_version}}';

\echo '  listing competing risk #1, `diabetes_mellitus_diagnosis`'

CREATE TEMPORARY TABLE _first_metavisit_with_diabetes_diagnosis AS
    SELECT DISTINCT ON (person_id)
           metavisit.*
      FROM metavisits_of_interest AS metavisit
      JOIN sequences_of_interest AS sequence
     USING (person_id)
     WHERE (lower(metavisit.metavisit_period) > sequence.period_3)
       AND (EXISTS (
            SELECT true
              FROM events_of_interest AS event
             WHERE (event.metavisit_occurrence_id
                     = metavisit.metavisit_occurrence_id)
               AND (event.concept_set_name LIKE
                     'Diagnoses;diabetes_mellitus;%')
           ))
  ORDER BY person_id,
           lower(metavisit_period);

CREATE INDEX ON _first_metavisit_with_diabetes_diagnosis (metavisit_occurrence_id);

INSERT INTO competing_risks
  SELECT person_id,
         metavisit_period,
         'diabetes_mellitus_diagnosis'::TEXT
    FROM _first_metavisit_with_diabetes_diagnosis AS metavisit
   WHERE (NOT EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (
                     (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreatitis;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_structural_anomaly;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;obstetric;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_injury;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_neoplasm;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_absence;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;%')
                 )
         ));

INSERT INTO competing_risks
  SELECT person_id,
         metavisit_period,
         'censoring_event'::TEXT
    FROM _first_metavisit_with_diabetes_diagnosis AS metavisit
   WHERE (EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (
                     (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreatitis;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_structural_anomaly;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;obstetric;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_injury;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_neoplasm;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_absence;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;%')
                 )
         ));

\echo '  listing competing risk #2, `hospitalization_without_diabetes_mellitus_diagnosis`'

CREATE TEMPORARY TABLE _first_hospitalization_without_diagnosis AS
    SELECT DISTINCT ON (person_id)
           metavisit.*
      FROM metavisits_of_interest AS metavisit
      JOIN sequences_of_interest AS sequence
     USING (person_id)
     WHERE (metavisit.visit_types && '{inpatient,emergency}'::TEXT[])
       AND (lower(metavisit.metavisit_period) > sequence.period_3)
       AND (NOT EXISTS (
            SELECT true
              FROM events_of_interest AS event
             WHERE (event.metavisit_occurrence_id
                     = metavisit.metavisit_occurrence_id)
               AND (event.concept_set_name LIKE
                     'Diagnoses;diabetes_mellitus;%')
           ))
  ORDER BY person_id,
           lower(metavisit_period);

CREATE INDEX ON _first_hospitalization_without_diagnosis (metavisit_occurrence_id);

INSERT INTO competing_risks
  SELECT person_id,
         metavisit_period,
         'hospitalization_without_diabetes_mellitus_diagnosis'::TEXT
    FROM _first_hospitalization_without_diagnosis AS metavisit
   WHERE (NOT EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (
                     (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreatitis;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_structural_anomaly;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;obstetric;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_injury;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_neoplasm;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_absence;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;%')
                 )
         ));

INSERT INTO competing_risks
  SELECT person_id,
         metavisit_period,
         'censoring_event'::TEXT
    FROM _first_hospitalization_without_diagnosis AS metavisit
   WHERE (EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (
                     (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreatitis;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_structural_anomaly;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;obstetric;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_injury;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_neoplasm;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreas_absence;%')
                  OR (event.concept_set_name LIKE
                       'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;%')
                 )
         ));

\echo '  listing competing risk #3, `censoring_event`'

-- death
INSERT INTO competing_risks
  SELECT op_start.person_id,
         codex_instant(event.death_date),
         'censoring_event'::TEXT
    FROM sequences_of_interest AS op_start
    JOIN {{cdm_schema}}.death AS event USING (person_id);

-- last recorded event
INSERT INTO competing_risks
  WITH
    _last_recorded_events AS (
         SELECT DISTINCT ON (op_start.person_id)
                op_start.person_id,
                event.event_period
           FROM sequences_of_interest AS op_start
           JOIN events_of_interest AS event USING (person_id)
       ORDER BY op_start.person_id,
                upper(event.event_period) DESC
      )
    SELECT person_id,
           codex_instant(upper(event_period) - 1),
           'censoring_event'::TEXT
      FROM _last_recorded_events AS event;

-- onset of excluded conditions
INSERT INTO competing_risks
  SELECT op_start.person_id,
         event.event_period,
         'censoring_event'::TEXT
    FROM sequences_of_interest AS op_start
    JOIN events_of_interest AS event USING (person_id)
   WHERE (
             (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;pancreatitis;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;pancreas_structural_anomaly;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;obstetric;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;pancreas_injury;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;pancreas_neoplasm;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;pancreas_absence;%')
          OR (event.concept_set_name LIKE
               'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;%')
         );

\echo '  indexing competing_risks'

\echo '    creating index on competing_risks.person_id'

CREATE INDEX ON competing_risks (person_id);

\echo '    creating index on competing_risks.event_period'

CREATE INDEX ON competing_risks USING GIST (event_period);

\echo '    creating index on competing_risks.event_type'

CREATE INDEX ON competing_risks (event_type);

\echo '  truncating competing_risks'

-- delete all competing risks that are not from patients in
-- `sequences_of_interest` (this should not be necessary)
DELETE
  FROM competing_risks AS op_stop
 USING (
           SELECT DISTINCT
                  op_stop.person_id
             FROM competing_risks AS op_stop
        LEFT JOIN sequences_of_interest AS op_start
            USING (person_id)
            WHERE (op_start.person_id IS NULL)
       ) AS orphan
 WHERE (op_stop.person_id = orphan.person_id);

-- delete all competing risks that started
-- on or before the index prescription date
DELETE
  FROM competing_risks AS op_stop
 USING sequences_of_interest AS op_start
 WHERE (op_start.person_id = op_stop.person_id)
   AND (lower(op_stop.event_period)
         <= op_start.period_3);

\echo '    analyzing competing_risks'

ANALYZE competing_risks;

/**
Test: each competing risk should have been found at least once

    SELECT event_type,
           count(DISTINCT person_id) AS n_persons
      FROM competing_risks
  GROUP BY event_type
  ORDER BY event_type;
**/
