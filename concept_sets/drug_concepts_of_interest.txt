Drugs
 +-- mental_procedure
 |   +-- drug_of_interest
 +-- concomitant
 |   +-- vasodilators
 |   +-- estrogen
 |   +-- anti_hiv
 |   +-- anti_viral
 |   |   +-- foscarnet
 |   +-- anti_arrhytmic
 |   +-- ace_inhibitor
 |   +-- sulfonylurea
 |   +-- carbapenem
 |   +-- hormone
 |   |   +-- somatostatin
 |   |   +-- growth_hormone
 |   +-- estrogen_receptor_modulator
 |   +-- thiazid
 |   +-- anti_tubercular
 |   |   +-- rifampin
 |   +-- penicillin
 |   +-- anti_asthma
 |   +-- loop_diuretic
 |   +-- fluoroquinolon
 |   +-- hypolipidemic
 |   |   +-- niacin
 |   |   +-- statin
 |   +-- reproductive_control
 |   +-- thyroid_hormone
 |   +-- cytochrome_p450_inducer
 |   +-- beta_blocker
 |   +-- macrolyd
 |   +-- bone_density_conservation
 |   |   +-- pamidronate
 |   +-- immunosuppressive
 |   |   +-- cyclosporine
 |   +-- androgen_antagonist
 |   +-- cytochrome_p450_inhibitor
 |   +-- glucocorticoid
 |   +-- analgesic
 |   |   +-- opioid
 |   |   |   +-- heroin
 |   |   +-- non_narcotic
 |   +-- anti_histamine_h2
 |   |   +-- ranitidine
 |   |   +-- cimetidine
 |   +-- gold_preparation
 |   +-- cephalosporin
 |   |   +-- second_generation
 |   |   +-- third_generation
 |   |   +-- first_generation
 |   |   +-- fourth_generation
 |   +-- cns_stimulant
 |   +-- penicillamine
 |   +-- nsaid
 |   +-- immunoglobulins
 |   +-- androgen
 |   +-- anti_histamine_h1
 |   +-- hypnotic_sedativ
 |   +-- anti_convulsant
 |   |   +-- phenytoin
 |   +-- aristolochic
 |   +-- calcium_channel
 |   +-- tetracyclin
 |   +-- aminoglycosid
 |   +-- sulfa_drugs
 |   +-- benzodiazepine
 |   +-- anti_bacterial
 |   |   +-- demeclocyclin
 |   |   +-- rifampin
 |   |   +-- amphotericin
 |   |   +-- ciprofloxacin
 |   |   +-- vancomycin
 |   +-- anti_anxiety
 |   +-- pentamidine
 |   +-- antineoplastic
 |   |   +-- cyclophosphamide
 |   |   +-- cisplatin
 |   +-- alcohol_deterrent
 |   +-- beta_lactamase_inhibitors
 |   +-- prostaglandin_antagonist
 |   +-- progestin
 |   +-- narcotic_antagonist
 |   +-- estrogen_antagonist
 |   +-- fluoride
 |   +-- contrast_media
 |   +-- anti_thyroid
 |   +-- diuretic
 |       +-- mannitol
 |       +-- furosemide
 +-- exclusion
 |   +-- memantine
 |   +-- cholinesterase_inhibitor
 |   +-- drug
 |   |   +-- hypoglycemic
 |   +-- diabetes
 |       +-- hypoglycemic
 |           +-- insulin
 +-- drug_of_interest
     +-- lithium_carbonate
     +-- first_generation_antipsychotic
     |   +-- haloperidol
     |   +-- fluphenazine
     |   +-- thiothixene
     |   +-- promazine
     |   +-- pimozide
     |   +-- perphenazine
     |   +-- trifluoperazine
     |   +-- methotrimeprazine
     |   +-- perazine
     |   +-- triflupromazine
     |   +-- loxapine
     |   +-- mesoridazine
     |   +-- chlorprothixene
     |   +-- molindone
     |   +-- thioridazine
     |   +-- chlorpromazine
     +-- clozapine
     +-- second_generation_antipsychotic
     |   +-- lurasidone
     |   +-- brexpiprazole
     |   +-- risperidone
     |   +-- asenapine
     |   +-- quetiapine
     |   +-- aripiprazole
     |   +-- ziprasidone
     |   +-- cariprazine
     |   +-- olanzapine
     |   +-- paliperidone
     |   +-- iloperidone
     +-- anti_depressant
     |   +-- ndri
     |   |   +-- bupropion
     |   +-- snri
     |   |   +-- venlafaxine
     |   |   +-- milnacipran
     |   |   +-- duloxetine
     |   |   +-- desvenlafaxine
     |   |   +-- levomilnacipran
     |   +-- ssri
     |   |   +-- vortioxetine
     |   |   +-- paroxetine
     |   |   +-- escitalopram
     |   |   +-- fluoxetine
     |   |   +-- citalopram
     |   |   +-- fluvoxamine
     |   |   +-- vilazodone
     |   |   +-- sertraline
     |   +-- nassa
     |   |   +-- mirtazapine
     |   +-- tri_and_tetracyclic
     |   |   +-- trimipramine
     |   |   +-- amoxapine
     |   |   +-- imipramine
     |   |   +-- clomipramine
     |   |   +-- desipramine
     |   |   +-- maprotiline
     |   |   +-- doxepin
     |   |   +-- protriptyline
     |   +-- mao
     |       +-- phenelzine
     |       +-- selegiline
     |       +-- isocarboxazid
     |       +-- tranylcypromine
     +-- mood_stabilizer
         +-- valproate
         +-- lamotrigine
         +-- oxcarbazepine
         +-- carbamazepine