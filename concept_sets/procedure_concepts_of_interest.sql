
-- Infusion set for external insulin pump, non needle cannula type (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614703);

-- Infusion set for external insulin pump, needle type (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614704);

-- Syringe with needle for external insulin pump, sterile, 3 cc (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2614705);

-- External ambulatory insulin delivery system, disposable, each, includes all supplies and accessories (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2615310);

-- External ambulatory infusion pump, insulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2616754);

-- Outpt iv insulin tx any mea (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664549);

-- Injection, insulin, per 5 units (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718458);

-- Insulin for administration through dme (i.e., insulin pump) per 50 units (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718459);

-- Insulin, rapid onset, 5 units (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721331);

-- Insulin, most rapid onset (lispro or aspart); 5 units (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721332);

-- Insulin, intermediate acting (nph or lente); 5 units (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721333);

-- Insulin, long acting; 5 units (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721334);

-- Insulin delivery device, reusable pen; 1.5 ml size (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721335);

-- Insulin delivery device, reusable pen; 3 ml size (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721336);

-- Insulin cartridge for use in insulin delivery device other than pump; 150 units (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721337);

-- Insulin cartridge for use in insulin delivery device other than pump; 300 units (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721338);

-- Insulin delivery device, disposable pen (including insulin); 1.5 ml size (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721339);

-- Insulin delivery device, disposable pen (including insulin); 3 ml size (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721340);

-- Insulin syringes (100 syringes, any size) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721411);

-- Insulin pump initiation, instruction in initial use of pump (pump not included) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721452);

-- Home infusion therapy, continuous insulin infusion therapy; administrative services, professional pharmacy services, care coordination, and all necessary supplies and equipment (drugs and nursing visits coded separately), per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721493);

-- Injection of insulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2008262);

-- Introduction of Insulin into Peripheral Vein, Percutaneous Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786436);

-- Introduction of Insulin into Central Vein, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786464);

-- Introduction of Insulin into Central Vein, Percutaneous Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786683);

-- Introduction of Insulin into Peripheral Artery, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786711);

-- Introduction of Insulin into Peripheral Artery, Percutaneous Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786739);

-- Introduction of Insulin into Central Artery, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786957);

-- Introduction of Insulin into Central Artery, Percutaneous Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786985);

-- Introduction of Insulin into Subcutaneous Tissue, Percutaneous Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786174);

-- Introduction of Insulin into Peripheral Vein, Open Approach (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2786214);

-- Unlisted psychiatric service or procedure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213561);

-- Psychiatry Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889674);

-- Other Psychiatric Services or Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887735);

-- Mental health services, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618145);

-- Psychiatric health facility service, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618163);

-- Intensive outpatient psychiatric services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721547);

-- Mental health partial hospitalization, treatment, less than 24 hours (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618134);

-- Mental health service plan development by non-physician (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618131);

-- Mental Health (Procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900282);

-- Mental Health @ None (Procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894430);

-- Alcohol and drug rehabilitation and detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007785);

-- Alcohol rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007786);

-- Alcohol detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007787);

-- Alcohol rehabilitation and detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007788);

-- Drug rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007789);

-- Drug detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007790);

-- Drug rehabilitation and detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007791);

-- Combined alcohol and drug rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007802);

-- Combined alcohol and drug detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007803);

-- Combined alcohol and drug rehabilitation and detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007804);

-- Drug addiction counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007765);

-- Alcoholism counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007766);

-- Referral for alcoholism rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007771);

-- Referral for drug addiction rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007782);

-- Alcohol and/or substance (other than tobacco) abuse structured screening (eg, AUDIT, DAST), and brief intervention (SBI) services; 15 to 30 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2514536);

-- Alcohol and/or substance (other than tobacco) abuse structured screening (eg, AUDIT, DAST), and brief intervention (SBI) services; greater than 30 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2514537);

-- Patient screened for unhealthy alcohol use using a systematic screening method (PV) (DSP) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106161);

-- Patient counseled regarding psychosocial and pharmacologic treatment options for opioid addiction (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108850);

-- Patient counseled regarding psychosocial and pharmacologic treatment options for alcohol dependence (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108871);

-- Alcohol and/or substance (other than tobacco) abuse structured screening (eg, AUDIT, DAST), and brief intervention (SBI) services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888215);

-- Patient screened for depression (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2102214);

-- Alcohol and/or drug services; group counseling by a clinician (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618104);

-- Alcohol and/or drug services; case management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618105);

-- Alcohol and/or drug services; crisis intervention (outpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618106);

-- Alcohol and/or drug services; sub-acute detoxification (hospital inpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618107);

-- Alcohol and/or drug services; acute detoxification (hospital inpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618108);

-- Alcohol and/or drug services; sub-acute detoxification (residential addiction program inpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618109);

-- Alcohol and/or drug services; acute detoxification (residential addiction program inpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618110);

-- Alcohol and/or drug services; sub-acute detoxification (residential addiction program outpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618111);

-- Alcohol and/or drug services; acute detoxification (residential addiction program outpatient) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618112);

-- Alcohol and/or drug services; ambulatory detoxification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618113);

-- Alcohol and/or drug services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618114);

-- Alcohol and/or drug services; medical/somatic (medical intervention in ambulatory setting) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618115);

-- Alcohol and/or drug services; methadone administration and/or service (provision of the drug by a licensed program) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618119);

-- Alcohol and/or drug intervention service (planned facilitation) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618121);

-- Behavioral health outreach service (planned approach to reach a targeted population) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618122);

-- Alcohol and/or drug prevention environmental service (broad range of external activities geared toward modifying systems in order to mainstream prevention through policy and law) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618126);

-- Alcohol and/or drug prevention problem identification and referral service (e.g., student assistance and employee assistance programs), does not include assessment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618127);

-- Alcohol and/or other drug abuse services, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618146);

-- Alcohol and/or other drug testing: collection and handling only, specimens other than blood (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618147);

-- Alcohol and/or drug services, brief intervention, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618149);

-- Substance abuse program (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618194);

-- Opioid addiction treatment program (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618195);

-- Integrated mental health/substance abuse program (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618196);

-- Ambulatory setting substance abuse treatment or detoxification services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721545);

-- Alcohol and/or substance abuse services, family/couple counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721628);

-- Alcohol and/or substance abuse services, treatment plan development and/or modification (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721629);

-- Alcohol and/or substance abuse services, skills development (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721634);

-- Substance Abuse Treatment (Therapy) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801374);

-- Substance Abuse Treatment @ None (Therapy) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814217);

-- Substance Abuse Treatment, Detoxification Services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795858);

-- Substance Abuse Treatment @ None @ Detoxification Services @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821591);

-- Substance Abuse Treatment @ None @ Detoxification Services @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894438);

-- Substance Abuse Treatment @ None @ Detoxification Services @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814218);

-- Detoxification Services for Substance Abuse Treatment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795859);

-- Substance Abuse Treatment, Individual Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795860);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839393);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834820);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834821);

-- Individual Counseling for Substance Abuse Treatment, Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795861);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854454);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854455);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849572);

-- Individual Counseling for Substance Abuse Treatment, Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795862);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809351);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive-Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826848);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Cognitive-Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862955);

-- Individual Counseling for Substance Abuse Treatment, Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795863);

-- Substance Abuse Treatment @ None @ Individual Counseling @ 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862956);

-- Substance Abuse Treatment @ None @ Individual Counseling @ 12-Step @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875865);

-- Substance Abuse Treatment @ None @ Individual Counseling @ 12-Step @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849573);

-- Individual Counseling for Substance Abuse Treatment, 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795864);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849574);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Interpersonal @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889395);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Interpersonal @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826849);

-- Individual Counseling for Substance Abuse Treatment, Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795865);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862957);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Vocational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849575);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Vocational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834822);

-- Individual Counseling for Substance Abuse Treatment, Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795866);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875866);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Psychoeducation @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821592);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Psychoeducation @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821593);

-- Individual Counseling for Substance Abuse Treatment, Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795867);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821594);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Motivational Enhancement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854456);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Motivational Enhancement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894439);

-- Individual Counseling for Substance Abuse Treatment, Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795868);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854457);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Confrontational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854458);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Confrontational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868120);

-- Individual Counseling for Substance Abuse Treatment, Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795869);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Continuing Care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839394);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Continuing Care @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821595);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Continuing Care @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809352);

-- Individual Counseling for Substance Abuse Treatment, Continuing Care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795870);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Spiritual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839395);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Spiritual @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849576);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Spiritual @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839396);

-- Individual Counseling for Substance Abuse Treatment, Spiritual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795871);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Pre/Post-Test Infectious Disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839397);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Pre/Post-Test Infectious Disease @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862958);

-- Substance Abuse Treatment @ None @ Individual Counseling @ Pre/Post-Test Infectious Disease @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875992);

-- Individual Counseling for Substance Abuse Treatment, Pre/Post-Test Infectious Disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795872);

-- Substance Abuse Treatment, Group Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795873);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894440);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868121);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821596);

-- Group Counseling for Substance Abuse Treatment, Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795874);

-- Substance Abuse Treatment @ None @ Group Counseling @ Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875993);

-- Substance Abuse Treatment @ None @ Group Counseling @ Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821597);

-- Substance Abuse Treatment @ None @ Group Counseling @ Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834823);

-- Group Counseling for Substance Abuse Treatment, Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795875);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900289);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive-Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814219);

-- Substance Abuse Treatment @ None @ Group Counseling @ Cognitive-Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894441);

-- Group Counseling for Substance Abuse Treatment, Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795876);

-- Substance Abuse Treatment @ None @ Group Counseling @ 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854459);

-- Substance Abuse Treatment @ None @ Group Counseling @ 12-Step @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894442);

-- Substance Abuse Treatment @ None @ Group Counseling @ 12-Step @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875994);

-- Group Counseling for Substance Abuse Treatment, 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795877);

-- Substance Abuse Treatment @ None @ Group Counseling @ Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849577);

-- Substance Abuse Treatment @ None @ Group Counseling @ Interpersonal @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839398);

-- Substance Abuse Treatment @ None @ Group Counseling @ Interpersonal @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894443);

-- Group Counseling for Substance Abuse Treatment, Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795878);

-- Substance Abuse Treatment @ None @ Group Counseling @ Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881411);

-- Substance Abuse Treatment @ None @ Group Counseling @ Vocational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854460);

-- Substance Abuse Treatment @ None @ Group Counseling @ Vocational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900290);

-- Group Counseling for Substance Abuse Treatment, Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795879);

-- Substance Abuse Treatment @ None @ Group Counseling @ Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814220);

-- Substance Abuse Treatment @ None @ Group Counseling @ Psychoeducation @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875995);

-- Substance Abuse Treatment @ None @ Group Counseling @ Psychoeducation @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862959);

-- Group Counseling for Substance Abuse Treatment, Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795880);

-- Substance Abuse Treatment @ None @ Group Counseling @ Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839399);

-- Substance Abuse Treatment @ None @ Group Counseling @ Motivational Enhancement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834824);

-- Substance Abuse Treatment @ None @ Group Counseling @ Motivational Enhancement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839400);

-- Group Counseling for Substance Abuse Treatment, Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795881);

-- Substance Abuse Treatment @ None @ Group Counseling @ Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849578);

-- Substance Abuse Treatment @ None @ Group Counseling @ Confrontational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849579);

-- Substance Abuse Treatment @ None @ Group Counseling @ Confrontational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839401);

-- Group Counseling for Substance Abuse Treatment, Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795882);

-- Substance Abuse Treatment @ None @ Group Counseling @ Continuing Care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881412);

-- Substance Abuse Treatment @ None @ Group Counseling @ Continuing Care @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849580);

-- Substance Abuse Treatment @ None @ Group Counseling @ Continuing Care @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894444);

-- Group Counseling for Substance Abuse Treatment, Continuing Care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795883);

-- Substance Abuse Treatment @ None @ Group Counseling @ Spiritual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875996);

-- Substance Abuse Treatment @ None @ Group Counseling @ Spiritual @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839402);

-- Substance Abuse Treatment @ None @ Group Counseling @ Spiritual @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900291);

-- Group Counseling for Substance Abuse Treatment, Spiritual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795884);

-- Substance Abuse Treatment @ None @ Group Counseling @ Pre/Post-Test Infectious Disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868122);

-- Substance Abuse Treatment @ None @ Group Counseling @ Pre/Post-Test Infectious Disease @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821598);

-- Substance Abuse Treatment @ None @ Group Counseling @ Pre/Post-Test Infectious Disease @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862960);

-- Group Counseling for Substance Abuse Treatment, Pre/Post-Test Infectious Disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795885);

-- Substance Abuse Treatment, Individual Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795886);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839403);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834825);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875997);

-- Individual Psychotherapy for Substance Abuse Treatment, Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795887);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862961);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900292);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854461);

-- Individual Psychotherapy for Substance Abuse Treatment, Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795888);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889396);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive-Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894445);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Cognitive-Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875998);

-- Individual Psychotherapy for Substance Abuse Treatment, Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795889);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900293);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ 12-Step @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854462);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ 12-Step @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849581);

-- Individual Psychotherapy for Substance Abuse Treatment, 12-Step (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795890);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854463);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interpersonal @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862962);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interpersonal @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881413);

-- Individual Psychotherapy for Substance Abuse Treatment, Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795891);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interactive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900294);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interactive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801375);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Interactive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809353);

-- Individual Psychotherapy for Substance Abuse Treatment, Interactive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795892);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854464);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoeducation @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889397);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoeducation @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889398);

-- Individual Psychotherapy for Substance Abuse Treatment, Psychoeducation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795893);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821599);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Motivational Enhancement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814221);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Motivational Enhancement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881414);

-- Individual Psychotherapy for Substance Abuse Treatment, Motivational Enhancement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795894);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801376);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Confrontational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875999);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Confrontational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889399);

-- Individual Psychotherapy for Substance Abuse Treatment, Confrontational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795895);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Supportive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889400);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Supportive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814222);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Supportive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889401);

-- Individual Psychotherapy for Substance Abuse Treatment, Supportive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795896);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834826);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoanalysis @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900295);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychoanalysis @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894446);

-- Individual Psychotherapy for Substance Abuse Treatment, Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795897);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychodynamic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821600);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychodynamic @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849582);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychodynamic @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854465);

-- Individual Psychotherapy for Substance Abuse Treatment, Psychodynamic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795898);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychophysiological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801377);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychophysiological @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889402);

-- Substance Abuse Treatment @ None @ Individual Psychotherapy @ Psychophysiological @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876000);

-- Individual Psychotherapy for Substance Abuse Treatment, Psychophysiological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795899);

-- Substance Abuse Treatment, Family Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795900);

-- Substance Abuse Treatment @ None @ Family Counseling @ Other Family Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889403);

-- Substance Abuse Treatment @ None @ Family Counseling @ Other Family Counseling @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826850);

-- Substance Abuse Treatment @ None @ Family Counseling @ Other Family Counseling @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809354);

-- Family Counseling for Substance Abuse Treatment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795901);

-- Substance Abuse Treatment, Medication Management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795902);

-- Substance Abuse Treatment @ None @ Medication Management @ Nicotine Replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889404);

-- Substance Abuse Treatment @ None @ Medication Management @ Nicotine Replacement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826851);

-- Substance Abuse Treatment @ None @ Medication Management @ Nicotine Replacement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801378);

-- Medication Management for Substance Abuse Treatment, Nicotine Replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795903);

-- Substance Abuse Treatment @ None @ Medication Management @ Methadone Maintenance (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889405);

-- Substance Abuse Treatment @ None @ Medication Management @ Methadone Maintenance @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900296);

-- Substance Abuse Treatment @ None @ Medication Management @ Methadone Maintenance @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854466);

-- Medication Management for Substance Abuse Treatment, Methadone Maintenance (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796047);

-- Substance Abuse Treatment @ None @ Medication Management @ Levo-alpha-acetyl-methadol (LAAM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862963);

-- Substance Abuse Treatment @ None @ Medication Management @ Levo-alpha-acetyl-methadol (LAAM) @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876001);

-- Substance Abuse Treatment @ None @ Medication Management @ Levo-alpha-acetyl-methadol (LAAM) @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839404);

-- Medication Management for Substance Abuse Treatment, Levo-alpha-acetyl-methadol (LAAM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796048);

-- Substance Abuse Treatment @ None @ Medication Management @ Antabuse (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889406);

-- Substance Abuse Treatment @ None @ Medication Management @ Antabuse @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834827);

-- Substance Abuse Treatment @ None @ Medication Management @ Antabuse @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862964);

-- Medication Management for Substance Abuse Treatment, Antabuse (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796049);

-- Substance Abuse Treatment @ None @ Medication Management @ Naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900297);

-- Substance Abuse Treatment @ None @ Medication Management @ Naltrexone @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809355);

-- Substance Abuse Treatment @ None @ Medication Management @ Naltrexone @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826852);

-- Medication Management for Substance Abuse Treatment, Naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796050);

-- Substance Abuse Treatment @ None @ Medication Management @ Naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814223);

-- Substance Abuse Treatment @ None @ Medication Management @ Naloxone @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881415);

-- Substance Abuse Treatment @ None @ Medication Management @ Naloxone @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849583);

-- Medication Management for Substance Abuse Treatment, Naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796051);

-- Substance Abuse Treatment @ None @ Medication Management @ Clonidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881416);

-- Substance Abuse Treatment @ None @ Medication Management @ Clonidine @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854467);

-- Substance Abuse Treatment @ None @ Medication Management @ Clonidine @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801379);

-- Medication Management for Substance Abuse Treatment, Clonidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796052);

-- Substance Abuse Treatment @ None @ Medication Management @ Bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854468);

-- Substance Abuse Treatment @ None @ Medication Management @ Bupropion @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801380);

-- Substance Abuse Treatment @ None @ Medication Management @ Bupropion @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881417);

-- Medication Management for Substance Abuse Treatment, Bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796053);

-- Substance Abuse Treatment @ None @ Medication Management @ Psychiatric Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849584);

-- Substance Abuse Treatment @ None @ Medication Management @ Psychiatric Medication @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814224);

-- Substance Abuse Treatment @ None @ Medication Management @ Psychiatric Medication @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814225);

-- Medication Management for Substance Abuse Treatment, Psychiatric Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796054);

-- Substance Abuse Treatment @ None @ Medication Management @ Other Replacement Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814226);

-- Substance Abuse Treatment @ None @ Medication Management @ Other Replacement Medication @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876002);

-- Substance Abuse Treatment @ None @ Medication Management @ Other Replacement Medication @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821601);

-- Medication Management for Substance Abuse Treatment, Other Replacement Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796055);

-- Substance Abuse Treatment, Pharmacotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796056);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Nicotine Replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881418);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Nicotine Replacement @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849585);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Nicotine Replacement @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839405);

-- Pharmacotherapy for Substance Abuse Treatment, Nicotine Replacement (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796057);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Methadone Maintenance (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876003);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Methadone Maintenance @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801512);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Methadone Maintenance @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900298);

-- Pharmacotherapy for Substance Abuse Treatment, Methadone Maintenance (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796058);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Levo-alpha-acetyl-methadol (LAAM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900299);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Levo-alpha-acetyl-methadol (LAAM) @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862965);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Levo-alpha-acetyl-methadol (LAAM) @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849586);

-- Pharmacotherapy for Substance Abuse Treatment, Levo-alpha-acetyl-methadol (LAAM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796059);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Antabuse (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821602);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Antabuse @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849587);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Antabuse @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834828);

-- Pharmacotherapy for Substance Abuse Treatment, Antabuse (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796060);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814227);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naltrexone @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881419);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naltrexone @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881420);

-- Pharmacotherapy for Substance Abuse Treatment, Naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796061);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862966);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naloxone @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854469);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Naloxone @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839406);

-- Pharmacotherapy for Substance Abuse Treatment, Naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796062);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Clonidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849588);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Clonidine @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826853);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Clonidine @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894447);

-- Pharmacotherapy for Substance Abuse Treatment, Clonidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796063);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809356);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Bupropion @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2876004);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Bupropion @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849589);

-- Pharmacotherapy for Substance Abuse Treatment, Bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796064);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Psychiatric Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814228);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Psychiatric Medication @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862967);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Psychiatric Medication @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826854);

-- Pharmacotherapy for Substance Abuse Treatment, Psychiatric Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796065);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Other Replacement Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900300);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Other Replacement Medication @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889407);

-- Substance Abuse Treatment @ None @ Pharmacotherapy @ Other Replacement Medication @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881421);

-- Pharmacotherapy for Substance Abuse Treatment, Other Replacement Medication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;alcohol_drug;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2796066);

-- Narcosynthesis for psychiatric diagnostic and therapeutic purposes (eg, sodium amobarbital (Amytal) interview) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213551);

-- Psychologic evaluation and testing (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007690);

-- Administration of intelligence test (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007691);

-- Developmental screening, with interpretation and report, per standardized instrument form (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314189);

-- Developmental testing, (includes assessment of motor, language, social, adaptive, and/or cognitive functioning by standardized developmental instruments) with interpretation and report (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314190);

-- Administration of psychologic test (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007702);

-- Character analysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007703);

-- Other psychologic evaluation and testing (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007704);

-- Psychologic mental status determination, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007705);

-- Psychiatric mental status determination (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007707);

-- Other psychiatric interview and evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007710);

-- Routine psychiatric visit, not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007708);

-- Psychiatric interviews, consultations, and evaluations (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007706);

-- Psychiatric commitment evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007709);

-- Other counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007767);

-- Psychiatric diagnostic evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527984);

-- Psychiatric diagnostic evaluation with medical services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527985);

-- Psychiatric diagnostic interview examination (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213518);

-- Interactive psychiatric diagnostic interview examination using play equipment, physical devices, language interpreter, or other mechanisms of communication (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213519);

-- Psychological testing (includes psychodiagnostic assessment of personality, psychopathology, emotionality, intellectual abilities, eg, WAIS-R, Rorschach, MMPI) with interpretation and report, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738926);

-- Psychological testing (includes psychodiagnostic assessment of emotionality, intellectual abilities, personality and psychopathology, eg, MMPI, Rorschach, WAIS), per hour of the psychologist's or physician's time, both face-to-face time administering test (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314185);

-- Psychological testing (includes psychodiagnostic assessment of emotionality, intellectual abilities, personality and psychopathology, eg, MMPI and WAIS), with qualified health care professional interpretation and report, administered by technician, per ho (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314186);

-- Psychological testing (includes psychodiagnostic assessment of emotionality, intellectual abilities, personality and psychopathology, eg, MMPI), administered by a computer, with qualified health care professional interpretation and report (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314187);

-- Neurobehavioral status exam (clinical assessment of thinking, reasoning and judgment, eg, acquired knowledge, attention, memory, visual spatial abilities, language functions, planning) with interpretation and report, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738930);

-- Neurobehavioral status exam (clinical assessment of thinking, reasoning and judgment, eg, acquired knowledge, attention, language, memory, planning and problem solving, and visual spatial abilities), per hour of the psychologist's or physician's time, bot (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314192);

-- Neuropsychological testing battery (eg, Halstead-Reitan, Luria, WAIS-R) with interpretation and report, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42738931);

-- Neuropsychological testing (eg, Halstead-Reitan Neuropsychological Battery, Wechsler Memory Scales and Wisconsin Card Sorting Test), per hour of the psychologist's or physician's time, both face-to-face time administering tests to the patient and time int (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314194);

-- Neuropsychological testing (eg, Halstead-Reitan Neuropsychological Battery, Wechsler Memory Scales and Wisconsin Card Sorting Test), with qualified health care professional interpretation and report, administered by technician, per hour of technician time (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314195);

-- Neuropsychological testing (eg, Wisconsin Card Sorting Test), administered by a computer, with qualified health care professional interpretation and report (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314196);

-- Standardized cognitive performance testing (eg, Ross Information Processing Assessment) per hour of a qualified health care professional's time, both face-to-face time administering tests to the patient and time interpreting these test results and prepari (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314197);

-- Brief emotional/behavioral assessment (eg, depression inventory, attention-deficit/hyperactivity disorder [ADHD] scale), with scoring and documentation, per standardized instrument (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257498);

-- Health and behavior assessment (eg, health-focused clinical interview, behavioral observations, psychophysiological monitoring, health-oriented questionnaires), each 15 minutes face-to-face with the patient; initial assessment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314198);

-- Health and behavior assessment (eg, health-focused clinical interview, behavioral observations, psychophysiological monitoring, health-oriented questionnaires), each 15 minutes face-to-face with the patient; re-assessment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314199);

-- Psychiatric Diagnostic Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888161);

-- Health and behavior assessment (eg, health-focused clinical interview, behavioral observations, psychophysiological monitoring, health-oriented questionnaires), each 15 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887787);

-- Patient screened for depression (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2102214);

-- Negative screen for depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107225);

-- No significant depressive symptoms as categorized by using a standardized depression assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107229);

-- Mild to moderate depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- Clinically significant depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

-- Suicide risk assessed (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106303);

-- Brief emotional/behavioral assessment (eg, depression inventory, attention-deficit/hyperactivity disorder [ADHD] scale), with scoring and documentation, per standardized instrument (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257498);

-- PATIENT DOCUMENTED TO HAVE MENTAL STATUS ASSESSED (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617772);

-- Mental status assessed (CAP) (EM) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2102833);

-- One or more neuropsychiatric symptoms (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533178);

-- Mental health assessment, by non-physician (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618130);

-- Suicide risk assessed at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533176);

-- Screening for clinical depression is documented as negative, a follow-up plan is not required (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617909);

-- Screening for clinical depression is documented as being positive and a follow-up plan is documented (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

-- Screening for clinical depression documented as positive, follow-up plan not documented, reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

-- Screening for clinical depression documented as positive, a follow-up plan not documented, documentation stating the patient is not eligible (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

-- Assessment of depression severity at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533277);

-- Annual depression screening, 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664726);

-- Central Nervous System Assessments/Tests (eg, Neuro-Cognitive, Mental Status, Speech Testing) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888918);

-- Psychiatric evaluation of hospital records, other psychiatric reports, psychometric and/or projective tests, and other accumulated data for medical diagnostic purposes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213558);

-- Mental Health, Psychological Tests (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795665);

-- Mental Health @ None @ Psychological Tests @ Developmental (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900283);

-- Mental Health @ None @ Psychological Tests @ Developmental @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900284);

-- Mental Health @ None @ Psychological Tests @ Developmental @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801370);

-- Psychological Tests, Developmental (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795666);

-- Mental Health @ None @ Psychological Tests @ Personality and Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814211);

-- Mental Health @ None @ Psychological Tests @ Personality and Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839385);

-- Mental Health @ None @ Psychological Tests @ Personality and Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834815);

-- Psychological Tests, Personality and Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795667);

-- Mental Health @ None @ Psychological Tests @ Intellectual and Psychoeducational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814212);

-- Mental Health @ None @ Psychological Tests @ Intellectual and Psychoeducational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834816);

-- Mental Health @ None @ Psychological Tests @ Intellectual and Psychoeducational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862946);

-- Psychological Tests, Intellectual and Psychoeducational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795668);

-- Mental Health @ None @ Psychological Tests @ Neuropsychological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2867987);

-- Mental Health @ None @ Psychological Tests @ Neuropsychological @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839386);

-- Mental Health @ None @ Psychological Tests @ Neuropsychological @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862947);

-- Psychological Tests, Neuropsychological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795669);

-- Mental Health @ None @ Psychological Tests @ Neurobehavioral and Cognitive Status (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862948);

-- Mental Health @ None @ Psychological Tests @ Neurobehavioral and Cognitive Status @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875858);

-- Mental Health @ None @ Psychological Tests @ Neurobehavioral and Cognitive Status @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814213);

-- Psychological Tests, Neurobehavioral and Cognitive Status (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;assessment;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795670);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007723);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007724);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108577);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257488);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257654);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257555);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212118);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816350);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108573);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257739);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257410);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257697);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257553);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212119);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212122);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212106);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212111);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257696);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257552);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257738);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257591);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212113);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816347);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816361);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889818);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212108);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212109);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816351);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212121);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212114);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890740);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617571);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786399);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617574);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40659692);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664583);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718424);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718425);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2720963);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718258);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786557);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718568);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718648);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718605);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718549);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786380);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718621);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721001);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718315);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718584);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718633);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2720945);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890662);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890663);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890665);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664472);

-- Subconvulsive electroshock therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007727);

-- Other electroshock therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007728);

-- Anesthesia for electroconvulsive therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2100627);

-- Electroconvulsive therapy (includes necessary monitoring) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213552);

-- Electroconvulsive therapy (includes necessary monitoring); multiple seizures, per day (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42732301);

-- Electroconvulsive therapy (ECT) provided (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108578);

-- Mental Health, Electroconvulsive Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795842);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Single Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875862);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Single Seizure @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854452);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Single Seizure @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854453);

-- Electroconvulsive Therapy, Unilateral-Single Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795843);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Multiple Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881409);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Multiple Seizure @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809347);

-- Mental Health @ None @ Electroconvulsive Therapy @ Unilateral-Multiple Seizure @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839391);

-- Electroconvulsive Therapy, Unilateral-Multiple Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795844);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Single Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834818);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Single Seizure @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900287);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Single Seizure @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862952);

-- Electroconvulsive Therapy, Bilateral-Single Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795845);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Multiple Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839392);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Multiple Seizure @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875863);

-- Mental Health @ None @ Electroconvulsive Therapy @ Bilateral-Multiple Seizure @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862953);

-- Electroconvulsive Therapy, Bilateral-Multiple Seizure (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795846);

-- Mental Health @ None @ Electroconvulsive Therapy @ Other Electroconvulsive Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889393);

-- Mental Health @ None @ Electroconvulsive Therapy @ Other Electroconvulsive Therapy @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894436);

-- Mental Health @ None @ Electroconvulsive Therapy @ Other Electroconvulsive Therapy @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809348);

-- Other Electroconvulsive Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;electroconvulsive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795847);

-- Mental Health, Light Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795856);

-- Mental Health @ None @ Light Therapy @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868119);

-- Mental Health @ None @ Light Therapy @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894437);

-- Mental Health @ None @ Light Therapy @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801373);

-- Light Therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;light_therapy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795857);

-- Mild to moderate depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- Clinically significant depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

-- Patient interviewed directly on or before date of diagnosis of major depressive disorder (MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757095);

-- Major depressive disorder, mild (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106304);

-- Major depressive disorder, moderate (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106305);

-- Major depressive disorder, severe without psychotic features (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

-- Major depressive disorder, severe with psychotic features (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

-- Major depressive disorder, in remission (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106324);

-- Documentation of new diagnosis of initial or recurrent episode of major depressive disorder (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106326);

-- Plan for follow-up care for major depressive disorder, documented (MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756912);

-- DSM-V criteria for major depressive disorder documented at the initial evaluation (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2101900);

-- Clinician documented that patient is not an eligible candidate for suicide risk assessment; major depressive disorder, in remission (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617865);

-- Documentation of new diagnosis of initial or recurrent episode of major depressive disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617866);

-- Clinician treating major depressive disorder communicates to clinician treating comorbid condition (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533366);

-- Clinician treating major depressive disorder did not communicate to clinician treating comorbid condition, reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533229);

-- Clinician treating major depressive disorder did not communicate to clinician treating comorbid condition for specified patient reason (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786437);

-- Dsm-ivtm criteria for major depressive disorder documented at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786417);

-- Dsm-iv-tr criteria for major depressive disorder not documented at the initial evaluation, reason not otherwise specified (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786418);

-- Performance measurement, evaluation of patient self assessment, depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721177);

-- Patient with a diagnosis of major depression documented as being treated with antidepressant medication during the entire 84 day (12 week) acute treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617571);

-- Patient with a diagnosis of major depression not documented as being treated with antidepressant medication during the entire 84 day (12 week) acute treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617572);

-- Patient with a diagnosis of major depression documented as being treated with antidepressant medication during the entire 180 day (6 month) continuation treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786399);

-- Patient with a diagnosis of major depression not documented as being treated with antidepressant medication during the entire 180 day (6 months) continuation treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786400);

-- Screening for clinical depression is documented as being positive and a follow-up plan is documented (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

-- Screening for clinical depression documented as positive, follow-up plan not documented, reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

-- Screening for clinical depression documented as positive, a follow-up plan not documented, documentation stating the patient is not eligible (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

-- Clinician documented that patient with a diagnosis of major depression was not an eligible candidate for antidepressant medication treatment or patient did not have a diagnosis of major depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786398);

-- Patient documented as being treated with antidepressant medication for at least 6 months continuous treatment phase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;major_depres;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617574);

-- Individual psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007730);

-- Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007731);

-- Behavior therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007743);

-- Individual therapy for psychosexual dysfunction (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007744);

-- Crisis intervention (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007745);

-- Play psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007746);

-- Exploratory verbal psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007747);

-- Supportive verbal psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007748);

-- Other individual psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007749);

-- Other psychotherapy and counselling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007750);

-- Group therapy for psychosexual dysfunction (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007751);

-- Family therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007762);

-- Psychodrama (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007763);

-- Other group therapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007764);

-- Hypnotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007742);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 20 to 30 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213520);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 20 to 30 minutes face-to-face with the patient; with medical evaluation and management services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213521);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 45 to 50 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213522);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 45 to 50 minutes face-to-face with the patient; with medical evaluation and management services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213523);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 75 to 80 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213524);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an office or outpatient facility, approximately 75 to 80 minutes face-to-face with the patient; with medical evaluation and management services (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213525);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 20 to 30 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213526);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 20 to 30 minutes face-to-face with the patient; with (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213527);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 45 to 50 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213528);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 45 to 50 minutes face-to-face with the patient; with (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213529);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 75 to 80 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213530);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an office or outpatient facility, approximately 75 to 80 minutes face-to-face with the patient; with (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213531);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 20 to 30 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213532);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 20 to 30 minutes face-to-face with the patient; with medical evaluation and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213533);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 45 to 50 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213534);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 45 to 50 minutes face-to-face with the patient; with medical evaluation and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213535);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 75 to 80 minutes face-to-face with the patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213536);

-- Individual psychotherapy, insight oriented, behavior modifying and/or supportive, in an inpatient hospital, partial hospital or residential care setting, approximately 75 to 80 minutes face-to-face with the patient; with medical evaluation and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213537);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 20 to 30 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213538);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 20 to 30 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213539);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 45 to 50 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213540);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 45 to 50 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213541);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 75 to 80 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213542);

-- Individual psychotherapy, interactive, using play equipment, physical devices, language interpreter, or other mechanisms of non-verbal communication, in an inpatient hospital, partial hospital or residential care setting, approximately 75 to 80 minutes f (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213543);

-- Psychotherapy, 30 minutes with patient and/or family member (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527986);

-- Psychotherapy, 30 minutes with patient and/or family member when performed with an evaluation and management service (List separately in addition to the code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527987);

-- Psychotherapy, 45 minutes with patient and/or family member (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527904);

-- Psychotherapy, 45 minutes with patient and/or family member when performed with an evaluation and management service (List separately in addition to the code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527988);

-- Psychotherapy, 60 minutes with patient and/or family member (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527905);

-- Psychotherapy, 60 minutes with patient and/or family member when performed with an evaluation and management service (List separately in addition to the code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527989);

-- Psychotherapy for crisis; first 60 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527990);

-- Psychotherapy for crisis; each additional 30 minutes (List separately in addition to code for primary service) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527991);

-- Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213544);

-- Family psychotherapy (without the patient present) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213545);

-- Family psychotherapy (conjoint psychotherapy) (with patient present) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213546);

-- Multiple-family group psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213547);

-- Group psychotherapy (other than of a multiple-family group) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213548);

-- Interactive group psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213549);

-- Individual psychophysiological therapy incorporating biofeedback training by any modality (face-to-face with the patient), with psychotherapy (eg, insight oriented, behavior modifying or supportive psychotherapy); 30 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213554);

-- Individual psychophysiological therapy incorporating biofeedback training by any modality (face-to-face with the patient), with psychotherapy (eg, insight oriented, behavior modifying or supportive psychotherapy); 45 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213555);

-- Other Psychotherapy Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887728);

-- Individual psychophysiological therapy incorporating biofeedback training by any modality (face-to-face with the patient), with psychotherapy (eg, insight oriented, behavior modifying or supportive psychotherapy) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890023);

-- Interactive Complexity Psychiatry Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889017);

-- Psychotherapy Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45887951);

-- Interpretation or explanation of results of psychiatric, other medical examinations and procedures, or other accumulated data to family or other responsible persons, or advising them how to assist patient (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213559);

-- Psychotherapy for Crisis Services and Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888237);

-- Psychotherapy for crisis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889353);

-- Psychotherapy services provided (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108571);

-- Health and behavior intervention, each 15 minutes, face-to-face; individual (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314200);

-- Health and behavior intervention, each 15 minutes, face-to-face; group (2 or more patients) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314201);

-- Health and behavior intervention, each 15 minutes, face-to-face; family (with the patient present) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314202);

-- Health and behavior intervention, each 15 minutes, face-to-face; family (without the patient present) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314203);

-- Pharmacologic management, including prescription and review of medication, when performed with psychotherapy services (List separately in addition to the code for primary procedure) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43527992);

-- Hypnotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213556);

-- Environmental intervention for medical management purposes on a psychiatric patient's behalf with agencies, employers, or institutions (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2213557);

-- Group psychotherapy other than of a multiple-family group, in a partial hospitalization setting, approximately 45 to 50 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617477);

-- Interactive group psychotherapy, in a partial hospitalization setting, approximately 45 to 50 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617478);

-- Behavioral health counseling and therapy, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618103);

-- Community psychiatric supportive treatment, face-to-face, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618135);

-- Community psychiatric supportive treatment program, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618136);

-- Behavioral health day treatment, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618162);

-- Family stabilization services, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721548);

-- Crisis intervention mental health services, per hour (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721549);

-- Crisis intervention mental health services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721550);

-- Activity therapy, such as music, dance, art or play therapies not for recreation, related to the care and treatment of patient's disabling mental health problems, per session (45 minutes or more) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617264);

-- Social work and psychological services, directly relating to and/or furthering the patient's rehabilitation goals, each 15 minutes, face-to-face; individual (services provided by a corf-qualified social worker or psychologist in a corf) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617476);

-- Psychosocial rehabilitation services, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618167);

-- Psychosocial rehabilitation services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618168);

-- Mental health clubhouse services, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618180);

-- Mental health clubhouse services, per diem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618181);

-- Training and educational services related to the care and treatment of patient's disabling mental health problems per session (45 minutes or more) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617265);

-- Psychoeducational service, per 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2618177);

-- Mental Health, Crisis Intervention (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795671);

-- Mental Health @ None @ Crisis Intervention @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854450);

-- Mental Health @ None @ Crisis Intervention @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839387);

-- Mental Health @ None @ Crisis Intervention @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801371);

-- Crisis Intervention (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795672);

-- Mental Health, Individual Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795675);

-- Mental Health @ None @ Individual Psychotherapy @ Interactive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894432);

-- Mental Health @ None @ Individual Psychotherapy @ Interactive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809343);

-- Mental Health @ None @ Individual Psychotherapy @ Interactive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889389);

-- Individual Psychotherapy, Interactive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795676);

-- Mental Health @ None @ Individual Psychotherapy @ Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814214);

-- Mental Health @ None @ Individual Psychotherapy @ Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894433);

-- Mental Health @ None @ Individual Psychotherapy @ Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849566);

-- Individual Psychotherapy, Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795677);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894434);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889390);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889391);

-- Individual Psychotherapy, Cognitive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795678);

-- Mental Health @ None @ Individual Psychotherapy @ Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814215);

-- Mental Health @ None @ Individual Psychotherapy @ Interpersonal @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2867988);

-- Mental Health @ None @ Individual Psychotherapy @ Interpersonal @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849567);

-- Individual Psychotherapy, Interpersonal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795679);

-- Mental Health @ None @ Individual Psychotherapy @ Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839388);

-- Mental Health @ None @ Individual Psychotherapy @ Psychoanalysis @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900285);

-- Mental Health @ None @ Individual Psychotherapy @ Psychoanalysis @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875859);

-- Individual Psychotherapy, Psychoanalysis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795680);

-- Mental Health @ None @ Individual Psychotherapy @ Psychodynamic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849568);

-- Mental Health @ None @ Individual Psychotherapy @ Psychodynamic @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881407);

-- Mental Health @ None @ Individual Psychotherapy @ Psychodynamic @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2854451);

-- Individual Psychotherapy, Psychodynamic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795681);

-- Mental Health @ None @ Individual Psychotherapy @ Supportive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809344);

-- Mental Health @ None @ Individual Psychotherapy @ Supportive @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834817);

-- Mental Health @ None @ Individual Psychotherapy @ Supportive @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862949);

-- Individual Psychotherapy, Supportive (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795682);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2801372);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive-Behavioral @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2868118);

-- Mental Health @ None @ Individual Psychotherapy @ Cognitive-Behavioral @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826845);

-- Individual Psychotherapy, Cognitive-Behavioral (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795683);

-- Mental Health @ None @ Individual Psychotherapy @ Psychophysiological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862950);

-- Mental Health @ None @ Individual Psychotherapy @ Psychophysiological @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2821590);

-- Mental Health @ None @ Individual Psychotherapy @ Psychophysiological @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839389);

-- Individual Psychotherapy, Psychophysiological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795684);

-- Mental Health, Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795836);

-- Mental Health @ None @ Counseling @ Educational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2839390);

-- Mental Health @ None @ Counseling @ Educational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809345);

-- Mental Health @ None @ Counseling @ Educational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889392);

-- Educational Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795837);

-- Mental Health @ None @ Counseling @ Vocational (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900286);

-- Mental Health @ None @ Counseling @ Vocational @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875860);

-- Mental Health @ None @ Counseling @ Vocational @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809346);

-- Vocational Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795838);

-- Mental Health @ None @ Counseling @ Other Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826846);

-- Mental Health @ None @ Counseling @ Other Counseling @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875861);

-- Mental Health @ None @ Counseling @ Other Counseling @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2894435);

-- Other Counseling (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795839);

-- Mental Health, Family Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795840);

-- Mental Health @ None @ Family Psychotherapy @ Other Family Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849569);

-- Mental Health @ None @ Family Psychotherapy @ Other Family Psychotherapy @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881408);

-- Mental Health @ None @ Family Psychotherapy @ Other Family Psychotherapy @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862951);

-- Family Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795841);

-- Mental Health, Biofeedback (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795848);

-- Mental Health @ None @ Biofeedback @ Other Biofeedback (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2881410);

-- Mental Health @ None @ Biofeedback @ Other Biofeedback @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849570);

-- Mental Health @ None @ Biofeedback @ Other Biofeedback @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2862954);

-- Biofeedback (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795849);

-- Mental Health, Hypnosis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795850);

-- Mental Health @ None @ Hypnosis @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2814216);

-- Mental Health @ None @ Hypnosis @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2826847);

-- Mental Health @ None @ Hypnosis @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2900288);

-- Hypnosis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795851);

-- Mental Health, Narcosynthesis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795852);

-- Mental Health @ None @ Narcosynthesis @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2849571);

-- Mental Health @ None @ Narcosynthesis @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809349);

-- Mental Health @ None @ Narcosynthesis @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2834819);

-- Narcosynthesis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795853);

-- Mental Health, Group Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795854);

-- Mental Health @ None @ Group Psychotherapy @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2889394);

-- Mental Health @ None @ Group Psychotherapy @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2875864);

-- Mental Health @ None @ Group Psychotherapy @ None @ None @ None (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2809350);

-- Group Psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;psychosocial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2795855);

-- Referral for psychologic rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007768);

-- Referral for psychotherapy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007769);

-- Referral for psychiatric aftercare (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007770);

-- Referral for vocational rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007783);

-- Referral for other psychologic rehabilitation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007784);

-- Patient referral for psychotherapy documented (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;referral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108572);

-- Polysomnography; younger than 6 years, sleep staging with 4 or more additional parameters of sleep, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43528014);

-- Polysomnography; younger than 6 years, sleep staging with 4 or more additional parameters of sleep, with initiation of continuous positive airway pressure therapy or bi-level ventilation, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43528015);

-- Sleep study, unattended, simultaneous recording; heart rate, oxygen saturation, respiratory analysis (eg, by airflow or peripheral arterial tone), and sleep time (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757072);

-- Sleep study, unattended, simultaneous recording; minimum of heart rate, oxygen saturation, and respiratory analysis (eg, by airflow or peripheral arterial tone) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756854);

-- Multiple sleep latency or maintenance of wakefulness testing, recording, analysis and interpretation of physiological measurements of sleep during multiple trials to assess sleepiness (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314095);

-- Sleep study, unattended, simultaneous recording of, heart rate, oxygen saturation, respiratory airflow, and respiratory effort (eg, thoracoabdominal movement) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314096);

-- Sleep study, simultaneous recording of ventilation, respiratory effort, ECG or heart rate, and oxygen saturation, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314097);

-- Polysomnography; any age, sleep staging with 1-3 additional parameters of sleep, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314098);

-- Polysomnography; age 6 years or older, sleep staging with 4 or more additional parameters of sleep, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314099);

-- Polysomnography; age 6 years or older, sleep staging with 4 or more additional parameters of sleep, with initiation of continuous positive airway pressure therapy or bilevel ventilation, attended by a technologist (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2314100);

-- Home visit for polysomnography and sleep studies (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42739877);

-- Sleep Medicine Testing Procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888731);

-- Polysomnography (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890139);

-- Sleep study, unattended, simultaneous recording (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888226);

-- Sleep study, unattended, simultaneous recording; heart rate, oxygen saturation, respiratory analysis (eg, by airflow or peripheral arterial tone) and sleep time (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42741857);

-- Sleep study, unattended, simultaneous recording; minimum of heart rate, oxygen saturation, and respiratory analysis (eg, by airflow or peripheral arterial tone) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42741849);

-- Continuous measurement of wheeze rate during treatment assessment or during sleep for documentation of nocturnal wheeze and cough for diagnostic evaluation 3 to 24 hours, with interpretation and report (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756984);

-- Home sleep study test (hst) with type ii portable monitor, unattended; minimum of 7 channels: eeg, eog, emg, ecg/heart rate, airflow, respiratory effort and oxygen saturation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617466);

-- Home sleep test (hst) with type iii portable monitor, unattended; minimum of 4 channels: 2 respiratory movement/airflow, 1 ecg/heart rate and 1 oxygen saturation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617467);

-- Home sleep test (hst) with type iv portable monitor, unattended; minimum of 3 channels (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;sleep_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617468);

-- Suicide risk assessed (MDD, MDD ADOL) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106303);

-- Negative screen for depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107225);

-- No significant depressive symptoms as categorized by using a standardized depression assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107229);

-- Mild to moderate depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- Clinically significant depressive symptoms as categorized by using a standardized depression screening/assessment tool (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

-- Brief emotional/behavioral assessment (eg, depression inventory, attention-deficit/hyperactivity disorder [ADHD] scale), with scoring and documentation, per standardized instrument (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257498);

-- Patient screened for depression (SUD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2102214);

-- Suicide risk assessed at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533176);

-- Annual depression screening, 15 minutes (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664726);

-- Screening for clinical depression is documented as negative, a follow-up plan is not required (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617909);

-- Screening for clinical depression is documented as being positive and a follow-up plan is documented (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

-- Screening for clinical depression documented as positive, follow-up plan not documented, reason not given (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

-- Screening for clinical depression documented as positive, a follow-up plan not documented, documentation stating the patient is not eligible (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

-- Assessment of depression severity at the initial evaluation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;suic_depr_asses;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533277);

-- Therapeutic repetitive transcranial magnetic stimulation (TMS) treatment; initial, including cortical mapping, motor threshold determination, delivery and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757109);

-- Therapeutic repetitive transcranial magnetic stimulation (TMS) treatment; subsequent delivery and management, per session (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756837);

-- Therapeutic repetitive transcranial magnetic stimulation (TMS) treatment; subsequent motor threshold re-determination with delivery and management (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42742381);

-- Therapeutic repetitive transcranial magnetic stimulation (TMS) treatment (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45888229);

-- Therapeutic repetitive transcranial magnetic stimulation treatment delivery and management, per session (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Procedures;mental_procedure;transcranial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42741151);
