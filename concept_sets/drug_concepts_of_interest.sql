
-- benazepril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1335471);

-- captopril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1340128);

-- cilazapril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050216);

-- enalapril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1341927);

-- enalaprilat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1342001);

-- fosinopril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1363749);

-- fosinoprilat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775539);

-- imidapril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19122327);

-- lisinopril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1308216);

-- moexipril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1310756);

-- perindopril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1373225);

-- perindoprilat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775544);

-- quinapril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1331235);

-- quinaprilat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775375);

-- ramipril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1334456);

-- ramiprilat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775527);

-- spirapril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19040051);

-- trandolapril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1342439);

-- zofenopril (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;ace_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19102107);

-- acamprosate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;alcohol_deterrent;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19043959);

-- disulfiram (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;alcohol_deterrent;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(735850);

-- Amikacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1790868);

-- arbekacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19101402);

-- Dibekacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19023508);

-- Gentamicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45892419);

-- Kanamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1784749);

-- Neomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915981);

-- Netilmicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017585);

-- Sisomicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136044);

-- Streptomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836191);

-- Tobramycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aminoglycosid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(902722);

-- 2-diethylaminoethanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43559949);

-- aceclofenac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029393);

-- acemetacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029398);

-- Acetaminophen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1125315);

-- acexamic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19025107);

-- adapalene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(981774);

-- alminoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018431);

-- Amantadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19087090);

-- Amitriptyline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(710062);

-- Antipyrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1036059);

-- Apazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136654);

-- apremilast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816294);

-- Aspirin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1112807);

-- baicalin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44506770);

-- balsalazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(934262);

-- benorilate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19016435);

-- bromfenac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1139179);

-- Bufexamac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19032724);

-- bumadizone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19039703);

-- butibufen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020568);

-- Carbachol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(940183);

-- Carbamoylcholine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775534);

-- carprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19046454);

-- caryophyllene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014045);

-- celecoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1118084);

-- Chloroquine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1792515);

-- Choline Magnesium Trisalicyclate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1157456);

-- Clonixin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19099129);

-- Curcumin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072030);

-- Dexketoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056874);

-- Dexmedetomidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19061088);

-- Diclofenac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1124300);

-- Diflunisal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1126128);

-- Dihydroergotamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1126557);

-- Dipyrone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19031397);

-- droxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056645);

-- Ergotamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1145379);

-- Etanercept (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1151789);

-- ethenzamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059506);

-- Etodolac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1195492);

-- etofenamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095561);

-- etoricoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011355);

-- Evening primrose oil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045290);

-- Felbinac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100761);

-- fenbufen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095703);

-- Fenoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1153928);

-- Feprazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135796);

-- ferulate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014099);

-- floctafenine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054931);

-- flunixin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19096546);

-- Flurbiprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1156378);

-- glucametacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19098123);

-- Ibuprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1177480);

-- icatibant (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40242044);

-- imidazole-2-hydroxybenzoate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069104);

-- indobufen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069107);

-- Indomethacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1178663);

-- kebuzone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069191);

-- Ketoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1185922);

-- Ketorolac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1136980);

-- lonazolac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19071933);

-- lornoxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19049709);

-- loxoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19071936);

-- Magnesium Salicylate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1168079);

-- Masoprocol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(988447);

-- Meclofenamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1180182);

-- Meclofenamic Acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125097);

-- Medetomidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19067415);

-- Mefenamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1197736);

-- meloxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1150345);

-- mesalamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(968426);

-- Methotrimeprazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005147);

-- mofebutazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072129);

-- mofezolac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011062);

-- nabumetone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1113648);

-- Naproxen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1115008);

-- Nefopam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19015602);

-- nepafenac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(909795);

-- Niflumic Acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19019023);

-- nimesulide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069425);

-- Nitrous Oxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19021129);

-- Nortriptyline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(721724);

-- olopatadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915855);

-- olsalazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(916282);

-- orgotein (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19082874);

-- oxaprozin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1118045);

-- Oxyphenbutazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19025925);

-- parecoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003570);

-- parthenolide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19083839);

-- Phenacetin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19033710);

-- Phenylbutazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1135710);

-- Pimecrolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915935);

-- pirfenidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775206);

-- Piroxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1146810);

-- Pizotyline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19047076);

-- proglumetacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029327);

-- propyphenazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029322);

-- Quinine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1760616);

-- resveratrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40225716);

-- rofecoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1189754);

-- salicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100313);

-- salicylamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1164108);

-- Salicylic Acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(964407);

-- Salsalate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1137460);

-- Samarium SM 153 lexidronam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018483);

-- serratiopeptidase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100379);

-- Sulfasalazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(964339);

-- Sulindac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1236607);

-- Suprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1036636);

-- tenoxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041220);

-- Tetrahydrocannabinol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1037005);

-- Tiaprofenate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19086910);

-- tolfenamic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042155);

-- Tolmetin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1102917);

-- tranilast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19120306);

-- tribenoside (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042575);

-- ursolate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014258);

-- valdecoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1103374);

-- ziconotide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005061);

-- zileuton (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1147878);

-- zomepirac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;non_narcotic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19102108);

-- alfentanil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059528);

-- alphaprodine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080847);

-- buprenorphine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1133201);

-- butorphanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1133732);

-- codeine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1201620);

-- dextromoramide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19021940);

-- dezocine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19088393);

-- dihydrocodeine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1189596);

-- diphenoxylate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(930916);

-- ethylmorphine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050414);

-- fentanyl (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1154029);

-- hydrocodone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1174888);

-- hydromorphone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1126658);

-- levomethadyl (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092108);

-- levorphanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1189766);

-- meperidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1102527);

-- meptazinol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003010);

-- methadone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1103640);

-- methadyl acetate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003959);

-- morphine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1110410);

-- nalbuphine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1114122);

-- normethadone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19015787);

-- opium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(923829);

-- oxycodone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1124957);

-- oxymorphone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1125765);

-- papaveretum (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19129648);

-- pentazocine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1130585);

-- phenazocine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19132884);

-- phenoperidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19132889);

-- pirinitramide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19134009);

-- propoxyphene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1153664);

-- remifentanil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19016749);

-- sufentanil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19078219);

-- tilidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19002431);

-- tramadol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1103314);

-- heroin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;analgesic;opioid;heroin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19022417);

-- nandrolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1514412);

-- oxandrolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1524769);

-- oxymetholone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1525741);

-- stanozolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1636145);

-- testosterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1636780);

-- bicalutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1344381);

-- chlormadinone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026733);

-- cyproterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010792);

-- flutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1356461);

-- nilutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1315286);

-- tibolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;androgen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041933);

-- gabapentin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(797399);

-- alprazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(781039);

-- bromazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19030353);

-- buspirone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(733301);

-- carpipramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026925);

-- chlordiazepoxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(990678);

-- chlormezanone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092290);

-- clorazepate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(790253);

-- diazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(723013);

-- doramectin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017646);

-- estazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(748010);

-- ethyl loflazepate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095467);

-- etifoxine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059441);

-- flunitrazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055224);

-- flurazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(756349);

-- halazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(801396);

-- ketazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003946);

-- lorazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(791967);

-- medazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125106);

-- meprobamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(702865);

-- midazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(708298);

-- nabilone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(913440);

-- nitrazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020021);

-- nordazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080959);

-- ondansetron (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1000560);

-- oxazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(724816);

-- oxprenolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19024904);

-- prazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050461);

-- pregabalin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(734354);

-- temazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(836715);

-- trazodone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(703547);

-- triazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(704599);

-- triazulenone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042550);

-- zolazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_anxiety;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44818464);

-- Acebutolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319998);

-- Acetyldigitoxin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011193);

-- Acetyldigoxins (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020949);

-- Adenosine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309204);

-- Ajmaline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19105879);

-- Alprenolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081284);

-- Amiodarone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309944);

-- Aprindine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19006969);

-- Atenolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1314002);

-- Atropine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(914335);

-- Bepridil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319751);

-- bretylium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19038998);

-- Bupranolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018489);

-- Carteolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(950370);

-- Celiprolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19049145);

-- cicletanine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051444);

-- cifenline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045581);

-- Deslanoside (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017074);

-- detajmium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19133173);

-- detajmium bitartrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056573);

-- Digitoxin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026180);

-- Digoxin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1326303);

-- Disopyramide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1335606);

-- dofetilide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1362979);

-- Encainide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051513);

-- Felodipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1353776);

-- Flecainide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1354860);

-- fosinoprilat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775539);

-- glimepiride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1597756);

-- hydroquinidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069075);

-- Hyoscyamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(923672);

-- ibutilide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050087);

-- Levosimendan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40173184);

-- Lidocaine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(989878);

-- Losartan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1367500);

-- Magnesium Sulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19093848);

-- Magnesium sulfate heptahydrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19091804);

-- Medigoxin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19094483);

-- Metipranolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(905531);

-- Metoprolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1307046);

-- Mexiletine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1307542);

-- Moricizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1344996);

-- Nadolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1313200);

-- Nicorandil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19014977);

-- Oxprenolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19024904);

-- Practolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135791);

-- Prajmaline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135792);

-- Procainamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1351461);

-- Propafenone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1353256);

-- Propranolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1353766);

-- Quinidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1360421);

-- Sotalol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1370109);

-- talinolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100435);

-- tertatolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100451);

-- Timolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(902427);

-- Tocainide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1351447);

-- Verapamil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_arrhytmic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1307863);

-- Albuterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1154343);

-- Aminophylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1105775);

-- arformoterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1111220);

-- Atropine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(914335);

-- azelastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(934075);

-- baicalin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44506770);

-- bambuterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19034275);

-- bamifylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018518);

-- Beclomethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1115572);

-- Betamethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(920458);

-- bitolterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1138050);

-- Budesonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(939259);

-- cilostazol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1350310);

-- Clenbuterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097824);

-- Cromoglycate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19008867);

-- Cromolyn (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1152631);

-- Diphemanil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135822);

-- Dyphylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1140088);

-- Epinephrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1343916);

-- etofylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095564);

-- Fenoterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19053979);

-- fenspiride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19107829);

-- flunisolide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1196514);

-- fluticasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1149380);

-- formoterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1196677);

-- Hexoprenaline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19068969);

-- Hyoscyamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(923672);

-- Ipratropium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1112921);

-- Isoetharine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1181809);

-- Isoproterenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1183554);

-- ISOSPAGLUMIC ACID (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19091288);

-- Khellin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19123346);

-- metaproterenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1123995);

-- montelukast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1154161);

-- Nedocromil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1114620);

-- Nitric Oxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020068);

-- olodaterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775116);

-- omalizumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1110942);

-- oxatomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19071314);

-- oxtriphylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19049816);

-- pirbuterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1125449);

-- Procaterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19028950);

-- proxyphylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029547);

-- Pseudoephedrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1154332);

-- Racepinephrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1300153);

-- reproterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19035396);

-- reslizumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35603983);

-- salmeterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1137529);

-- Terbutaline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1236744);

-- Theobromine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19137056);

-- Theophylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1237049);

-- tiotropium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1106776);

-- tulobuterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19043191);

-- zafirlukast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_asthma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1111706);

-- amdinocillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19123877);

-- amdinocillin pivoxil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19088223);

-- amikacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1790868);

-- aminosalicylic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1726228);

-- amoxicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1713332);

-- ampicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1717327);

-- amprenavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789428);

-- azithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1734104);

-- azlocillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19015123);

-- aztreonam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1715117);

-- bacampicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1734205);

-- bacitracin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915175);

-- bambermycins (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012512);

-- bedaquiline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012518);

-- capreomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026710);

-- carbenicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1740546);

-- cefaclor (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1768849);

-- cefadroxil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1769535);

-- cefamandole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19070174);

-- cefatrizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19070680);

-- cefazolin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1771162);

-- cefdinir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1796458);

-- cefditoren (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1747005);

-- cefepime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1748975);

-- cefetamet (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19028241);

-- cefixime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1796435);

-- cefmetazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072255);

-- cefodizime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19028286);

-- cefonicid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072857);

-- cefoperazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1773402);

-- ceforanide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19028288);

-- cefotaxime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1774470);

-- cefotetan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1774932);

-- cefotiam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051271);

-- cefoxitin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1775741);

-- cefpirome (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19001904);

-- cefpodoxime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1749008);

-- cefprozil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738366);

-- cefsulodin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051345);

-- ceftazidime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1776684);

-- ceftibuten (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1749083);

-- ceftiofur (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900335);

-- ceftizoxime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1777254);

-- ceftriaxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1777806);

-- cefuroxime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1778162);

-- cephalexin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1786621);

-- cephaloridine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19052683);

-- cephalothin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19086759);

-- cephapirin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19086790);

-- cephradine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1786842);

-- chloramphenicol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(990069);

-- chloroxine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(949453);

-- chlortetracycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095043);

-- clarithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1750500);

-- clavulanate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1759842);

-- clindamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(997881);

-- clofazimine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1798476);

-- cloxacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1800835);

-- colistin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(901845);

-- cyclacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009277);

-- cycloserine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1710446);

-- dactinomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1311443);

-- dalbavancin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45774861);

-- dalfopristin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789517);

-- dapsone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1711759);

-- daptomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1786617);

-- dibekacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19023508);

-- dicloxacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724666);

-- dihydrostreptomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45776730);

-- dirithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1790024);

-- doxycycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738521);

-- enoxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1743222);

-- ertapenem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1717963);

-- erythromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1746940);

-- ethambutol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1749301);

-- ethionamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1750074);

-- florfenicol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19096487);

-- floxacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054936);

-- fosfomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(956653);

-- framycetin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056694);

-- fusafungin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097405);

-- fusidate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010400);

-- gamithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40240027);

-- gatifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789276);

-- gemifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1716721);

-- gentamicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45892419);

-- gentamicin sulfate (usp) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(919345);

-- gramicidin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(963742);

-- grepafloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1747032);

-- hygromycin b (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43531970);

-- imipenem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1778262);

-- isoniazid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1782521);

-- josamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19123240);

-- kanamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1784749);

-- lasalocid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012743);

-- levofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1742253);

-- lincomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1790692);

-- linezolid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1736887);

-- lomefloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1707800);

-- loracarbef (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1708100);

-- lymecycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092353);

-- marbofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40170759);

-- meclocycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009022);

-- meropenem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1709170);

-- methacycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003644);

-- methampicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072054);

-- methicillin sodium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19001323);

-- mezlocillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19007701);

-- micronomicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072089);

-- midecamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072122);

-- minocycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1708880);

-- miocamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009138);

-- moxalactam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19126622);

-- moxifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1716903);

-- mupirocin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(951511);

-- nafcillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1713930);

-- nalidixate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(986864);

-- narasin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42903851);

-- natamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915392);

-- neomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915981);

-- netilmicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017585);

-- norfloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1721543);

-- novobiocin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19022015);

-- nystatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(922570);

-- ofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(923081);

-- oleandomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19023254);

-- oritavancin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45776147);

-- oxacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724703);

-- oxolinic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19129642);

-- oxytetracycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(925952);

-- paromomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1727443);

-- pefloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027679);

-- penicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19126544);

-- penicillin g (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1728416);

-- penicillin v (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1729720);

-- phenethicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19088795);

-- pipemidate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010564);

-- piperacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1746114);

-- pivampicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19047071);

-- polymyxin b (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(948582);

-- pristinamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125201);

-- propicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19096054);

-- prothionamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135812);

-- pyrazinamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1759455);

-- quinupristin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789515);

-- rifabutin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1777417);

-- rifamycins (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136008);

-- rifapentine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19035953);

-- rolitetracycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136024);

-- roxarsone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42903869);

-- roxithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19063874);

-- salinomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900569);

-- sirolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19034726);

-- sisomicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136044);

-- sodium thiosulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(940004);

-- sparfloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1733765);

-- spectinomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1701651);

-- spiramycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19070251);

-- streptomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836191);

-- sulbactam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836241);

-- sulbenicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000817);

-- sulfamerazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136426);

-- sulfamethoxypyridazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000820);

-- sulfanilamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1036475);

-- sultamicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19040627);

-- talampicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19002077);

-- tazobactam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1741122);

-- tedizolid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775686);

-- teicoplanin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19078399);

-- telavancin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40166675);

-- telithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1702911);

-- temafloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041153);

-- temocillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100438);

-- tetracycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836948);

-- thalidomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19137042);

-- thiamphenicol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19137362);

-- thiostrepton (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013000);

-- tiamulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775549);

-- tiamulin fumarate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42903890);

-- ticarcillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1702364);

-- tigecycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1742432);

-- tilmicosin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100758);

-- tobramycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(902722);

-- troleandomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19006043);

-- tylosin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40222391);

-- tyrothricin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19006842);

-- virginiamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010969);

-- amphotericin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;amphotericin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(952045);

-- amphotericin b (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;amphotericin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1717240);

-- amphotericin b liposomal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;amphotericin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056402);

-- ciprofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;ciprofloxacin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1797513);

-- demeclocycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;demeclocyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1714527);

-- rifampin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;rifampin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1763204);

-- vancomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_bacterial;vancomycin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1707687);

-- gabapentin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(797399);

-- Acetazolamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(929435);

-- Chlormethiazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092283);

-- clobazam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050832);

-- Clonazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(798874);

-- clorazepate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(790253);

-- Diazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(723013);

-- dipropylacetamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054995);

-- doramectin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017646);

-- Estazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(748010);

-- Ethosuximide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(750119);

-- Ethotoin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(750146);

-- ezogabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40239995);

-- felbamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(795661);

-- Flunarizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055183);

-- fosphenytoin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(713192);

-- indeloxazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900470);

-- lacosamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19087394);

-- Levetiracetam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(711584);

-- Lorazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(791967);

-- Magnesium Sulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19093848);

-- Magnesium sulfate heptahydrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19091804);

-- mebeverine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19008994);

-- Medazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125106);

-- Mephenytoin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(702661);

-- Mephobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(702685);

-- Meprobamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(702865);

-- methsuximide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(759401);

-- Nitrazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020021);

-- Paraldehyde (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027181);

-- Phenobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(734275);

-- pregabalin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(734354);

-- Primidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(751347);

-- progabide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095776);

-- Riluzole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(735951);

-- sulthiame (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000921);

-- Thiopental (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(700253);

-- tiagabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(715458);

-- Tiletamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44818460);

-- tizanidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(778474);

-- topiramate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(742267);

-- tramiprosate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43526386);

-- Trimethadione (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005629);

-- vanillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014262);

-- Vigabatrin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020002);

-- zaleplon (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(720727);

-- zonisamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(744798);

-- Phenytoin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_convulsant;phenytoin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(740910);

-- acrivastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1140123);

-- alcaftadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40224159);

-- Antazoline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(951237);

-- Astemizole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1150770);

-- azatadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1133993);

-- azelastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(934075);

-- Brompheniramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1130863);

-- carbinoxamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1141018);

-- Cetirizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1149196);

-- chloropyramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19049437);

-- Chlorpheniramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1192710);

-- Cinnarizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097481);

-- Clemastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1197677);

-- Cyclizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(909358);

-- Cyproheptadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1110727);

-- desloratadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1103006);

-- dexbrompheniramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1188052);

-- dexchlorpheniramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1188114);

-- Dimenhydrinate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(928744);

-- Dimethindene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19028947);

-- Diphenhydramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1129625);

-- Doxylamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(738818);

-- ebastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092433);

-- emedastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(903893);

-- epinastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(943634);

-- fexofenadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1153428);

-- Flunarizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055183);

-- fonazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19113015);

-- Hydroxyzine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(777221);

-- ISOSPAGLUMIC ACID (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19091288);

-- Ketotifen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(986117);

-- levocabastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(907553);

-- levocetirizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1136422);

-- Lodoxamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(967496);

-- Loratadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1107830);

-- Meclizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(994341);

-- mequitazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009145);

-- Methapyrilene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004158);

-- Mianserin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19007737);

-- Mirtazapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(725131);

-- mizolastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19086100);

-- olopatadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915855);

-- oxatomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19071314);

-- phenindamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1123534);

-- Pheniramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(933952);

-- Promethazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1153013);

-- Pyrilamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1159811);

-- Terfenadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1150836);

-- tranilast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19120306);

-- Trimeprazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005570);

-- Tripelennamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1105853);

-- Triprolidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1105889);

-- tritoqualine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h1;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19101651);

-- Famotidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h2;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(953076);

-- Nizatidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h2;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(950696);

-- roxatidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h2;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011685);

-- Cimetidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h2;cimetidine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(997276);

-- Ranitidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_histamine_h2;ranitidine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(961047);

-- abacavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1736971);

-- aldesleukin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309770);

-- amprenavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789428);

-- atazanavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1727223);

-- cobicistat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42874220);

-- darunavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1756831);

-- delavirdine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1747157);

-- didanosine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724869);

-- dolutegravir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43560385);

-- emtricitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1703069);

-- enfuvirtide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1717002);

-- fosamprenavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1736829);

-- indinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1711523);

-- lamivudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1704183);

-- lopinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738170);

-- maraviroc (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1787101);

-- miglustat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045272);

-- nelfinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1715472);

-- nevirapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1769389);

-- plerixafor (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017581);

-- raltegravir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1712889);

-- rilpivirine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40238930);

-- ritonavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1748921);

-- saquinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1746244);

-- stavudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1781406);

-- tenofovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011093);

-- tenofovir disoproxil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1710281);

-- tipranavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1736999);

-- zalcitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724827);

-- zidovudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_hiv;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1710612);

-- Methimazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_thyroid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1504620);

-- potassium perchlorate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_thyroid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027339);

-- Propylthiouracil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_thyroid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1554072);

-- aminosalicylic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1726228);

-- amprenavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789428);

-- bedaquiline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012518);

-- capreomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026710);

-- cycloserine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1710446);

-- ethambutol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1749301);

-- ethionamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1750074);

-- isoniazid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1782521);

-- prothionamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135812);

-- pyrazinamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1759455);

-- rifabutin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1777417);

-- rifapentine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19035953);

-- sodium thiosulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(940004);

-- sparfloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1733765);

-- rifampin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_tubercular;rifampin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1763204);

-- abacavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1736971);

-- acemannan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43532246);

-- acetylcysteine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1139042);

-- acyclovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1703687);

-- adefovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1729323);

-- aldesleukin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309770);

-- amantadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19087090);

-- amonafide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44506774);

-- amprenavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789428);

-- atazanavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1727223);

-- brivudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19122130);

-- carbomer homopolymer type b (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900353);

-- carbomer-934 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900354);

-- carbomer-940 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012252);

-- cidofovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1745072);

-- cobicistat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42874220);

-- cytarabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1311078);

-- darunavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1756831);

-- delavirdine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1747157);

-- didanosine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724869);

-- diethyldithiocarbamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43532333);

-- docosanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(980955);

-- dolutegravir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43560385);

-- edoxudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19063245);

-- efavirenz (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738135);

-- emtricitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1703069);

-- enfuvirtide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1717002);

-- entecavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1711246);

-- etravirine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1758536);

-- famciclovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1703603);

-- fomivirsen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19048999);

-- fosamprenavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1736829);

-- ganciclovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1757803);

-- gemcitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1314924);

-- hypericin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775583);

-- idoxuridine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19078156);

-- indinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1711523);

-- inosine pranobex (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19083285);

-- interferon alfacon-1 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1781314);

-- interferon beta-1a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(722424);

-- interferon type ii (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19122123);

-- interferon-beta (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19109079);

-- lamivudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1704183);

-- ledipasvir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775020);

-- lopinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738170);

-- maraviroc (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1787101);

-- miglustat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045272);

-- moroxydine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010924);

-- nelfinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1715472);

-- nevirapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1769389);

-- oseltamivir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1799139);

-- palivizumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(537647);

-- peginterferon alfa-2a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1714165);

-- peginterferon alfa-2b (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1797155);

-- penciclovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(981654);

-- peramivir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40167569);

-- plerixafor (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017581);

-- raltegravir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1712889);

-- ribavirin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1762711);

-- rilpivirine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40238930);

-- rimantadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1763339);

-- ritonavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1748921);

-- saquinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1746244);

-- simeprevir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44785086);

-- sofosbuvir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44785094);

-- stavudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1781406);

-- telbivudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1758392);

-- tenofovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011093);

-- tenofovir disoproxil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1710281);

-- tipranavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1736999);

-- trifluridine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(905078);

-- troclosene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44818503);

-- tromantadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19101679);

-- valacyclovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1717704);

-- valganciclovir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1703063);

-- vidarabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(908126);

-- zalcitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724827);

-- zanamivir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1708748);

-- zidovudine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1710612);

-- foscarnet (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;anti_viral;foscarnet;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724700);

-- 3,3'-diindolylmethane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46221501);

-- 3-Iodobenzylguanidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19019942);

-- 6-O-palmitoylascorbic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40235242);

-- abiraterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40239056);

-- acemannan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43532246);

-- Aclarubicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19057483);

-- ado-trastuzumab emtansine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43525787);

-- alatrofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018154);

-- Aldesleukin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309770);

-- alitretinoin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(941052);

-- allyl sulfide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1330144);

-- Altretamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1368823);

-- Aminoglutethimide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1503057);

-- amonafide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44506774);

-- Amsacrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017810);

-- Amygdalin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43525788);

-- anastrozole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1348265);

-- arsenic trioxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1333379);

-- ascorbyl phosphate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013873);

-- ASPARAGINASE (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19012585);

-- Azacitidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1314865);

-- Azathioprine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19014878);

-- Azelate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(986790);

-- belinostat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45776670);

-- bendamustine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19015523);

-- besifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40161662);

-- beta-thujaplicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42903462);

-- bevacizumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1397141);

-- bexarotene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1389888);

-- bicalutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1344381);

-- Bleomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1329241);

-- blinatumomab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45892531);

-- bortezomib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1336825);

-- Busulfan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1333357);

-- cabergoline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1558471);

-- capecitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1337620);

-- CARBOMER HOMOPOLYMER TYPE B (ALLYL PENTAERYTHRITOL OR ALLYL SUCROSE CROSSLINKED) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35606037);

-- carbomer-934 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900354);

-- Carbomer-940 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012252);

-- Carboplatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1344905);

-- Carmustine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1350066);

-- carvone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43532259);

-- ceritinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44818466);

-- cetuximab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1315411);

-- Chlorambucil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1390051);

-- Chlorotrianisene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092684);

-- chlorozotocin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012229);

-- cholesteryl sulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013057);

-- cinnamic aldehyde (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014055);

-- Cinoxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(997496);

-- Ciprofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1797513);

-- Cladribine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054825);

-- clofarabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054821);

-- Curcumin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072030);

-- Cyproterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010792);

-- Cytarabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1311078);

-- dabrafenib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43532299);

-- Dacarbazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1311409);

-- Dactinomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1311443);

-- daratumumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35605744);

-- dasatinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1358436);

-- Daunorubicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1311799);

-- decitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19024728);

-- denileukin diftitox (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051642);

-- Dexamethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1518254);

-- Dexrazoxane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1353011);

-- dienogest (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054876);

-- Dihematoporphyrin Ether (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19089602);

-- docetaxel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1315942);

-- Doxorubicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1338512);

-- doxorubicin liposome (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051649);

-- Eflornithine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(978236);

-- Enoxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1743222);

-- enrofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40170592);

-- epigallocatechin gallate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19058494);

-- Epirubicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1344354);

-- erlotinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1325363);

-- Estramustine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1349025);

-- Ethiodized Oil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050016);

-- Ethoglucid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19104221);

-- Etoposide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1350504);

-- everolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011440);

-- exemestane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1398399);

-- Fleroxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050750);

-- Floxuridine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1355509);

-- fludarabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1395557);

-- flumequine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19064329);

-- Fluorouracil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(955632);

-- Flutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1356461);

-- formestane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020079);

-- fosfestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19065450);

-- fulvestrant (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1304044);

-- fumagillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1592567);

-- gallium nitrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097605);

-- gatifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789276);

-- gefitinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319193);

-- gemcitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1314924);

-- gemifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1716721);

-- Gemtuzumab ozogamicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42799711);

-- Genistein (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097682);

-- geranylgeranylacetone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012210);

-- Goserelin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1366310);

-- Grape Seed Extract (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1392182);

-- grepafloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1747032);

-- hydroxyurea (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1377141);

-- hypericin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775583);

-- Idarubicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19078097);

-- idelalisib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45776944);

-- Ifosfamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19078187);

-- imatinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1304107);

-- imiquimod (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(981691);

-- Interferon beta-1a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(722424);

-- Interferon-beta (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19109079);

-- Interleukin-4 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44784851);

-- Interleukin-12 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42899448);

-- iodine-131-tositumomab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054997);

-- irinotecan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1367268);

-- isobutyramide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46234101);

-- ixazomib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35606214);

-- lanreotide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1503501);

-- lapatinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1359548);

-- lenalidomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026972);

-- lenvatinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46221433);

-- letrozole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1315946);

-- Leuprolide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1351541);

-- Levofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1742253);

-- lomefloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1707800);

-- Lomustine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1391846);

-- lycopene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1308185);

-- Mechlorethamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1394337);

-- Medrogestone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000207);

-- Medroxyprogesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1500211);

-- Megestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1300978);

-- Melphalan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1301267);

-- mequinol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(920113);

-- mercaptopurine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1436650);

-- Methotrexate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1305058);

-- Methylprednisolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1506270);

-- Methyltestosterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1506602);

-- midostaurin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1594003);

-- miltefosine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816310);

-- Mitobronitol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125635);

-- Mitomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1389036);

-- Mitotane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309161);

-- Mitoxantrone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309188);

-- molgramostim (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009676);

-- moxifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1716903);

-- musk ketone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44506746);

-- MYCOPHENOLATE (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19068900);

-- mycophenolate mofetil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003999);

-- Mycophenolic Acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19012565);

-- Nalidixate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(986864);

-- nilutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1315286);

-- nintedanib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775396);

-- niraparib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1593861);

-- nivolumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45892628);

-- Norfloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1721543);

-- obinutuzumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44507676);

-- Octreotide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1522957);

-- Ofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(923081);

-- Oil of garlic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42899536);

-- olaparib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45892579);

-- omacetaxine mepesuccinate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069046);

-- Oprelvekin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1318030);

-- osimertinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35605522);

-- oxaliplatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1318011);

-- Oxolinic Acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19129642);

-- Paclitaxel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1378382);

-- palbociclib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45892075);

-- panobinostat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46221435);

-- Pefloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027679);

-- pegaspargase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1326481);

-- pembrolizumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775965);

-- pemetrexed (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1304919);

-- Pentostatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19031224);

-- pertuzumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42801287);

-- phenylacetate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1511856);

-- phenylbutyrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1539954);

-- Pipobroman (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19046625);

-- Pirarubicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19093366);

-- pirfenidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775206);

-- Plicamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009165);

-- podofilox (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(947705);

-- podophyllin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(947651);

-- polysaccharide-K (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40220969);

-- pomalidomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014237);

-- ponatinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013182);

-- Prednimustine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135793);

-- prednisolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1550557);

-- Prednisone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1551099);

-- Procarbazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1351779);

-- proxigermanium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018290);

-- Quinacrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19060400);

-- raltitrexed (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19038536);

-- ramucirumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44818489);

-- ranibizumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080982);

-- razoxane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135936);

-- resveratrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40225716);

-- retinamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013154);

-- rituximab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1314273);

-- romidepsin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40168385);

-- rucaparib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1718850);

-- satraplatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35606078);

-- siltuximab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44818461);

-- Sirolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19034726);

-- sorafenib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1363387);

-- sparfloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1733765);

-- Streptozocin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136210);

-- Sulindac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1236607);

-- sunitinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1336539);

-- Suramin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136651);

-- Tamoxifen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1436678);

-- Tegafur (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056756);

-- temafloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041153);

-- temoporfin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011067);

-- temozolomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1341149);

-- Teniposide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136750);

-- Testolactone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1436776);

-- Thalidomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19137042);

-- Thioguanine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1437379);

-- Thiotepa (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19137385);

-- thymalfasin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100740);

-- tibolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041933);

-- Topotecan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1378509);

-- Toremifene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1342346);

-- trabectedin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35603017);

-- trametinib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43532497);

-- trastuzumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1387104);

-- treosulfan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042545);

-- Tretinoin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(903643);

-- trilostane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042801);

-- Trimetrexate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1750928);

-- Triptorelin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1343039);

-- trofosfamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19101677);

-- trovafloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1712549);

-- Uracil Mustard (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19006880);

-- Urethane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43526080);

-- ursolate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014258);

-- valrubicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19012543);

-- Vinblastine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19008264);

-- Vincristine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1308290);

-- Vindesine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19008336);

-- vinorelbine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1343346);

-- Vitamin A (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19008339);

-- Vorinostat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1337651);

-- Cisplatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;cisplatin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1397599);

-- Cyclophosphamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;antineoplastic;cyclophosphamide;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1310317);

-- Aristolochic Acids (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;aristolochic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4252806);

-- alprazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(781039);

-- bromazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19030353);

-- chlordiazepoxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(990678);

-- clobazam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050832);

-- clorazepate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(790253);

-- clotiazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19068821);

-- cloxazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051096);

-- diazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(723013);

-- ethyl loflazepate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095467);

-- halazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(801396);

-- ketazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003946);

-- lorazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(791967);

-- medazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125106);

-- nordazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080959);

-- oxazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(724816);

-- prazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050461);

-- tofisopam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;benzodiazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100773);

-- acebutolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319998);

-- alprenolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081284);

-- atenolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1314002);

-- betaxolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1322081);

-- bisoprolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1338005);

-- bopindolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018640);

-- bupranolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018489);

-- carazolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19025945);

-- carteolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(950370);

-- carvedilol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1346823);

-- celiprolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19049145);

-- esmolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19063575);

-- labetalol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1386957);

-- levobunolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(932815);

-- mepindolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072028);

-- metipranolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(905531);

-- metoprolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1307046);

-- nadolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1313200);

-- oxprenolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19024904);

-- penbutolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1327978);

-- pindolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1345858);

-- practolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135791);

-- propranolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1353766);

-- sotalol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1370109);

-- talinolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100435);

-- tertatolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100451);

-- timolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_blocker;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(902427);

-- avibactam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_lactamase_inhibitors;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46221507);

-- Clavulanate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_lactamase_inhibitors;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1759842);

-- Sulbactam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_lactamase_inhibitors;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836241);

-- tazobactam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;beta_lactamase_inhibitors;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1741122);

-- Alendronate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1557272);

-- Alfacalcidol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19014202);

-- bazedoxifene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44506794);

-- Calcifediol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19035569);

-- Calcitonin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900359);

-- Calcitriol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19035631);

-- Cholecalciferol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095164);

-- Clodronic Acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19024249);

-- denosumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40222444);

-- Dihydrotachysterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027015);

-- Doxercalciferol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1512446);

-- Ergocalciferol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045045);

-- Etidronate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1552929);

-- Ibandronate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1512480);

-- methylene diphosphonate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072085);

-- Raloxifene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1513103);

-- Risedronate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1516800);

-- risedronic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19115051);

-- salmon calcitonin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1537655);

-- Tamoxifen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1436678);

-- Teriparatide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1521987);

-- Tiludronate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1578445);

-- tiludronic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011064);

-- Toremifene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1342346);

-- Vitamin D (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009405);

-- zoledronic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1524674);

-- pamidronate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;bone_density_conservation;pamidronate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1511646);

-- amlodipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1332418);

-- bencyclane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19016320);

-- bepridil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319751);

-- cinnarizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097481);

-- clevidipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19089969);

-- diltiazem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1328165);

-- felodipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1353776);

-- fendiline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19053866);

-- flunarizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055183);

-- gallopamil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19057715);

-- inamrinone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017805);

-- isradipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1326012);

-- lacidipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004539);

-- lercanidipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19015802);

-- lidoflazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19124331);

-- magnesium sulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19093848);

-- magnesium sulfate heptahydrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19091804);

-- manidipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19071995);

-- mepirodipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19102106);

-- mibefradil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1345141);

-- nicardipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1318137);

-- nifedipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1318853);

-- nilvadipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19113063);

-- nimodipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319133);

-- nisoldipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319880);

-- nitrendipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020061);

-- octylonium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19016099);

-- osthol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45776162);

-- perhexiline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19032359);

-- pinaverium (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19025198);

-- prenylamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051285);

-- risedronate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1516800);

-- risedronic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19115051);

-- terodiline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100440);

-- tolfenamic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042155);

-- tranilast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19120306);

-- verapamil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1307863);

-- ziconotide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;calcium_channel;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005061);

-- doripenem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;carbapenem;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1713905);

-- ertapenem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;carbapenem;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1717963);

-- meropenem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;carbapenem;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1709170);

-- Cefadroxil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;first_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1769535);

-- Cefatrizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;first_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19070680);

-- Cefazolin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;first_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1771162);

-- Cephalexin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;first_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1786621);

-- Cephaloridine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;first_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19052683);

-- Cephalothin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;first_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19086759);

-- Cephapirin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;first_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19086790);

-- Cephradine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;first_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1786842);

-- cefepime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;fourth_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1748975);

-- cefpirome (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;fourth_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19001904);

-- Cefaclor (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1768849);

-- Cefamandole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19070174);

-- Cefmetazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072255);

-- Cefonicid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072857);

-- ceforanide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19028288);

-- Cefotetan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1774932);

-- Cefotiam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051271);

-- Cefoxitin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1775741);

-- cefprozil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738366);

-- Cefuroxime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1778162);

-- loracarbef (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;second_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1708100);

-- cefdinir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1796458);

-- cefditoren (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1747005);

-- cefetamet (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19028241);

-- Cefixime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1796435);

-- cefodizime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19028286);

-- Cefoperazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1773402);

-- Cefotaxime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1774470);

-- cefpodoxime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1749008);

-- Cefsulodin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051345);

-- Ceftazidime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1776684);

-- ceftibuten (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1749083);

-- Ceftizoxime (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1777254);

-- Ceftriaxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1777806);

-- Moxalactam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cephalosporin;third_generation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19126622);

-- amphetamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(714785);

-- armodafinil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19090984);

-- atomoxetine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(742185);

-- benzphetamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(719128);

-- caffeine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1134439);

-- dexmethylphenidate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(731533);

-- dextroamphetamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(719311);

-- dimethylamphetamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012262);

-- doxapram (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(738152);

-- ephedrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1143374);

-- fenethylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19061288);

-- lisdexamfetamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(709567);

-- mazindol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(794229);

-- megestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1300978);

-- methamphetamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(704053);

-- methylphenidate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(705944);

-- modafinil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(710650);

-- nikethamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19019088);

-- pemoline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(727835);

-- pentylenetetrazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19031345);

-- phendimetrazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(723344);

-- phenmetrazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19034281);

-- phentermine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(735340);

-- pholedrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19090831);

-- prolintane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029018);

-- strychnine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19094153);

-- strychnine nitrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cns_stimulant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900206);

-- Barium Sulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011331);

-- Diatrizoate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19022596);

-- Ethiodized Oil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050016);

-- ferumoxides (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19015703);

-- Fluorescein (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(996625);

-- Gadobenate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19007652);

-- gadobutrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19048493);

-- gadodiamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19103699);

-- gadofosveset (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012718);

-- Gadopentetate Dimeglumine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097468);

-- gadoterate meglumine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43526404);

-- gadoteridol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097463);

-- gadoversetamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19089496);

-- gadoxetate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19031583);

-- iobitridol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19053540);

-- Iodamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080542);

-- Iodipamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080817);

-- iodixanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003201);

-- iodoxamid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19067248);

-- Iohexol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080985);

-- Iopamidol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081224);

-- Iopanoic Acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081256);

-- iopentol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069110);

-- Iophendylate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081269);

-- iopromide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003206);

-- Iothalamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19122158);

-- Iothalamic Acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775579);

-- Iotrolan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19067271);

-- ioversol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069131);

-- Ioxaglate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081316);

-- ioxilan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003249);

-- Ipodate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081385);

-- MANGAFODIPIR (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092105);

-- Meglumine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125279);

-- meglumine iotroxinate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19113046);

-- meglumine ioxithalamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009054);

-- Metrizamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125554);

-- oxypolygelatine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19082895);

-- PERFLUOROPERHYDROPHENANTHRENE (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775726);

-- Perflutren (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19071160);

-- Propyliodone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054048);

-- Tyropanoate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;contrast_media;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19006659);

-- efavirenz (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inducer;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738135);

-- modafinil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inducer;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(710650);

-- montelukast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inducer;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1154161);

-- nevirapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inducer;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1769389);

-- phenobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inducer;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(734275);

-- phenytoin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inducer;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(740910);

-- rifampin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inducer;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1763204);

-- troglitazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inducer;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1515249);

-- abiraterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40239056);

-- allyl sulfide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1330144);

-- amiodarone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309944);

-- cimetidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(997276);

-- ciprofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1797513);

-- clarithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1750500);

-- clotrimazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1000632);

-- cobicistat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42874220);

-- econazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(940864);

-- efavirenz (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738135);

-- enoxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1743222);

-- fluconazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1754994);

-- gemfibrozil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1558242);

-- itraconazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1703653);

-- ketoconazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(985708);

-- levofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1742253);

-- lopinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738170);

-- mibefradil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1345141);

-- miconazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(907879);

-- norfloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1721543);

-- ofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(923081);

-- orphenadrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(724394);

-- pefloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027679);

-- piperine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900463);

-- posaconazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1704139);

-- quinidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1360421);

-- ritonavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1748921);

-- saquinavir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1746244);

-- ticlopidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1302398);

-- tioconazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(942028);

-- trimethoprim (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1705674);

-- voriconazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;cytochrome_p450_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1714277);

-- acetazolamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(929435);

-- amiloride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(991382);

-- azosemide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018517);

-- bendroflumethiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1316354);

-- bumetanide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(932745);

-- buthiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19039770);

-- canrenoate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19123864);

-- canrenoic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19132906);

-- canrenone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19019698);

-- chlorothiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(992590);

-- chlorthalidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1395058);

-- cicletanine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051444);

-- clopamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19099181);

-- cyclopenthiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010015);

-- cyclothiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19082886);

-- drospirenone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1512674);

-- eplerenone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309799);

-- ethacrynate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(987406);

-- hydrochlorothiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(974166);

-- hydroflumethiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1376289);

-- ibopamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19113028);

-- indapamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(978555);

-- isosorbide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1383815);

-- mefruside (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000556);

-- methazolamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(904250);

-- methyclothiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(905273);

-- metolazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(907013);

-- piretanide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19025498);

-- polythiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(948787);

-- potassium citrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(976545);

-- potassium citrate anhydrous (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43526341);

-- quinethazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081320);

-- spironolactone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(970250);

-- torsemide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(942350);

-- triamterene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(904542);

-- trichlormethiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(904639);

-- xipamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010493);

-- furosemide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;furosemide;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(956874);

-- mannitol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;diuretic;mannitol;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(994058);

-- bisphenol a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43560342);

-- chlorotrianisene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092684);

-- dienestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(925102);

-- diethylstilbestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1525866);

-- estradiol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1548195);

-- estrogens, conjugated (usp) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1549080);

-- estrogens, esterified (usp) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1551673);

-- estrone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1549254);

-- ethinyl estradiol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1549786);

-- genistein (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097682);

-- mestranol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1503184);

-- polyestradiol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19094980);

-- quinestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19060423);

-- synthetic conjugated estrogens, a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1596779);

-- synthetic conjugated estrogens, b (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1586808);

-- zeranol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013877);

-- aminoglutethimide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1503057);

-- anastrozole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1348265);

-- clomiphene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1598819);

-- danazol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1511449);

-- exemestane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1398399);

-- formestane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020079);

-- fulvestrant (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1304044);

-- hydroxyprogesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19077143);

-- letrozole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1315946);

-- naringenin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014124);

-- raloxifene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1513103);

-- saw palmetto extract (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1391307);

-- tamoxifen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1436678);

-- bazedoxifene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_receptor_modulator;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44506794);

-- clomiphene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_receptor_modulator;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1598819);

-- raloxifene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_receptor_modulator;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1513103);

-- tamoxifen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_receptor_modulator;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1436678);

-- tibolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_receptor_modulator;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041933);

-- toremifene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;estrogen_receptor_modulator;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1342346);

-- monofluorophosphate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoride;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19068715);

-- Sodium Fluoride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoride;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069022);

-- Ciprofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1797513);

-- Enoxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1743222);

-- Fleroxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050750);

-- gatifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1789276);

-- gemifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1716721);

-- grepafloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1747032);

-- Levofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1742253);

-- lomefloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1707800);

-- moxifloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1716903);

-- Norfloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1721543);

-- Ofloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(923081);

-- Pefloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027679);

-- sparfloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1733765);

-- temafloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041153);

-- trovafloxacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;fluoroquinolon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1712549);

-- alclometasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(905151);

-- amcinonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(930747);

-- beclomethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1115572);

-- betamethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(920458);

-- budesonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(939259);

-- ciclesonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(902938);

-- clobetasol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(998415);

-- clobetasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005129);

-- clocortolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(950882);

-- desoximetasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(917336);

-- dexamethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1518254);

-- diflorasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(960988);

-- diflucortolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026096);

-- difluprednate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19089579);

-- flumethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055156);

-- fluocinolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(996541);

-- fluocinonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(955252);

-- fluocortin butyl ester (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19064432);

-- fluocortolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055344);

-- fluorometholone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(955583);

-- fluprednidene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056447);

-- fluprednisolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19111234);

-- flurandrenolide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(956266);

-- medrysone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(909021);

-- melengestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44818512);

-- methylprednisolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1506270);

-- paramethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027186);

-- prednicarbate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(928110);

-- prednisolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1550557);

-- prednisone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1551099);

-- rimexolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(977421);

-- triamcinolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;glucocorticoid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(903963);

-- Auranofin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;gold_preparation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1114771);

-- Aurothioglucose (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;gold_preparation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1163570);

-- Aurothiomalate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;gold_preparation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1152134);

-- sodium thiosulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;gold_preparation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(940004);

-- albiglutide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816332);

-- alclometasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(905151);

-- algestone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080969);

-- allylestrenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19068397);

-- alogliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013884);

-- altrenogest (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42903898);

-- amcinonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(930747);

-- beclomethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1115572);

-- betamethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(920458);

-- bisphenol a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43560342);

-- budesonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(939259);

-- chlorotrianisene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092684);

-- ciclesonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(902938);

-- clobetasol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(998415);

-- clobetasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005129);

-- clocortolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(950882);

-- corticotropin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1541079);

-- corticotropin-releasing hormone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19048699);

-- cosyntropin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19008009);

-- deoxycorticosterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017895);

-- desogestrel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1588000);

-- desoximetasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(917336);

-- dexamethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1518254);

-- dienestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(925102);

-- diethylstilbestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1525866);

-- diflorasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(960988);

-- diflucortolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026096);

-- difluprednate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19089579);

-- dydrogesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19040060);

-- estradiol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1548195);

-- estrogens, conjugated (usp) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1549080);

-- estrogens, esterified (usp) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1551673);

-- estrone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1549254);

-- ethinyl estradiol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1549786);

-- ethylestrenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050387);

-- exenatide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1583722);

-- flumethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055156);

-- fluocinolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(996541);

-- fluocinonide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(955252);

-- fluocortin butyl ester (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19064432);

-- fluocortolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055344);

-- fluorometholone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(955583);

-- fluoxymesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1555887);

-- fluprednidene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056447);

-- fluprednisolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19111234);

-- flurandrenolide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(956266);

-- follicle stimulating hormone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1588712);

-- genistein (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097682);

-- gestodene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097684);

-- gestrinone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059522);

-- glucagon (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1560278);

-- hydroxyprogesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19077143);

-- linagliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40239216);

-- liraglutide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40170911);

-- medrysone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(909021);

-- melengestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44818512);

-- mesterolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003140);

-- mestranol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1503184);

-- methandriol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004095);

-- methandrostenolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004098);

-- methenolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004621);

-- methylprednisolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1506270);

-- methyltestosterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1506602);

-- nandrolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1514412);

-- norethandrolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19128219);

-- oxandrolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1524769);

-- oxymetholone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1525741);

-- paramethasone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027186);

-- polyestradiol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19094980);

-- prednicarbate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(928110);

-- prednisolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1550557);

-- prednisone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1551099);

-- progesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1552310);

-- quinestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19060423);

-- rimexolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(977421);

-- saxagliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40166035);

-- secretin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19066188);

-- sitagliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1580747);

-- stanozolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1636145);

-- synthetic conjugated estrogens, a (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1596779);

-- synthetic conjugated estrogens, b (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1586808);

-- testosterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1636780);

-- tetrahydrocannabinol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1037005);

-- thyrotropin-releasing hormone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19001701);

-- tibolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041933);

-- trenbolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44784980);

-- triamcinolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(903963);

-- zeranol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013877);

-- Growth hormone (somatropine) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;growth_hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1584910);

-- somatostatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hormone;somatostatin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136066);

-- acecarbromal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029370);

-- alprazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(781039);

-- amobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(712757);

-- barbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19015346);

-- brotizolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19039262);

-- butabarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(959174);

-- butobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19039767);

-- chlordiazepoxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(990678);

-- chlormethiazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092283);

-- detomidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19088035);

-- dexmedetomidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19061088);

-- diazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(723013);

-- diphenhydramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1129625);

-- eszopiclone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(757352);

-- ethchlorvynol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(749727);

-- etomidate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050488);

-- flurazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(756349);

-- fospropofol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19044273);

-- glutethimide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19061124);

-- hexobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19068964);

-- lorazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(791967);

-- lormetazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19007977);

-- medazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125106);

-- medetomidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19067415);

-- mephobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(702685);

-- meprobamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(702865);

-- methapyrilene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004158);

-- methaqualone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004160);

-- midazolam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(708298);

-- nitrazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020021);

-- oleylamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014153);

-- oxazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(724816);

-- paraldehyde (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19027181);

-- pentobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(730729);

-- phenobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(734275);

-- propofol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(753626);

-- proxibarbal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19134133);

-- quazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(731188);

-- remifentanil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19016749);

-- secobarbital (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(766067);

-- temazepam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(836715);

-- thiamylal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004295);

-- thiopental (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(700253);

-- trichloroacetaldehyde (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(742594);

-- trazodone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(703547);

-- xylazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40238058);

-- zaleplon (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(720727);

-- zolpidem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(744740);

-- zopiclone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypnotic_sedativ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19044883);

-- acipimox (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029644);

-- Ascorbate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013471);

-- benfluorex (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19035533);

-- beta Sitosterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19112540);

-- Betaine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1520218);

-- Bezafibrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19022956);

-- Chitosan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1380341);

-- Cholestyramine Resin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095309);

-- Choline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1195334);

-- Choline Hydroxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42898956);

-- ciprofibrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19050375);

-- Clofibrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1598658);

-- colesevelam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1518148);

-- Colestipol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1501617);

-- Ethionamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1750074);

-- etofibrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19060156);

-- Evening primrose oil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045290);

-- evolocumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46287466);

-- ezetimibe (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1526475);

-- Fenofibrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1551803);

-- gamma-oryzanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19065867);

-- Gemfibrozil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1558242);

-- GUGGUL LIPIDS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19070952);

-- gugulu extract (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1400992);

-- isoniazid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1782521);

-- mipomersen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014182);

-- policosanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1508241);

-- Probucol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051463);

-- Pyridinolcarbamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059744);

-- tiadenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100753);

-- Triclosan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1704758);

-- Niacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;niacin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1517824);

-- atorvastatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;statin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1545958);

-- fluvastatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;statin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1549686);

-- Lovastatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;statin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1592085);

-- pitavastatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;statin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40165636);

-- Pravastatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;statin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1551860);

-- red yeast rice (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;statin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1389885);

-- rosuvastatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;statin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1510813);

-- Simvastatin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;hypolipidemic;statin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1539403);

-- Anthrax immune globulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35605786);

-- bezlotoxumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1718211);

-- cytomegalovirus immune globulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(586491);

-- Diphtheria Antitoxin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19031041);

-- hepatitis B immune globulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(501343);

-- Human vaccinia immune globulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19122168);

-- palivizumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(537647);

-- rabies immune globulin, human (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135830);

-- raxibacumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013193);

-- Rho(D) Immune Globulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(535714);

-- Tetanus immune globulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35604680);

-- varicella-zoster immune globulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunoglobulins;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(543291);

-- abatacept (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1186087);

-- Antilymphocyte Immunoglobulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136041);

-- Azathioprine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19014878);

-- basiliximab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19038440);

-- belatacept (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40239665);

-- belimumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40236987);

-- benzonidazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018545);

-- Busulfan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1333357);

-- C1 esterase inhibitor (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45892906);

-- certolizumab pegol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(912263);

-- Cladribine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054825);

-- Cyclophosphamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1310317);

-- Cytarabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1311078);

-- Daclizumab (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19036892);

-- deflazacort (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19086888);

-- dimethyl fumarate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43526424);

-- Etanercept (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1151789);

-- everolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011440);

-- fingolimod (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40226579);

-- fludarabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1395557);

-- Fluorouracil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(955632);

-- gemcitabine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1314924);

-- Glatiramer (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(751889);

-- glimepiride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1597756);

-- Gliotoxin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46276250);

-- hypericin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775583);

-- icatibant (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40242044);

-- leflunomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1101898);

-- Lymphocyte Immune Globulin, Anti-Thymocyte Globulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136207);

-- Melphalan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1301267);

-- mercaptopurine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1436650);

-- Methotrexate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1305058);

-- Muromonab-CD3 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051865);

-- Pimecrolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915935);

-- Sirolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19034726);

-- Tacrolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(950637);

-- Thalidomide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19137042);

-- Thiotepa (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19137385);

-- thymocyte immunoglobulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43532538);

-- Triamcinolone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(903963);

-- trofosfamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19101677);

-- Cyclosporine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;immunosuppressive;cyclosporine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010482);

-- bumetanide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;loop_diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(932745);

-- furosemide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;loop_diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(956874);

-- piretanide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;loop_diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19025498);

-- torsemide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;loop_diuretic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(942350);

-- Azithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1734104);

-- Clarithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1750500);

-- dirithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1790024);

-- Erythromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1746940);

-- Josamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19123240);

-- midecamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072122);

-- Miocamycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009138);

-- Oleandomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19023254);

-- Roxithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19063874);

-- Spiramycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19070251);

-- telithromycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1702911);

-- Troleandomycin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;macrolyd;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19006043);

-- buprenorphine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1133201);

-- butorphanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1133732);

-- levallorphan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19124297);

-- lofexidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19007803);

-- meptazinol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003010);

-- methylnaltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(909841);

-- nalbuphine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1114122);

-- nalmefene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19014158);

-- naloxegol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45774613);

-- naloxone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1114220);

-- naltrexone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1714319);

-- pentazocine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;narcotic_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1130585);

-- aceclofenac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029393);

-- acemetacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029398);

-- acexamic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19025107);

-- adapalene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(981774);

-- alminoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018431);

-- antipyrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1036059);

-- apazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136654);

-- apremilast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816294);

-- aspirin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1112807);

-- baicalin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44506770);

-- balsalazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(934262);

-- benorilate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19016435);

-- bromfenac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1139179);

-- bufexamac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19032724);

-- bumadizone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19039703);

-- butibufen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020568);

-- carprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19046454);

-- caryophyllene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014045);

-- celecoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1118084);

-- chloroquine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1792515);

-- choline magnesium trisalicyclate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1157456);

-- clonixin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19099129);

-- curcumin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072030);

-- dexketoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056874);

-- diclofenac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1124300);

-- diflunisal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1126128);

-- dipyrone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19031397);

-- droxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056645);

-- etanercept (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1151789);

-- ethenzamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059506);

-- etodolac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1195492);

-- etofenamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095561);

-- etoricoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011355);

-- evening primrose oil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045290);

-- felbinac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100761);

-- fenbufen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095703);

-- fenoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1153928);

-- feprazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19135796);

-- ferulate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014099);

-- floctafenine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054931);

-- flunixin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19096546);

-- flurbiprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1156378);

-- glucametacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19098123);

-- ibuprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1177480);

-- icatibant (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40242044);

-- imidazole-2-hydroxybenzoate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069104);

-- indobufen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069107);

-- indomethacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1178663);

-- kebuzone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069191);

-- ketoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1185922);

-- ketorolac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1136980);

-- lonazolac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19071933);

-- lornoxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19049709);

-- loxoprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19071936);

-- magnesium salicylate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1168079);

-- masoprocol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(988447);

-- meclofenamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1180182);

-- meclofenamic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19125097);

-- mefenamate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1197736);

-- meloxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1150345);

-- mesalamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(968426);

-- mofebutazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072129);

-- mofezolac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011062);

-- nabumetone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1113648);

-- naproxen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1115008);

-- nepafenac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(909795);

-- niflumic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19019023);

-- nimesulide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069425);

-- olopatadine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915855);

-- olsalazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(916282);

-- orgotein (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19082874);

-- oxaprozin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1118045);

-- oxyphenbutazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19025925);

-- parecoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003570);

-- parthenolide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19083839);

-- phenylbutazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1135710);

-- pimecrolimus (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(915935);

-- pirfenidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775206);

-- piroxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1146810);

-- proglumetacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029327);

-- propyphenazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029322);

-- resveratrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40225716);

-- rofecoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1189754);

-- salicin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100313);

-- salicylamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1164108);

-- salicylic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(964407);

-- salsalate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1137460);

-- serratiopeptidase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19100379);

-- sulfasalazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(964339);

-- sulindac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1236607);

-- suprofen (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1036636);

-- tenoxicam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19041220);

-- tiaprofenate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19086910);

-- tolfenamic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042155);

-- tolmetin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1102917);

-- tranilast (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19120306);

-- tribenoside (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042575);

-- ursolate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43014258);

-- valdecoxib (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1103374);

-- zileuton (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1147878);

-- zomepirac (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19102108);

-- 2-diethylaminoethanol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;nsaid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43559949);

-- penicillamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillamine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(7975);

-- Amoxicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1713332);

-- Ampicillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1717327);

-- Dicloxacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724666);

-- Nafcillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1713930);

-- Oxacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1724703);

-- Penicillin G (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1728416);

-- Penicillin V (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1729720);

-- Piperacillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1746114);

-- Ticarcillin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;penicillin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1702364);

-- Pentamidine (antifungal, antiprotozoan) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;pentamidine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1730370);

-- algestone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080969);

-- allylestrenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19068397);

-- altrenogest (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42903898);

-- desogestrel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1588000);

-- dydrogesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19040060);

-- gestodene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097684);

-- gestrinone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059522);

-- hydroxyprogesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19077143);

-- progesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;progestin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1552310);

-- baicalein (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;prostaglandin_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45775438);

-- tolfenamic acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;prostaglandin_antagonist;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042155);

-- 4-coumaric acid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44785506);

-- albuterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1154343);

-- algestone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080969);

-- atosiban (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19122136);

-- buserelin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19033280);

-- carboprost (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19049150);

-- cetrorelix (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1503983);

-- chlormadinone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026733);

-- chorionic gonadotropin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1563600);

-- clomiphene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1598819);

-- cloprostenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19099183);

-- cyclofenil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19072083);

-- cyproterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010792);

-- desogestrel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1588000);

-- dienogest (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19054876);

-- dinoprost (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19029421);

-- dinoprostone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1329415);

-- ergonovine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1345205);

-- ergotamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1145379);

-- estradiol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1548195);

-- estropipate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1525278);

-- ethynodiol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1595461);

-- etonogestrel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1519936);

-- fenoterol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19053979);

-- gemeprost (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19011126);

-- gestodene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097684);

-- gestrinone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059522);

-- hexoprenaline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19068969);

-- indomethacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1178663);

-- isoxsuprine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1384360);

-- kininogenase (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19123342);

-- leuprolide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1351541);

-- levonorgestrel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1589505);

-- lynestrenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092358);

-- magnesium sulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19093848);

-- magnesium sulfate heptahydrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19091804);

-- medroxyprogesterone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1500211);

-- megestrol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1300978);

-- metaproterenol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1123995);

-- methotrexate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1305058);

-- methylergonovine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1305637);

-- mifepristone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1508439);

-- misoprostol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1150871);

-- nafarelin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1507558);

-- nifedipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1318853);

-- nonoxynol-4 sulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43012830);

-- nonoxynol-9 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(969444);

-- norelgestromin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1518198);

-- norethindrone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1521369);

-- norethynodrel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19021481);

-- norgestimate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1515774);

-- norgestrel (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1521592);

-- norgestrienone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19128258);

-- nylidrin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19022494);

-- octoxynol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(916059);

-- octoxynol-9 (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(970277);

-- oxytocin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1326115);

-- ritodrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1563413);

-- terbutaline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1236744);

-- trilostane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19042801);

-- triptorelin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;reproductive_control;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1343039);

-- Sulfadoxine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836410);

-- Sulfaguanidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000819);

-- Sulfalene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136423);

-- Sulfamerazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136426);

-- Sulfamethazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136429);

-- Sulfamethizole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1036425);

-- Sulfamethoxazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836430);

-- Sulfamethoxypyridazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000820);

-- Sulfamoxole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136481);

-- Sulfanilamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1036475);

-- Sulfapyridine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136493);

-- Sulfaquinoxaline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(42900099);

-- Sulfasalazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(964339);

-- sulfathiazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1036487);

-- Sulfisomidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000821);

-- Sulfisoxazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfa_drugs;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836503);

-- Acetohexamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1530014);

-- Carbutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19033498);

-- Chlorpropamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1594973);

-- glibornuride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19001409);

-- Gliclazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059796);

-- glimepiride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1597756);

-- Glipizide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1560171);

-- gliquidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097821);

-- Glyburide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1559684);

-- Tolazamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1502809);

-- Tolbutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;sulfonylurea;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1502855);

-- Chlortetracycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095043);

-- clomocycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19047265);

-- Demeclocycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1714527);

-- Doxycycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1738521);

-- Lymecycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19092353);

-- Methacycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19003644);

-- Minocycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1708880);

-- Oxytetracycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(925952);

-- Rolitetracycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19136024);

-- Tetracycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1836948);

-- tigecycline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;tetracyclin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1742432);

-- bendroflumethiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1316354);

-- chlorothiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(992590);

-- chlorthalidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1395058);

-- cyclopenthiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19010015);

-- hydrochlorothiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(974166);

-- hydroflumethiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1376289);

-- indapamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(978555);

-- methyclothiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(905273);

-- metolazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(907013);

-- polythiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(948787);

-- quinethazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19081320);

-- trichlormethiazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thiazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(904639);

-- thyroxine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thyroid_hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1501700);

-- liothyronine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thyroid_hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1505346);

-- thyroglobulin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thyroid_hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19000732);

-- desiccated thyroid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;thyroid_hormone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1501309);

-- Acetylcholine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19037624);

-- Adenosine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309204);

-- ajmalicine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018408);

-- Alprostadil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1381504);

-- Amiodarone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309944);

-- Amlodipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1332418);

-- Amyl Nitrite (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319051);

-- bamethan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19015342);

-- bamifylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018518);

-- Bencyclane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19016320);

-- Bepridil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319751);

-- BETAHISTINE (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020124);

-- buflomedil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19039639);

-- butalamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018488);

-- cafedrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19058510);

-- carvedilol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1346823);

-- Celiprolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19049145);

-- cetiedil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19031267);

-- Chromonar (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19063698);

-- cilostazol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1350310);

-- Cyclandelate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309323);

-- Diazoxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1523280);

-- Dihydroergocristine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026526);

-- Dihydroergocryptine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19026564);

-- Diltiazem (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1328165);

-- Dipyridamole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1331270);

-- dopexamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056154);

-- drotaverin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19056611);

-- Dyphylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1140088);

-- Enoximone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19063454);

-- ergoloid mesylates, USP (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(745268);

-- Erythritol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013623);

-- Erythrityl Tetranitrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045644);

-- Felodipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1353776);

-- Fenoldopam (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095721);

-- flosequinan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19096489);

-- Flunarizine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19055183);

-- Hexobendine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19113060);

-- Hydralazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1373928);

-- ibopamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19113028);

-- ifenprodil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19069103);

-- Iloprost (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1344992);

-- Inamrinone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017805);

-- Inositol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1379525);

-- Isosorbide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1383815);

-- Isosorbide Dinitrate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1383925);

-- Isoxsuprine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1384360);

-- Isradipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1326012);

-- Khellin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19123346);

-- Levosimendan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40173184);

-- Lidoflazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19124331);

-- Mibefradil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1345141);

-- Milrinone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1368671);

-- Minoxidil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1309068);

-- Molsidomine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009819);

-- Moxisylyte (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19007422);

-- Nafronyl (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19014035);

-- nebivolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1314577);

-- Niacin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1517824);

-- Nicardipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1318137);

-- Nicergoline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018352);

-- nicofuranose (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004049);

-- Nicorandil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19014977);

-- Nicotinyl Alcohol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19018823);

-- Nifedipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1318853);

-- Nimodipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319133);

-- Nisoldipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1319880);

-- Nitrendipine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020061);

-- Nitric Oxide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020068);

-- Nitroglycerin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1361711);

-- Nitroprusside (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19020994);

-- Nylidrin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19022494);

-- Oxprenolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19024904);

-- Oxyfedrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19129643);

-- Papaverine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1326901);

-- pentaerythritol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19022446);

-- Pentoxifylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1331247);

-- Perhexiline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19032359);

-- Phenoxybenzamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1335301);

-- pimobendan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40239290);

-- Pinacidil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19093166);

-- Pindolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1345858);

-- Prenylamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19051285);

-- Propranolol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1353766);

-- sildenafil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1316262);

-- tadalafil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1336926);

-- Theobromine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19137056);

-- Theophylline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1237049);

-- Tolazoline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19002829);

-- Trapidil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19009721);

-- Trimetazidine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005626);

-- Trimethaphan (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005658);

-- urapidil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19101849);

-- vardenafil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1311276);

-- Verapamil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1307863);

-- vinburnine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19078938);

-- Vincamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19008269);

-- vinpocetine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095462);

-- Xanthinol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;concomitant;vasodilators;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059142);

-- isocarboxazid (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;mao;isocarboxazid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(781705);

-- phenelzine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;mao;phenelzine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(733896);

-- selegiline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;mao;selegiline;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(766209);

-- tranylcypromine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;mao;tranylcypromine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(703470);

-- mirtazapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;nassa;mirtazapine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(725131);

-- bupropion (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ndri;bupropion;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(750982);

-- desvenlafaxine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;snri;desvenlafaxine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(717607);

-- duloxetine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;snri;duloxetine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(715259);

-- levomilnacipran (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;snri;levomilnacipran;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43560354);

-- milnacipran (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;snri;milnacipran;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19080226);

-- venlafaxine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;snri;venlafaxine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(743670);

-- citalopram (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ssri;citalopram;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(797617);

-- escitalopram (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ssri;escitalopram;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(715939);

-- fluoxetine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ssri;fluoxetine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(755695);

-- fluvoxamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ssri;fluvoxamine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(751412);

-- paroxetine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ssri;paroxetine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(722031);

-- sertraline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ssri;sertraline;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(739138);

-- vilazodone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ssri;vilazodone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40234834);

-- vortioxetine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;ssri;vortioxetine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44507700);

-- amoxapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;amoxapine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(713109);

-- clomipramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;clomipramine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(798834);

-- desipramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;desipramine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(716968);

-- doxepin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;doxepin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(738156);

-- imipramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;imipramine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(778268);

-- maprotiline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;maprotiline;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(794147);

-- protriptyline (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;protriptyline;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(754270);

-- trimipramine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;anti_depressant;tri_and_tetracyclic;trimipramine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(705755);

-- clozapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;clozapine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(800878);

-- chlorpromazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;chlorpromazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(794852);

-- chlorprothixene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;chlorprothixene;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19095002);

-- fluphenazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;fluphenazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(756018);

-- haloperidol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;haloperidol;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(766529);

-- loxapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;loxapine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(792263);

-- mesoridazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;mesoridazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(703083);

-- methotrimeprazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;methotrimeprazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005147);

-- molindone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;molindone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(709699);

-- perazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;perazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19131663);

-- perphenazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;perphenazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(733008);

-- pimozide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;pimozide;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(745790);

-- promazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;promazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19052903);

-- thioridazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;thioridazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(700299);

-- thiothixene (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;thiothixene;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(700465);

-- trifluoperazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;trifluoperazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(704984);

-- triflupromazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;first_generation_antipsychotic;triflupromazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19005104);

-- lithium carbonate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;lithium_carbonate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(751246);

-- carbamazepine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;mood_stabilizer;carbamazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(740275);

-- lamotrigine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;mood_stabilizer;lamotrigine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(705103);

-- oxcarbazepine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;mood_stabilizer;oxcarbazepine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(718122);

-- valproate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;mood_stabilizer;valproate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(745466);

-- aripiprazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;aripiprazole;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(757688);

-- asenapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;asenapine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40164052);

-- brexpiprazole (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;brexpiprazole;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46275300);

-- cariprazine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;cariprazine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(35603277);

-- iloperidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;iloperidone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19017241);

-- lurasidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;lurasidone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40230761);

-- olanzapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;olanzapine;',
            concept_id, concept_name
       FROM (
             SELECT _olanzapine_and_fluoxetine.concept_id,
                       _olanzapine_and_fluoxetine.concept_name
                  FROM :work_schema.codex_concept_descendants(785788) AS _olanzapine_and_fluoxetine
             LEFT JOIN :work_schema.codex_concept_descendants(755695) AS _fluoxetine USING (concept_id)
                 WHERE (_fluoxetine.concept_id IS NULL)
            ) AS concept;

-- paliperidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;paliperidone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(703244);

-- quetiapine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;quetiapine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(766814);

-- risperidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;risperidone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(735979);

-- ziprasidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;drug_of_interest;second_generation_antipsychotic;ziprasidone;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(712615);

-- donepezil (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;cholinesterase_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(715997);

-- galantamine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;cholinesterase_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(757627);

-- rivastigmine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;cholinesterase_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(733523);

-- tacrine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;cholinesterase_inhibitor;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(836654);

-- insulin detemir (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1516976);

-- Insulin Glargine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1502905);

-- Insulin Lispro (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1550023);

-- Insulin, Glulisine, Human (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1544838);

-- insulin, isophane (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46221581);

-- Insulin, Protamine Zinc, Beef-Pork (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19013926);

-- insulin, regular, beef-pork (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1590165);

-- Insulin, Regular, Pork (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1586346);

-- Acarbose (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1529331);

-- Acetohexamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1530014);

-- alogliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43013884);

-- canagliflozin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43526465);

-- Carbutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19033498);

-- Chlorpropamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1594973);

-- dulaglutide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45774435);

-- empagliflozin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45774751);

-- exenatide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1583722);

-- fenugreek seed meal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19004145);

-- glibornuride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19001409);

-- Gliclazide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19059796);

-- glimepiride (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1597756);

-- Glipizide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1560171);

-- gliquidone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19097821);

-- Glyburide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1559684);

-- Linagliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40239216);

-- liraglutide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40170911);

-- Metformin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1503297);

-- miglitol (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1510202);

-- Miglustat (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19045272);

-- nateglinide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1502826);

-- Phenformin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19033909);

-- pioglitazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1525215);

-- Pramlintide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1517998);

-- repaglinide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1516766);

-- rosiglitazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1547504);

-- saxagliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40166035);

-- sitagliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1580747);

-- Tolazamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1502809);

-- Tolbutamide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1502855);

-- troglitazone (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(1515249);

-- vanadyl sulfate (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19043266);

-- vildagliptin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(19122137);

-- albiglutide (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;drug;hypoglycemic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816332);

-- memantine (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;exclusion;memantine;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(701322);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007723);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2007724);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108577);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257488);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257654);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257555);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212118);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816350);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108573);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257739);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257410);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257697);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257553);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212119);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212122);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212106);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212111);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257696);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257552);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257738);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(46257591);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212113);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816347);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816361);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45889818);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212108);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212109);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44816351);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212121);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2212114);

-- all drugs of interest reflected in procedures (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890740);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617571);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786399);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617574);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40659692);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664583);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718424);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718425);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2720963);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718258);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786557);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718568);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718648);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718605);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718549);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786380);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718621);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2721001);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718315);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718584);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2718633);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2720945);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890662);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890663);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(45890665);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Drugs;mental_procedure;drug_of_interest;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40664472);
