
-- Diabetes mellitus (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250');

-- Diabetes mellitus without mention of complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.0');

-- Diabetes with other specified manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.8');

-- Diabetes with peripheral circulatory disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;circulatory;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.7');

-- Diabetes with other coma (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;coma;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.3');

-- Diabetes with ophthalmic manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.5');

-- Diabetic retinopathy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '362.0');

-- Background diabetic retinopathy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '362.01');

-- Proliferative diabetic retinopathy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '362.02');

-- Nonproliferative diabetic retinopathy NOS (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '362.03');

-- Mild nonproliferative diabetic retinopathy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '362.04');

-- Moderate nonproliferative diabetic retinopathy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '362.05');

-- Severe nonproliferative diabetic retinopathy (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '362.06');

-- Diabetic macular edema (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '362.07');

-- Diabetic cataract (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;eye;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '366.41');

-- Personal history of diabetic foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;foot;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z86.31');

-- Diabetes mellitus with hyperosmolarity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;hyperosmolarity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.2');

-- Diabetes with ketoacidosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;ketoacidosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.1');

-- Diabetes with neurological manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;neurologic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.6');

-- Polyneuropathy in diabetes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;neurologic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '357.2');

-- Diabetes with renal manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;renal;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.4');

-- Diabetes with unspecified complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;complication;unspecified;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.9');

-- Drug or chemical induced diabetes mellitus (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09');

-- Drug or chemical induced diabetes mellitus with hyperosmolarity (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.0');

-- Drug or chemical induced diabetes mellitus with hyperosmolarity (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.0');

-- Drug or chemical induced diabetes mellitus with hyperosmolarity without nonketotic hyperglycemic-hyperosmolar coma (NKHHC) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.00');

-- Drug or chemical induced diabetes mellitus with hyperosmolarity without nonketotic hyperglycemic-hyperosmolar coma (NKHHC) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.00');

-- Drug or chemical induced diabetes mellitus with hyperosmolarity without nonketotic hyperglycemic-hyperosmolar coma (NKHHC) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.00');

-- Drug or chemical induced diabetes mellitus with hyperosmolarity with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.01');

-- Drug or chemical induced diabetes mellitus with hyperosmolarity with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.01');

-- Drug or chemical induced diabetes mellitus with hyperosmolarity with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.01');

-- Drug or chemical induced diabetes mellitus with ketoacidosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.1');

-- Drug or chemical induced diabetes mellitus with ketoacidosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.1');

-- Drug or chemical induced diabetes mellitus with ketoacidosis without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.10');

-- Drug or chemical induced diabetes mellitus with ketoacidosis without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.10');

-- Drug or chemical induced diabetes mellitus with ketoacidosis with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.11');

-- Drug or chemical induced diabetes mellitus with ketoacidosis with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.11');

-- Drug or chemical induced diabetes mellitus with ketoacidosis with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.11');

-- Drug or chemical induced diabetes mellitus with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.2');

-- Drug or chemical induced diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.21');

-- Drug or chemical induced diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.21');

-- Drug or chemical induced diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.21');

-- Drug or chemical induced diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.22');

-- Drug or chemical induced diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.22');

-- Drug or chemical induced diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.22');

-- Drug or chemical induced diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.29');

-- Drug or chemical induced diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.29');

-- Drug or chemical induced diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.29');

-- Drug or chemical induced diabetes mellitus with ophthalmic complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.3');

-- Drug or chemical induced diabetes mellitus with unspecified diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.31');

-- Drug or chemical induced diabetes mellitus with unspecified diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.311');

-- Drug or chemical induced diabetes mellitus with unspecified diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.311');

-- Drug or chemical induced diabetes mellitus with unspecified diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.311');

-- Drug or chemical induced diabetes mellitus with unspecified diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.319');

-- Drug or chemical induced diabetes mellitus with unspecified diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.319');

-- Drug or chemical induced diabetes mellitus with unspecified diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.319');

-- Drug or chemical induced diabetes mellitus with mild nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.32');

-- Drug or chemical induced diabetes mellitus with mild nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.32');

-- Drug or chemical induced diabetes mellitus with mild nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.321');

-- Drug or chemical induced diabetes mellitus with mild nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.321');

-- Drug or chemical induced diabetes mellitus with mild nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.321');

-- Drug or chemical induced diabetes mellitus with mild nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.329');

-- Drug or chemical induced diabetes mellitus with mild nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.329');

-- Drug or chemical induced diabetes mellitus with mild nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.329');

-- Drug or chemical induced diabetes mellitus with moderate nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.33');

-- Drug or chemical induced diabetes mellitus with moderate nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.33');

-- Drug or chemical induced diabetes mellitus with moderate nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.331');

-- Drug or chemical induced diabetes mellitus with moderate nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.331');

-- Drug or chemical induced diabetes mellitus with moderate nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.331');

-- Drug or chemical induced diabetes mellitus with moderate nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.339');

-- Drug or chemical induced diabetes mellitus with moderate nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.339');

-- Drug or chemical induced diabetes mellitus with moderate nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.339');

-- Drug or chemical induced diabetes mellitus with severe nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.34');

-- Drug or chemical induced diabetes mellitus with severe nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.34');

-- Drug or chemical induced diabetes mellitus with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.341');

-- Drug or chemical induced diabetes mellitus with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.341');

-- Drug or chemical induced diabetes mellitus with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.341');

-- Drug or chemical induced diabetes mellitus with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.349');

-- Drug or chemical induced diabetes mellitus with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.349');

-- Drug or chemical induced diabetes mellitus with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.349');

-- Drug or chemical induced diabetes mellitus with proliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.35');

-- Drug or chemical induced diabetes mellitus with proliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.35');

-- Drug or chemical induced diabetes mellitus with proliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.351');

-- Drug or chemical induced diabetes mellitus with proliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.351');

-- Drug or chemical induced diabetes mellitus with proliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.351');

-- Drug or chemical induced diabetes mellitus with proliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.359');

-- Drug or chemical induced diabetes mellitus with proliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.359');

-- Drug or chemical induced diabetes mellitus with proliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.359');

-- Drug or chemical induced diabetes mellitus with diabetic cataract (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.36');

-- Drug or chemical induced diabetes mellitus with diabetic cataract (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.36');

-- Drug or chemical induced diabetes mellitus with diabetic cataract (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.36');

-- Drug or chemical induced diabetes mellitus with other diabetic ophthalmic complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.39');

-- Drug or chemical induced diabetes mellitus with other diabetic ophthalmic complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.39');

-- Drug or chemical induced diabetes mellitus with other diabetic ophthalmic complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.39');

-- Drug or chemical induced diabetes mellitus with neurological complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.4');

-- Drug or chemical induced diabetes mellitus with neurological complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.4');

-- Drug or chemical induced diabetes mellitus with neurological complications with diabetic neuropathy, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.40');

-- Drug or chemical induced diabetes mellitus with neurological complications with diabetic neuropathy, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.40');

-- Drug or chemical induced diabetes mellitus with neurological complications with diabetic mononeuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.41');

-- Drug or chemical induced diabetes mellitus with neurological complications with diabetic polyneuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.42');

-- Drug or chemical induced diabetes mellitus with neurological complications with diabetic autonomic (poly)neuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.43');

-- Drug or chemical induced diabetes mellitus with neurological complications with diabetic amyotrophy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.44');

-- Drug or chemical induced diabetes mellitus with neurological complications with other diabetic neurological complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.49');

-- Drug or chemical induced diabetes mellitus with circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.5');

-- Drug or chemical induced diabetes mellitus with diabetic peripheral angiopathy without gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.51');

-- Drug or chemical induced diabetes mellitus with diabetic peripheral angiopathy without gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.51');

-- Drug or chemical induced diabetes mellitus with diabetic peripheral angiopathy with gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.52');

-- Drug or chemical induced diabetes mellitus with diabetic peripheral angiopathy with gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.52');

-- Drug or chemical induced diabetes mellitus with diabetic peripheral angiopathy with gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.52');

-- Drug or chemical induced diabetes mellitus with other circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.59');

-- Drug or chemical induced diabetes mellitus with other specified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.6');

-- Drug or chemical induced diabetes mellitus with diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.61');

-- Drug or chemical induced diabetes mellitus with diabetic neuropathic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.610');

-- Drug or chemical induced diabetes mellitus with diabetic neuropathic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.610');

-- Drug or chemical induced diabetes mellitus with other diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.618');

-- Drug or chemical induced diabetes mellitus with other diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.618');

-- Drug or chemical induced diabetes mellitus with skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.62');

-- Drug or chemical induced diabetes mellitus with diabetic dermatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.620');

-- Drug or chemical induced diabetes mellitus with diabetic dermatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.620');

-- Drug or chemical induced diabetes mellitus with foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.621');

-- Drug or chemical induced diabetes mellitus with foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.621');

-- Drug or chemical induced diabetes mellitus with foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.621');

-- Drug or chemical induced diabetes mellitus with other skin ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.622');

-- Drug or chemical induced diabetes mellitus with other skin ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.622');

-- Drug or chemical induced diabetes mellitus with other skin ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.622');

-- Drug or chemical induced diabetes mellitus with other skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.628');

-- Drug or chemical induced diabetes mellitus with other skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.628');

-- Drug or chemical induced diabetes mellitus with other skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.628');

-- Drug or chemical induced diabetes mellitus with oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.63');

-- Drug or chemical induced diabetes mellitus with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.630');

-- Drug or chemical induced diabetes mellitus with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.630');

-- Drug or chemical induced diabetes mellitus with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.630');

-- Drug or chemical induced diabetes mellitus with other oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.638');

-- Drug or chemical induced diabetes mellitus with other oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.638');

-- Drug or chemical induced diabetes mellitus with hypoglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.64');

-- Drug or chemical induced diabetes mellitus with hypoglycemia with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.641');

-- Drug or chemical induced diabetes mellitus with hypoglycemia without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.649');

-- Drug or chemical induced diabetes mellitus with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.65');

-- Drug or chemical induced diabetes mellitus with other specified complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.69');

-- Drug or chemical induced diabetes mellitus with unspecified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.8');

-- Drug or chemical induced diabetes mellitus without complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E09.9');

-- Other specified diabetes mellitus (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13');

-- Other specified diabetes mellitus with hyperosmolarity (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.0');

-- Other specified diabetes mellitus with hyperosmolarity without nonketotic hyperglycemic-hyperosmolar coma (NKHHC) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.00');

-- Other specified diabetes mellitus with hyperosmolarity with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.01');

-- Other specified diabetes mellitus with ketoacidosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.1');

-- Other specified diabetes mellitus with ketoacidosis without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.10');

-- Other specified diabetes mellitus with ketoacidosis with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.11');

-- Other specified diabetes mellitus with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.2');

-- Other specified diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.21');

-- Other specified diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.22');

-- Other specified diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.29');

-- Other specified diabetes mellitus with ophthalmic complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.3');

-- Other specified diabetes mellitus with unspecified diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.31');

-- Other specified diabetes mellitus with unspecified diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.311');

-- Other specified diabetes mellitus with unspecified diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.319');

-- Other specified diabetes mellitus with mild nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.32');

-- Other specified diabetes mellitus with mild nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.321');

-- Other specified diabetes mellitus with mild nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.329');

-- Other specified diabetes mellitus with moderate nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.33');

-- Other specified diabetes mellitus with moderate nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.331');

-- Other specified diabetes mellitus with moderate nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.339');

-- Other specified diabetes mellitus with severe nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.34');

-- Other specified diabetes mellitus with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.341');

-- Other specified diabetes mellitus with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.349');

-- Other specified diabetes mellitus with proliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.35');

-- Other specified diabetes mellitus with proliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.351');

-- Other specified diabetes mellitus with proliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.359');

-- Other specified diabetes mellitus with diabetic cataract (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.36');

-- Other specified diabetes mellitus with other diabetic ophthalmic complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.39');

-- Other specified diabetes mellitus with neurological complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.4');

-- Other specified diabetes mellitus with diabetic neuropathy, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.40');

-- Other specified diabetes mellitus with diabetic mononeuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.41');

-- Other specified diabetes mellitus with diabetic polyneuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.42');

-- Other specified diabetes mellitus with diabetic autonomic (poly)neuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.43');

-- Other specified diabetes mellitus with diabetic amyotrophy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.44');

-- Other specified diabetes mellitus with other diabetic neurological complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.49');

-- Other specified diabetes mellitus with circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.5');

-- Other specified diabetes mellitus with diabetic peripheral angiopathy without gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.51');

-- Other specified diabetes mellitus with diabetic peripheral angiopathy with gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.52');

-- Other specified diabetes mellitus with diabetic peripheral angiopathy with gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.52');

-- Other specified diabetes mellitus with other circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.59');

-- Other specified diabetes mellitus with other specified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.6');

-- Other specified diabetes mellitus with diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.61');

-- Other specified diabetes mellitus with diabetic neuropathic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.610');

-- Other specified diabetes mellitus with other diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.618');

-- Other specified diabetes mellitus with other diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.618');

-- Other specified diabetes mellitus with skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.62');

-- Other specified diabetes mellitus with diabetic dermatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.620');

-- Other specified diabetes mellitus with foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.621');

-- Other specified diabetes mellitus with other skin ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.622');

-- Other specified diabetes mellitus with other skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.628');

-- Other specified diabetes mellitus with oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.63');

-- Other specified diabetes mellitus with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.630');

-- Other specified diabetes mellitus with other oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.638');

-- Other specified diabetes mellitus with hypoglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.64');

-- Other specified diabetes mellitus with hypoglycemia with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.641');

-- Other specified diabetes mellitus with hypoglycemia with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.641');

-- Other specified diabetes mellitus with hypoglycemia without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.649');

-- Other specified diabetes mellitus with hypoglycemia without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.649');

-- Other specified diabetes mellitus with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.65');

-- Other specified diabetes mellitus with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.65');

-- Other specified diabetes mellitus with other specified complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.69');

-- Other specified diabetes mellitus with unspecified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.8');

-- Other specified diabetes mellitus without complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E13.9');

-- Type 2 diabetes mellitus (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11');

-- Type 2 diabetes mellitus with hyperosmolarity (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.0');

-- Type 2 diabetes mellitus with hyperosmolarity without nonketotic hyperglycemic-hyperosmolar coma (NKHHC) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.00');

-- Type 2 diabetes mellitus with hyperosmolarity with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.01');

-- Type 2 diabetes mellitus with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.2');

-- Type 2 diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.21');

-- Type 2 diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.22');

-- Type 2 diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.29');

-- Type 2 diabetes mellitus with ophthalmic complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.3');

-- Type 2 diabetes mellitus with unspecified diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.31');

-- Type 2 diabetes mellitus with unspecified diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.311');

-- Type 2 diabetes mellitus with unspecified diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.319');

-- Type 2 diabetes mellitus with mild nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.32');

-- Type 2 diabetes mellitus with mild nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.321');

-- Type 2 diabetes mellitus with mild nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.329');

-- Type 2 diabetes mellitus with moderate nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.33');

-- Type 2 diabetes mellitus with moderate nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.331');

-- Type 2 diabetes mellitus with moderate nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.339');

-- Type 2 diabetes mellitus with severe nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.34');

-- Type 2 diabetes mellitus with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.341');

-- Type 2 diabetes mellitus with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.341');

-- Type 2 diabetes mellitus with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.349');

-- Type 2 diabetes mellitus with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.349');

-- Type 2 diabetes mellitus with proliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.35');

-- Type 2 diabetes mellitus with proliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.351');

-- Type 2 diabetes mellitus with proliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.359');

-- Type 2 diabetes mellitus with diabetic cataract (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.36');

-- Type 2 diabetes mellitus with other diabetic ophthalmic complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.39');

-- Type 2 diabetes mellitus with neurological complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.4');

-- Type 2 diabetes mellitus with diabetic neuropathy, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.40');

-- Type 2 diabetes mellitus with diabetic mononeuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.41');

-- Type 2 diabetes mellitus with diabetic polyneuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.42');

-- Type 2 diabetes mellitus with diabetic autonomic (poly)neuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.43');

-- Type 2 diabetes mellitus with diabetic amyotrophy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.44');

-- Type 2 diabetes mellitus with other diabetic neurological complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.49');

-- Type 2 diabetes mellitus with circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.5');

-- Type 2 diabetes mellitus with diabetic peripheral angiopathy without gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.51');

-- Type 2 diabetes mellitus with diabetic peripheral angiopathy with gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.52');

-- Type 2 diabetes mellitus with other circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.59');

-- Type 2 diabetes mellitus with other specified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.6');

-- Type 2 diabetes mellitus with diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.61');

-- Type 2 diabetes mellitus with diabetic neuropathic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.610');

-- Type 2 diabetes mellitus with other diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.618');

-- Type 2 diabetes mellitus with skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.62');

-- Type 2 diabetes mellitus with diabetic dermatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.620');

-- Type 2 diabetes mellitus with foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.621');

-- Type 2 diabetes mellitus with other skin ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.622');

-- Type 2 diabetes mellitus with other skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.628');

-- Type 2 diabetes mellitus with oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.63');

-- Type 2 diabetes mellitus with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.630');

-- Type 2 diabetes mellitus with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.630');

-- Type 2 diabetes mellitus with other oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.638');

-- Type 2 diabetes mellitus with hypoglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.64');

-- Type 2 diabetes mellitus with hypoglycemia with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.641');

-- Type 2 diabetes mellitus with hypoglycemia without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.649');

-- Type 2 diabetes mellitus with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.65');

-- Type 2 diabetes mellitus with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.65');

-- Type 2 diabetes mellitus with other specified complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.69');

-- Type 2 diabetes mellitus with unspecified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.8');

-- Type 2 diabetes mellitus without complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E11.9');

-- Diabetes mellitus without mention of complication, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.00');

-- Diabetes mellitus without mention of complication, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.02');

-- Diabetes mellitus without mention of complication, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.02');

-- Diabetes with ketoacidosis, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.10');

-- Diabetes with ketoacidosis, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.12');

-- Diabetes with hyperosmolarity, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.20');

-- Diabetes with hyperosmolarity, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.22');

-- Diabetes with other coma, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.30');

-- Diabetes with other coma, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.32');

-- Diabetes with renal manifestations, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.40');

-- Diabetes with renal manifestations, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.42');

-- Diabetes with ophthalmic manifestations, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.50');

-- Diabetes with ophthalmic manifestations, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.52');

-- Diabetes with neurological manifestations, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.60');

-- Diabetes with neurological manifestations, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.62');

-- Diabetes with peripheral circulatory disorders, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.70');

-- Diabetes with peripheral circulatory disorders, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.72');

-- Diabetes with other specified manifestations, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.80');

-- Diabetes with other specified manifestations, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.82');

-- Diabetes with unspecified complication, type II or unspecified type, not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.90');

-- Diabetes with unspecified complication, type II or unspecified type, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;diabetes_mellitus;type_2;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.92');

-- depressive episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[3-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.75');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.76');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F32%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F33%');

-- depressive episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.2%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.3%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.5%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.82');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '311');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.0');

-- CPT4 depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107249);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40756912);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106304);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106305);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106326);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2101900);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40757095);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2108573);

-- HCPCS depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617571);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617572);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617831);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617866);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2617910);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533277);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533193);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533366);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(43533229);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786417);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786418);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;depressive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44786437);

-- manic episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[0-2]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.71');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.72');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.73');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.74');

-- manic episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.0%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.4%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;manic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.81');

-- mixed episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.6%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.77');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.78');

-- mixed episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;mixed;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.6%');

-- unknown polarity episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.70');

-- unknown polarity episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.80');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;polarity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.89');

-- no psychotic features including remission (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.3%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.6[123]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.7%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.42');

-- no psychotic features  including remission (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][1236]');

-- CPT4 Major depressive disorder, severe without psychotic features (MDD) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;absent;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

-- psychotic features including remission (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.64');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F23%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F24%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F28%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F29%');

-- psychotic features  including remission and including nonorganic psychoses (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '298.[013489]');

-- CPT4 psychotic depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;present;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

-- unknown psychotic features including remission (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.6');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.60');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.40');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.41');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F33.[8-9]');

-- unknown psychotic features including remission (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][05] 296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;psychotic;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.9%');

-- mild episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.11');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.11');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.61');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.7%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F33.4%');

-- mild episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][156]');

-- CPT4 mild depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;mild;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106304);

-- moderate episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.12');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.12');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.62');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.1');

-- moderate episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6]2');

-- CPT4 moderate depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106305);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;moderate;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2107245);

-- severe episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.13');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.13');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.63');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.64');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F32.[2-3]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F33.[2-3]');

-- severe episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[0-6][3-4]');

-- CPT4 severe depression (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106310);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;severe;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(2106322);

-- unknown severity episode (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.10');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[89]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.10');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.30');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.60');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F33.9');

-- unknown severity episode (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[01234567]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;episode;severity;unknown;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

-- Abnormal glucose (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R73.0');

-- Hyperglycemia, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R73.9');

-- Abnormal glucose (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '790.2');

-- Impaired fasting glucose (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;fasting;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R73.01');

-- Impaired fasting glucose (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;fasting;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '790.21');

-- Elevated blood glucose level (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;hyperglycemia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R73');

-- Other abnormal glucose (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R73.09');

-- Other abnormal glucose (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '790.29');

-- Impaired glucose tolerance (oral) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;tolerance;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R73.02');

-- Impaired glucose tolerance test (oral) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;abnormal_glucose;tolerance;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '790.22');

-- Agenesis, aplasia and hypoplasia of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q45.0');

-- Annular pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q45.1');

-- Congenital pancreatic cyst (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q45.2');

-- Other congenital malformations of pancreas and pancreatic duct (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Q45.3');

-- Benign neoplasm of islets of Langerhans (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;congenital_pancreas_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '211.7');

-- Poisoning by, adverse effect of and underdosing of insulin and oral hypoglycemic [antidiabetic] drugs (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3');

-- Poisoning by, adverse effect of and underdosing of insulin and oral hypoglycemic [antidiabetic] drugs (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, accidental (unintentional) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X1');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, accidental (unintentional), initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X1A');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, accidental (unintentional), subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X1D');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, accidental (unintentional), sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X1S');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, intentional self-harm (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X2');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, intentional self-harm, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X2A');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, intentional self-harm, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X2D');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, intentional self-harm, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X2S');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, assault (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X3');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, assault, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X3A');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, assault, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X3D');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, assault, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X3S');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, undetermined (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X4');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, undetermined, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X4A');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, undetermined, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X4D');

-- Poisoning by insulin and oral hypoglycemic [antidiabetic] drugs, undetermined, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X4S');

-- Adverse effect of insulin and oral hypoglycemic [antidiabetic] drugs (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X5');

-- Adverse effect of insulin and oral hypoglycemic [antidiabetic] drugs, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X5A');

-- Adverse effect of insulin and oral hypoglycemic [antidiabetic] drugs, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X5D');

-- Adverse effect of insulin and oral hypoglycemic [antidiabetic] drugs, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X5S');

-- Underdosing of insulin and oral hypoglycemic [antidiabetic] drugs (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X6');

-- Underdosing of insulin and oral hypoglycemic [antidiabetic] drugs, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X6A');

-- Underdosing of insulin and oral hypoglycemic [antidiabetic] drugs, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X6D');

-- Underdosing of insulin and oral hypoglycemic [antidiabetic] drugs, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T38.3X6S');

-- Breakdown (mechanical) of insulin pump (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.614');

-- Breakdown (mechanical) of insulin pump, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.614A');

-- Breakdown (mechanical) of insulin pump, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.614D');

-- Breakdown (mechanical) of insulin pump, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.614S');

-- Displacement of insulin pump (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.624');

-- Displacement of insulin pump, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.624A');

-- Displacement of insulin pump, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.624D');

-- Displacement of insulin pump, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.624S');

-- Leakage of insulin pump (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.633');

-- Leakage of insulin pump, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.633A');

-- Leakage of insulin pump, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.633D');

-- Leakage of insulin pump, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.633S');

-- Other mechanical complication of insulin pump (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.694');

-- Other mechanical complication of insulin pump, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.694A');

-- Other mechanical complication of insulin pump, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.694D');

-- Other mechanical complication of insulin pump, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.694S');

-- Infection and inflammatory reaction due to insulin pump (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.72');

-- Infection and inflammatory reaction due to insulin pump (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.72');

-- Infection and inflammatory reaction due to insulin pump, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.72XA');

-- Infection and inflammatory reaction due to insulin pump, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.72XA');

-- Infection and inflammatory reaction due to insulin pump, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.72XD');

-- Infection and inflammatory reaction due to insulin pump, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.72XD');

-- Infection and inflammatory reaction due to insulin pump, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'T85.72XS');

-- Encounter for fitting and adjustment of insulin pump (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z46.81');

-- Long term (current) use of insulin (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z79.4');

-- Presence of insulin pump (external) (internal) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z96.41');

-- Poisoning by insulins and antidiabetic agents (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '962.3');

-- Insulins and antidiabetic agents causing adverse effects in therapeutic use (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE 'E932.3%');

-- Insulin pump status (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V45.85');

-- Fitting and adjustment of insulin pump (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V53.91');

-- Long-term (current) use of insulin (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V58.67');

-- Unspecified adverse effect of insulin (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '995.23');

-- Mechanical complication due to insulin pump (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '996.57');

-- Encounter for insulin pump training (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;hypoglycemic;insulin;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V65.46');

-- Abnormal glucose complicating pregnancy, childbirth and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O99.81');

-- Abnormal glucose complicating pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O99.810');

-- Abnormal glucose complicating childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O99.814');

-- Abnormal glucose complicating the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O99.815');

-- Diabetes mellitus in pregnancy, childbirth, and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24');

-- Pre-existing diabetes mellitus, type 1, in pregnancy, childbirth and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.0');

-- Pre-existing diabetes mellitus, type 1, in pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.01');

-- Pre-existing diabetes mellitus, type 1, in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.011');

-- Pre-existing diabetes mellitus, type 1, in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.011');

-- Pre-existing diabetes mellitus, type 1, in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.012');

-- Pre-existing diabetes mellitus, type 1, in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.012');

-- Pre-existing diabetes mellitus, type 1, in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.013');

-- Pre-existing diabetes mellitus, type 1, in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.013');

-- Pre-existing diabetes mellitus, type 1, in pregnancy, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.019');

-- Pre-existing diabetes mellitus, type 1, in childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.02');

-- Pre-existing diabetes mellitus, type 1, in the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.03');

-- Pre-existing diabetes mellitus, type 2, in pregnancy, childbirth and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.1');

-- Pre-existing diabetes mellitus, type 2, in pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.11');

-- Pre-existing diabetes mellitus, type 2, in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.111');

-- Pre-existing diabetes mellitus, type 2, in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.111');

-- Pre-existing diabetes mellitus, type 2, in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.112');

-- Pre-existing diabetes mellitus, type 2, in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.112');

-- Pre-existing diabetes mellitus, type 2, in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.113');

-- Pre-existing diabetes mellitus, type 2, in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.113');

-- Pre-existing diabetes mellitus, type 2, in pregnancy, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.119');

-- Pre-existing diabetes mellitus, type 2, in childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.12');

-- Pre-existing diabetes mellitus, type 2, in the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.13');

-- Unspecified pre-existing diabetes mellitus in pregnancy, childbirth and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.3');

-- Unspecified pre-existing diabetes mellitus in pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.31');

-- Unspecified pre-existing diabetes mellitus in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.311');

-- Unspecified pre-existing diabetes mellitus in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.311');

-- Unspecified pre-existing diabetes mellitus in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.312');

-- Unspecified pre-existing diabetes mellitus in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.312');

-- Unspecified pre-existing diabetes mellitus in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.313');

-- Unspecified pre-existing diabetes mellitus in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.313');

-- Unspecified pre-existing diabetes mellitus in pregnancy, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.319');

-- Unspecified pre-existing diabetes mellitus in childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.32');

-- Unspecified pre-existing diabetes mellitus in the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.33');

-- Gestational diabetes mellitus (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.4');

-- Gestational diabetes mellitus in pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.41');

-- Gestational diabetes mellitus in pregnancy, diet controlled (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.410');

-- Gestational diabetes mellitus in pregnancy, insulin controlled (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.414');

-- Gestational diabetes mellitus in pregnancy, unspecified control (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.419');

-- Gestational diabetes mellitus in childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.42');

-- Gestational diabetes mellitus in childbirth, diet controlled (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.420');

-- Gestational diabetes mellitus in childbirth, insulin controlled (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.424');

-- Gestational diabetes mellitus in childbirth, unspecified control (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.429');

-- Gestational diabetes mellitus in the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.43');

-- Gestational diabetes mellitus in the puerperium, diet controlled (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.430');

-- Gestational diabetes mellitus in the puerperium, insulin controlled (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.434');

-- Gestational diabetes mellitus in the puerperium, unspecified control (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.439');

-- Other pre-existing diabetes mellitus in pregnancy, childbirth, and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.8');

-- Other pre-existing diabetes mellitus in pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.81');

-- Other pre-existing diabetes mellitus in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.811');

-- Other pre-existing diabetes mellitus in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.811');

-- Other pre-existing diabetes mellitus in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.812');

-- Other pre-existing diabetes mellitus in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.812');

-- Other pre-existing diabetes mellitus in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.813');

-- Other pre-existing diabetes mellitus in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.813');

-- Other pre-existing diabetes mellitus in pregnancy, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.819');

-- Other pre-existing diabetes mellitus in childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.82');

-- Other pre-existing diabetes mellitus in the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.83');

-- Unspecified diabetes mellitus in pregnancy, childbirth and the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.9');

-- Unspecified diabetes mellitus in pregnancy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.91');

-- Unspecified diabetes mellitus in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.911');

-- Unspecified diabetes mellitus in pregnancy, first trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.911');

-- Unspecified diabetes mellitus in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.912');

-- Unspecified diabetes mellitus in pregnancy, second trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.912');

-- Unspecified diabetes mellitus in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.913');

-- Unspecified diabetes mellitus in pregnancy, third trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.913');

-- Unspecified diabetes mellitus in pregnancy, unspecified trimester (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.919');

-- Unspecified diabetes mellitus in childbirth (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.92');

-- Unspecified diabetes mellitus in the puerperium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O24.93');

-- Abnormal glucose tolerance of mother, complicating pregnancy, childbirth, or the puerperium (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.8');

-- Abnormal glucose tolerance of mother, unspecified as to episode of care or not applicable (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.80');

-- Abnormal glucose tolerance of mother, delivered, with or without mention of antepartum condition (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.81');

-- Abnormal glucose tolerance of mother, delivered, with mention of postpartum complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.82');

-- Abnormal glucose tolerance of mother, antepartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.83');

-- Abnormal glucose tolerance of mother, postpartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.84');

-- Diabetes mellitus complicating pregnancy, childbirth, or the puerperium (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.0');

-- Diabetes mellitus of mother, complicating pregnancy, childbirth, or the puerperium, unspecified as to episode of care or not applicable (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.00');

-- Diabetes mellitus of mother, complicating pregnancy, childbirth, or the puerperium, delivered, with or without mention of antepartum condition (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.01');

-- Diabetes mellitus of mother, complicating pregnancy, childbirth, or the puerperium, delivered, with mention of postpartum complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.02');

-- Diabetes mellitus of mother, complicating pregnancy, childbirth, or the puerperium, antepartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.03');

-- Diabetes mellitus of mother, complicating pregnancy, childbirth, or the puerperium, postpartum condition or complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;obstetric;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '648.04');

-- Acquired absence of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_absence;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z90.41');

-- Acquired total absence of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_absence;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z90.410');

-- Acquired partial absence of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_absence;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z90.411');

-- Acquired absence of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_absence;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V88.1');

-- Acquired total absence of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_absence;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V88.11');

-- Acquired partial absence of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_absence;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V88.12');

-- Injury of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.2');

-- Unspecified injury of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.20');

-- Unspecified injury of head of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.200');

-- Unspecified injury of head of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.200A');

-- Unspecified injury of head of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.200D');

-- Unspecified injury of head of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.200S');

-- Unspecified injury of body of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.201');

-- Unspecified injury of body of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.201A');

-- Unspecified injury of body of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.201D');

-- Unspecified injury of body of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.201S');

-- Unspecified injury of tail of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.202');

-- Unspecified injury of tail of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.202A');

-- Unspecified injury of tail of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.202D');

-- Unspecified injury of tail of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.202S');

-- Unspecified injury of unspecified part of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.209');

-- Unspecified injury of unspecified part of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.209A');

-- Unspecified injury of unspecified part of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.209D');

-- Unspecified injury of unspecified part of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.209S');

-- Contusion of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.22');

-- Contusion of head of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.220');

-- Contusion of head of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.220A');

-- Contusion of head of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.220D');

-- Contusion of head of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.220S');

-- Contusion of body of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.221');

-- Contusion of body of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.221A');

-- Contusion of body of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.221D');

-- Contusion of body of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.221S');

-- Contusion of tail of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.222');

-- Contusion of tail of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.222A');

-- Contusion of tail of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.222D');

-- Contusion of tail of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.222S');

-- Contusion of unspecified part of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.229');

-- Contusion of unspecified part of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.229A');

-- Contusion of unspecified part of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.229D');

-- Contusion of unspecified part of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.229S');

-- Laceration of pancreas, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.23');

-- Laceration of head of pancreas, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.230');

-- Laceration of head of pancreas, unspecified degree, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.230A');

-- Laceration of head of pancreas, unspecified degree, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.230D');

-- Laceration of head of pancreas, unspecified degree, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.230S');

-- Laceration of body of pancreas, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.231');

-- Laceration of body of pancreas, unspecified degree, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.231A');

-- Laceration of body of pancreas, unspecified degree, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.231D');

-- Laceration of body of pancreas, unspecified degree, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.231S');

-- Laceration of tail of pancreas, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.232');

-- Laceration of tail of pancreas, unspecified degree, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.232A');

-- Laceration of tail of pancreas, unspecified degree, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.232D');

-- Laceration of tail of pancreas, unspecified degree, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.232S');

-- Laceration of unspecified part of pancreas, unspecified degree (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.239');

-- Laceration of unspecified part of pancreas, unspecified degree, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.239A');

-- Laceration of unspecified part of pancreas, unspecified degree, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.239D');

-- Laceration of unspecified part of pancreas, unspecified degree, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.239S');

-- Minor laceration of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.24');

-- Minor laceration of head of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.240');

-- Minor laceration of head of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.240A');

-- Minor laceration of head of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.240D');

-- Minor laceration of head of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.240S');

-- Minor laceration of body of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.241');

-- Minor laceration of body of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.241A');

-- Minor laceration of body of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.241D');

-- Minor laceration of body of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.241S');

-- Minor laceration of tail of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.242');

-- Minor laceration of tail of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.242A');

-- Minor laceration of tail of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.242D');

-- Minor laceration of tail of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.242S');

-- Minor laceration of unspecified part of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.249');

-- Minor laceration of unspecified part of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.249A');

-- Minor laceration of unspecified part of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.249D');

-- Minor laceration of unspecified part of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.249S');

-- Moderate laceration of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.25');

-- Moderate laceration of head of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.250');

-- Moderate laceration of head of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.250A');

-- Moderate laceration of head of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.250D');

-- Moderate laceration of head of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.250S');

-- Moderate laceration of body of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.251');

-- Moderate laceration of body of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.251A');

-- Moderate laceration of body of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.251D');

-- Moderate laceration of body of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.251S');

-- Moderate laceration of tail of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.252');

-- Moderate laceration of tail of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.252A');

-- Moderate laceration of tail of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.252D');

-- Moderate laceration of tail of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.252S');

-- Moderate laceration of unspecified part of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.259');

-- Moderate laceration of unspecified part of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.259A');

-- Moderate laceration of unspecified part of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.259D');

-- Moderate laceration of unspecified part of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.259S');

-- Major laceration of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.26');

-- Major laceration of head of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.260');

-- Major laceration of head of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.260A');

-- Major laceration of head of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.260D');

-- Major laceration of head of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.260S');

-- Major laceration of body of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.261');

-- Major laceration of body of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.261A');

-- Major laceration of body of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.261D');

-- Major laceration of body of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.261S');

-- Major laceration of tail of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.262');

-- Major laceration of tail of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.262A');

-- Major laceration of tail of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.262D');

-- Major laceration of tail of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.262S');

-- Major laceration of unspecified part of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.269');

-- Major laceration of unspecified part of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.269A');

-- Major laceration of unspecified part of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.269D');

-- Major laceration of unspecified part of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.269S');

-- Other injury of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.29');

-- Other injury of head of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.290');

-- Other injury of head of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.290A');

-- Other injury of head of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.290D');

-- Other injury of head of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.290S');

-- Other injury of body of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.291');

-- Other injury of body of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.291A');

-- Other injury of body of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.291D');

-- Other injury of body of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.291S');

-- Other injury of tail of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.292');

-- Other injury of tail of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.292A');

-- Other injury of tail of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.292D');

-- Other injury of tail of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.292S');

-- Other injury of unspecified part of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.299');

-- Other injury of unspecified part of pancreas, initial encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.299A');

-- Other injury of unspecified part of pancreas, subsequent encounter (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.299D');

-- Other injury of unspecified part of pancreas, sequela (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'S36.299S');

-- Anomalies of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '751.7');

-- Injury to pancreas, head, without mention of open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '863.81');

-- Injury to pancreas, body, without mention of open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '863.82');

-- Injury to pancreas, tail, without mention of open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '863.83');

-- Injury to pancreas, multiple and unspecified sites, without mention of open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '863.84');

-- Injury to pancreas, head, with open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '863.91');

-- Injury to pancreas, body, with open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '863.92');

-- Injury to pancreas, tail, with open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '863.93');

-- Injury to pancreas, multiple and unspecified sites, with open wound into cavity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '863.94');

-- Malignant neoplasm of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25');

-- Malignant neoplasm of head of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25.0');

-- Malignant neoplasm of body of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25.1');

-- Malignant neoplasm of tail of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25.2');

-- Malignant neoplasm of pancreatic duct (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25.3');

-- Malignant neoplasm of endocrine pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25.4');

-- Malignant neoplasm of other parts of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25.7');

-- Malignant neoplasm of overlapping sites of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25.8');

-- Malignant neoplasm of pancreas, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'C25.9');

-- Benign neoplasm of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D13.6');

-- Benign neoplasm of endocrine pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'D13.7');

-- Personal history of malignant neoplasm of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z85.07');

-- Malignant neoplasm of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '157');

-- Malignant neoplasm of head of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '157.0');

-- Malignant neoplasm of body of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '157.1');

-- Malignant neoplasm of tail of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '157.2');

-- Malignant neoplasm of pancreatic duct (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '157.3');

-- Malignant neoplasm of islets of langerhans (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '157.4');

-- Malignant neoplasm of other specified sites of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '157.8');

-- Malignant neoplasm of pancreas, part unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '157.9');

-- Benign neoplasm of pancreas, except islets of Langerhans (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_neoplasm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '211.6');

-- Cyst of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K86.2');

-- Pseudocyst of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K86.3');

-- Cyst and pseudocyst of pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_structural_anomaly;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '577.2');

-- Pancreas transplant status (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z94.83');

-- Complications of transplanted pancreas (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '996.86');

-- Pancreas replaced by transplant (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreas_transplant;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = 'V42.83');

-- Other disorders of pancreatic internal secretion (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E16');

-- Other specified disorders of pancreatic internal secretion (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E16.8');

-- Disorder of pancreatic internal secretion, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E16.9');

-- Pancreatic steatorrhea (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K90.3');

-- Pancreatic steatorrhea (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatic_secretory_dysfunction;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '579.4');

-- Cytomegaloviral pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'B25.2');

-- Mumps pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'B26.3');

-- Acute pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K85');

-- Idiopathic acute pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K85.0');

-- Biliary acute pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K85.1');

-- Alcohol induced acute pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K85.2');

-- Drug induced acute pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K85.3');

-- Other acute pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K85.8');

-- Acute pancreatitis, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K85.9');

-- Other diseases of pancreas (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K86');

-- Alcohol-induced chronic pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K86.0');

-- Other chronic pancreatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'K86.1');

-- Acute pancreatitis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '577');

-- Chronic pancreatitis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;pancreatitis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '577.1');

-- Diabetes mellitus due to underlying condition (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08');

-- Diabetes mellitus due to underlying condition with hyperosmolarity (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.0');

-- Diabetes mellitus due to underlying condition with hyperosmolarity without nonketotic hyperglycemic-hyperosmolar coma (NKHHC) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.00');

-- Diabetes mellitus due to underlying condition with hyperosmolarity with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.01');

-- Diabetes mellitus due to underlying condition with ketoacidosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.1');

-- Diabetes mellitus due to underlying condition with ketoacidosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.1');

-- Diabetes mellitus due to underlying condition with ketoacidosis without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.10');

-- Diabetes mellitus due to underlying condition with ketoacidosis with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.11');

-- Diabetes mellitus due to underlying condition with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.2');

-- Diabetes mellitus due to underlying condition with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.21');

-- Diabetes mellitus due to underlying condition with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.21');

-- Diabetes mellitus due to underlying condition with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.22');

-- Diabetes mellitus due to underlying condition with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.22');

-- Diabetes mellitus due to underlying condition with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.29');

-- Diabetes mellitus due to underlying condition with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.29');

-- Diabetes mellitus due to underlying condition with ophthalmic complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.3');

-- Diabetes mellitus due to underlying condition with unspecified diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.31');

-- Diabetes mellitus due to underlying condition with unspecified diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.311');

-- Diabetes mellitus due to underlying condition with unspecified diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.311');

-- Diabetes mellitus due to underlying condition with unspecified diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.319');

-- Diabetes mellitus due to underlying condition with unspecified diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.319');

-- Diabetes mellitus due to underlying condition with mild nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.32');

-- Diabetes mellitus due to underlying condition with mild nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.321');

-- Diabetes mellitus due to underlying condition with mild nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.321');

-- Diabetes mellitus due to underlying condition with mild nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.329');

-- Diabetes mellitus due to underlying condition with mild nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.329');

-- Diabetes mellitus due to underlying condition with moderate nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.33');

-- Diabetes mellitus due to underlying condition with moderate nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.331');

-- Diabetes mellitus due to underlying condition with moderate nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.331');

-- Diabetes mellitus due to underlying condition with moderate nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.339');

-- Diabetes mellitus due to underlying condition with moderate nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.339');

-- Diabetes mellitus due to underlying condition with severe nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.34');

-- Diabetes mellitus due to underlying condition with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.341');

-- Diabetes mellitus due to underlying condition with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.341');

-- Diabetes mellitus due to underlying condition with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.349');

-- Diabetes mellitus due to underlying condition with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.349');

-- Diabetes mellitus due to underlying condition with proliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.35');

-- Diabetes mellitus due to underlying condition with proliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.351');

-- Diabetes mellitus due to underlying condition with proliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.351');

-- Diabetes mellitus due to underlying condition with proliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.359');

-- Diabetes mellitus due to underlying condition with proliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.359');

-- Diabetes mellitus due to underlying condition with diabetic cataract (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.36');

-- Diabetes mellitus due to underlying condition with diabetic cataract (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.36');

-- Diabetes mellitus due to underlying condition with other diabetic ophthalmic complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.39');

-- Diabetes mellitus due to underlying condition with other diabetic ophthalmic complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.39');

-- Diabetes mellitus due to underlying condition with neurological complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.4');

-- Diabetes mellitus due to underlying condition with diabetic neuropathy, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.40');

-- Diabetes mellitus due to underlying condition with diabetic neuropathy, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.40');

-- Diabetes mellitus due to underlying condition with diabetic mononeuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.41');

-- Diabetes mellitus due to underlying condition with diabetic mononeuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.41');

-- Diabetes mellitus due to underlying condition with diabetic polyneuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.42');

-- Diabetes mellitus due to underlying condition with diabetic polyneuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.42');

-- Diabetes mellitus due to underlying condition with diabetic autonomic (poly)neuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.43');

-- Diabetes mellitus due to underlying condition with diabetic autonomic (poly)neuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.43');

-- Diabetes mellitus due to underlying condition with diabetic amyotrophy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.44');

-- Diabetes mellitus due to underlying condition with diabetic amyotrophy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.44');

-- Diabetes mellitus due to underlying condition with other diabetic neurological complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.49');

-- Diabetes mellitus due to underlying condition with other diabetic neurological complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.49');

-- Diabetes mellitus due to underlying condition with circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.5');

-- Diabetes mellitus due to underlying condition with diabetic peripheral angiopathy without gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.51');

-- Diabetes mellitus due to underlying condition with diabetic peripheral angiopathy with gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.52');

-- Diabetes mellitus due to underlying condition with other circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.59');

-- Diabetes mellitus due to underlying condition with other specified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.6');

-- Diabetes mellitus due to underlying condition with diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.61');

-- Diabetes mellitus due to underlying condition with diabetic neuropathic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.610');

-- Diabetes mellitus due to underlying condition with diabetic neuropathic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.610');

-- Diabetes mellitus due to underlying condition with other diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.618');

-- Diabetes mellitus due to underlying condition with other diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.618');

-- Diabetes mellitus due to underlying condition with skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.62');

-- Diabetes mellitus due to underlying condition with diabetic dermatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.620');

-- Diabetes mellitus due to underlying condition with diabetic dermatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.620');

-- Diabetes mellitus due to underlying condition with foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.621');

-- Diabetes mellitus due to underlying condition with foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.621');

-- Diabetes mellitus due to underlying condition with other skin ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.622');

-- Diabetes mellitus due to underlying condition with other skin ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.622');

-- Diabetes mellitus due to underlying condition with other skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.628');

-- Diabetes mellitus due to underlying condition with other skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.628');

-- Diabetes mellitus due to underlying condition with oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.63');

-- Diabetes mellitus due to underlying condition with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.630');

-- Diabetes mellitus due to underlying condition with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.630');

-- Diabetes mellitus due to underlying condition with other oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.638');

-- Diabetes mellitus due to underlying condition with other oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.638');

-- Diabetes mellitus due to underlying condition with hypoglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.64');

-- Diabetes mellitus due to underlying condition with hypoglycemia with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.641');

-- Diabetes mellitus due to underlying condition with hypoglycemia with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.641');

-- Diabetes mellitus due to underlying condition with hypoglycemia without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.649');

-- Diabetes mellitus due to underlying condition with hypoglycemia without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.649');

-- Diabetes mellitus due to underlying condition with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.65');

-- Diabetes mellitus due to underlying condition with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.65');

-- Diabetes mellitus due to underlying condition with other specified complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.69');

-- Diabetes mellitus due to underlying condition with other specified complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.69');

-- Diabetes mellitus due to underlying condition with unspecified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.8');

-- Diabetes mellitus due to underlying condition with unspecified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.8');

-- Diabetes mellitus due to underlying condition without complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.9');

-- Diabetes mellitus due to underlying condition without complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E08.9');

-- Secondary diabetes mellitus (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249');

-- Secondary diabetes mellitus, without mention of complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.0');

-- Secondary diabetes mellitus without mention of complication, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.00');

-- Secondary diabetes mellitus without mention of complication, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.01');

-- Secondary diabetes mellitus with ketoacidosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.1');

-- Secondary diabetes mellitus with ketoacidosis (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.1');

-- Secondary diabetes mellitus with ketoacidosis, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.10');

-- Secondary diabetes mellitus with ketoacidosis, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.10');

-- Secondary diabetes mellitus with ketoacidosis, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.11');

-- Secondary diabetes mellitus with ketoacidosis, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.11');

-- Secondary diabetes mellitus with hyperosmolarity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.2');

-- Secondary diabetes mellitus with hyperosmolarity (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.2');

-- Secondary diabetes mellitus with hyperosmolarity, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.20');

-- Secondary diabetes mellitus with hyperosmolarity, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.20');

-- Secondary diabetes mellitus with hyperosmolarity, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.21');

-- Secondary diabetes mellitus with hyperosmolarity, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.21');

-- Secondary diabetes mellitus with other coma (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.3');

-- Secondary diabetes mellitus with other coma (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.3');

-- Secondary diabetes mellitus with other coma, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.30');

-- Secondary diabetes mellitus with other coma, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.30');

-- Secondary diabetes mellitus with other coma, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.31');

-- Secondary diabetes mellitus with other coma, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.31');

-- Secondary diabetes mellitus with renal manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.4');

-- Secondary diabetes mellitus with renal manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.4');

-- Secondary diabetes mellitus with renal manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.40');

-- Secondary diabetes mellitus with renal manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.40');

-- Secondary diabetes mellitus with renal manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.41');

-- Secondary diabetes mellitus with renal manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.41');

-- Secondary diabetes mellitus with ophthalmic manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.5');

-- Secondary diabetes mellitus with ophthalmic manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.5');

-- Secondary diabetes mellitus with ophthalmic manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.50');

-- Secondary diabetes mellitus with ophthalmic manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.50');

-- Secondary diabetes mellitus with ophthalmic manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.51');

-- Secondary diabetes mellitus with ophthalmic manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.51');

-- Secondary diabetes mellitus with neurological manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.6');

-- Secondary diabetes mellitus with neurological manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.6');

-- Secondary diabetes mellitus with neurological manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.60');

-- Secondary diabetes mellitus with neurological manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.60');

-- Secondary diabetes mellitus with neurological manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.61');

-- Secondary diabetes mellitus with neurological manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.61');

-- Secondary diabetes mellitus with peripheral circulatory disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.7');

-- Secondary diabetes mellitus with peripheral circulatory disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.7');

-- Secondary diabetes mellitus with peripheral circulatory disorders, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.70');

-- Secondary diabetes mellitus with peripheral circulatory disorders, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.70');

-- Secondary diabetes mellitus with peripheral circulatory disorders, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.71');

-- Secondary diabetes mellitus with peripheral circulatory disorders, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.71');

-- Secondary diabetes mellitus with other specified manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.8');

-- Secondary diabetes mellitus with other specified manifestations (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.8');

-- Secondary diabetes mellitus with other specified manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.80');

-- Secondary diabetes mellitus with other specified manifestations, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.80');

-- Secondary diabetes mellitus with other specified manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.81');

-- Secondary diabetes mellitus with other specified manifestations, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.81');

-- Secondary diabetes mellitus with unspecified complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.9');

-- Secondary diabetes mellitus with unspecified complication (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.9');

-- Secondary diabetes mellitus with unspecified complication, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.90');

-- Secondary diabetes mellitus with unspecified complication, not stated as uncontrolled, or unspecified (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.90');

-- Secondary diabetes mellitus with unspecified complication, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.91');

-- Secondary diabetes mellitus with unspecified complication, uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;secondary_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '249.91');

-- Type 1 diabetes mellitus (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10');

-- Type 1 diabetes mellitus with ketoacidosis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.1');

-- Type 1 diabetes mellitus with ketoacidosis without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.10');

-- Type 1 diabetes mellitus with ketoacidosis without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.10');

-- Type 1 diabetes mellitus with ketoacidosis with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.11');

-- Type 1 diabetes mellitus with kidney complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.2');

-- Type 1 diabetes mellitus with diabetic nephropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.21');

-- Type 1 diabetes mellitus with diabetic chronic kidney disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.22');

-- Type 1 diabetes mellitus with other diabetic kidney complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.29');

-- Type 1 diabetes mellitus with ophthalmic complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.3');

-- Type 1 diabetes mellitus with unspecified diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.31');

-- Type 1 diabetes mellitus with unspecified diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.311');

-- Type 1 diabetes mellitus with unspecified diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.319');

-- Type 1 diabetes mellitus with mild nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.32');

-- Type 1 diabetes mellitus with mild nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.321');

-- Type 1 diabetes mellitus with mild nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.329');

-- Type 1 diabetes mellitus with moderate nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.33');

-- Type 1 diabetes mellitus with moderate nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.331');

-- Type 1 diabetes mellitus with moderate nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.339');

-- Type 1 diabetes mellitus with severe nonproliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.34');

-- Type 1 diabetes mellitus with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.341');

-- Type 1 diabetes mellitus with severe nonproliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.341');

-- Type 1 diabetes mellitus with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.349');

-- Type 1 diabetes mellitus with severe nonproliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.349');

-- Type 1 diabetes mellitus with proliferative diabetic retinopathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.35');

-- Type 1 diabetes mellitus with proliferative diabetic retinopathy with macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.351');

-- Type 1 diabetes mellitus with proliferative diabetic retinopathy without macular edema (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.359');

-- Type 1 diabetes mellitus with diabetic cataract (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.36');

-- Type 1 diabetes mellitus with other diabetic ophthalmic complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.39');

-- Type 1 diabetes mellitus with neurological complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.4');

-- Type 1 diabetes mellitus with diabetic neuropathy, unspecified (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.40');

-- Type 1 diabetes mellitus with diabetic mononeuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.41');

-- Type 1 diabetes mellitus with diabetic polyneuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.42');

-- Type 1 diabetes mellitus with diabetic autonomic (poly)neuropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.43');

-- Type 1 diabetes mellitus with diabetic amyotrophy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.44');

-- Type 1 diabetes mellitus with other diabetic neurological complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.49');

-- Type 1 diabetes mellitus with circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.5');

-- Type 1 diabetes mellitus with diabetic peripheral angiopathy without gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.51');

-- Type 1 diabetes mellitus with diabetic peripheral angiopathy with gangrene (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.52');

-- Type 1 diabetes mellitus with other circulatory complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.59');

-- Type 1 diabetes mellitus with other specified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.6');

-- Type 1 diabetes mellitus with diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.61');

-- Type 1 diabetes mellitus with diabetic neuropathic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.610');

-- Type 1 diabetes mellitus with other diabetic arthropathy (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.618');

-- Type 1 diabetes mellitus with skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.62');

-- Type 1 diabetes mellitus with diabetic dermatitis (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.620');

-- Type 1 diabetes mellitus with foot ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.621');

-- Type 1 diabetes mellitus with other skin ulcer (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.622');

-- Type 1 diabetes mellitus with other skin complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.628');

-- Type 1 diabetes mellitus with oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.63');

-- Type 1 diabetes mellitus with periodontal disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.630');

-- Type 1 diabetes mellitus with other oral complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.638');

-- Type 1 diabetes mellitus with hypoglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.64');

-- Type 1 diabetes mellitus with hypoglycemia with coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.641');

-- Type 1 diabetes mellitus with hypoglycemia without coma (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.649');

-- Type 1 diabetes mellitus with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.65');

-- Type 1 diabetes mellitus with hyperglycemia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.65');

-- Type 1 diabetes mellitus with other specified complication (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.69');

-- Type 1 diabetes mellitus with unspecified complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.8');

-- Type 1 diabetes mellitus without complications (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E10.9');

-- Diabetes mellitus without mention of complication, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.01');

-- Diabetes mellitus without mention of complication, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.03');

-- Diabetes mellitus without mention of complication, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.03');

-- Diabetes with ketoacidosis, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.11');

-- Diabetes with ketoacidosis, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.13');

-- Diabetes with hyperosmolarity, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.21');

-- Diabetes with hyperosmolarity, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.23');

-- Diabetes with other coma, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.31');

-- Diabetes with other coma, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.33');

-- Diabetes with renal manifestations, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.41');

-- Diabetes with renal manifestations, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.43');

-- Diabetes with ophthalmic manifestations, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.51');

-- Diabetes with ophthalmic manifestations, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.53');

-- Diabetes with neurological manifestations, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.61');

-- Diabetes with neurological manifestations, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.63');

-- Diabetes with peripheral circulatory disorders, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.71');

-- Diabetes with peripheral circulatory disorders, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.73');

-- Diabetes with other specified manifestations, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.81');

-- Diabetes with other specified manifestations, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.83');

-- Diabetes with unspecified complication, type I [juvenile type], not stated as uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.91');

-- Diabetes with unspecified complication, type I [juvenile type], uncontrolled (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;diabetes;type_1_diabetes_mellitus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '250.93');

-- Autism-spectrum disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;autism_spectrum;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F84%');

-- Autism-spectrum disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;autism_spectrum;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '299%');

-- Delusional disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F22%');

-- Delusional disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;delusion_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.8');

-- Intellectual disabilities (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F70%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F71%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F72%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F73%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F78%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F79%');

-- Intellectual disabilities (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '317%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '318%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;intellectual_disability;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '319%');

-- Organic disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F01%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F02%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F03%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F04%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F06%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F07%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F09%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F48.2');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'G30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F10.2[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F10.9[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F13.2[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F13.9[67]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F19.9[67]');

-- Organic disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '290%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '294%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '293.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '310%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '291.[12]');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;organic_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '292.8[23]');

-- Parkinson's disease (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;parkinson_disease;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'G20');

-- Parkinson's disease (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;parkinson_disease;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '332.0');

-- schizoaffective disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;schizoaffective;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F25%');

-- schizoaffective disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;schizoaffective;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '295.7%');

-- Schizophrenia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;schizophrenia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F20%');

-- Schizophrenia (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;exclusion;mental;schizophrenia;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '295.[012345689]%');

-- bipolar disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F31%');

-- bipolar disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.0%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.4%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.5%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.6%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;inclusion;bipolar_spectrum;bipolar_disorder;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.8%');

-- Acute psychoses (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F23%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F24%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F28%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F29%');

-- Acute psychoses (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '297.9');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;acute_psychosis;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '298.[013489]');

-- Alcohol dependence or abuse (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.2%');

-- Alcohol dependence or abuse (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '303%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;alcohol_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '305.0%');

-- Alcohol-induced mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.2%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F10.9%');

-- Alcohol-induced mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;alcohol_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '291%');

-- Anxiety and dissociative disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F40%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F41%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F42%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F44%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F48%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F68%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F55%');

-- Anxiety and dissociative disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '300.[0123569]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_dissoc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.2');

-- NO_NAME (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.9');

-- NO_NAME (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;anxiety_other;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.09');

-- Childhood and adolescence disorders (behavioral, emotional) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F92%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F93%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F94%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F98%');

-- Childhood and adolescence disorders (behavioral, emotional) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '313%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;child_adolesc;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '307.[67]');

-- Conduct disorders and impulse disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F91%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F63%');

-- Conduct disorders and impulse disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '312%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;conduct_impuls;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '313.81');

-- Delirium (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F05');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F10.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F11.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F12.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F13.931');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F14.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F15.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F16.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F18.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.121');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.221');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.231');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.921');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F19.931');

-- Delirium (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '293.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '291.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;delirium;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '292.81');

-- Specific delays in development: (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F80%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F81%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F82%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F88%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F89%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R48.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'H93.25');

-- Specific delays in development: (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;development_delay;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '315%');

-- Drug dependence or abuse (except alcohol) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F1[1-9].[12]%');

-- Drug dependence or abuse (except alcohol) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '304%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;drug_depend_abus;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '305.[1-9]%');

-- Drug-induced mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F1[1-9].[129]%');

-- Drug-induced mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;drug_induced;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '292%');

-- Eating disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F50%');

-- Eating disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '307.1');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;eating;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.5%');

-- Generalized anxiety disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;gad;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.1');

-- Generalized anxiety disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;gad;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.02');

-- Hyperkinetic syndrome of childhood: (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;hyperkinet;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F90%');

-- Hyperkinetic syndrome of childhood: (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;hyperkinet;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '314%');

-- Motor disorders (tics) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;motor;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F95%');

-- Motor disorders (tics) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;motor;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '307.[023]%');

-- Opioid related disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;opioid_abuse_depend;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F11%');

-- Opioid abuse and opioid dependence (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;opioid_abuse_depend;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '305.5%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;opioid_abuse_depend;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '304.0%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;opioid_abuse_depend;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '304.7%');

-- Other and unspecified mood disorder (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F34%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F39');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F32.81');

-- Other and unspecified mood disorder (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '296.9%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '301.1%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;other_mood;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '625.4');

-- Panic disorder without agoraphobia (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;panic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F41.0');

-- Panic disorder without agoraphobia (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;panic;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.01');

-- Personality disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F21%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F60%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F68%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F69%');

-- Personality disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;personality;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '301.[023456789]%');

-- Pregnancy and delivery related mental disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'O90.6');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'O99.34%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F53');

-- Pregnancy and delivery related mental disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;pregnancy_delivery;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '648.4%');

-- Psychosomatic disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F42.4');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F45%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F52.5');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F54%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F59%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'G44.209');

-- Psychosomatic disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '306%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '300.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '300.8%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '316%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;psychosomat;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.8%');

-- Intentional self-harm (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X7[1-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X8[0-3]%');

-- Intentional self-harm (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E95[0-9]%');

-- Intentional self-harm (SNOMED) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4244894);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(439235);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;self_harm;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4303690);

-- Sexual and gender identity disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F52%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F64%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F65%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F66%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'Z87.890');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R37');

-- Sexual and gender identity disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sexual;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '302%');

-- Sleep disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F51%');

-- Sleep disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '307.4%');

-- Sleep disorders (SNOMED) (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;sleep;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(435524);

-- Acute reaction to stress and adjustment disorders (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F43%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'R45.7');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F93.0');

-- Acute reaction to stress and adjustment disorders (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '308%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mental_comorb;stress;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '309%');

-- bipolar I, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F30.[12349]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'F31.[0-7]%');

-- bipolar I, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[01456]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_i;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.7');

-- bipolar II, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_ii;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.81');

-- bipolar II, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_ii;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '296.89');

-- bipolar NOS, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F30.8');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.89');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'F31.9');

-- bipolar NOS, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;bipolar_disorder;bipolar_nos;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.8[0-2]');

-- MDD, no remission codes (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F32%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F33%');

-- MDD, no remission codes (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '296.[23]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '298.0');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;mmi;bipolar_spectrum;major_depressive;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '311');

-- all psychiatric diagnoses (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'F%');

-- all psychiatric diagnoses (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '29%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '30%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;psychiatric_diagnoses;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '31%');

-- pregnancy with abortive outcome (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;abortion;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(40539858);

-- cardiac arrhythmia (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;arrhytmia;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(44784217);

-- autoimmune (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;autoimmune;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(434621);

-- cardiovascular (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;cardiovascular;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(134057);

-- central nervous system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;central_nerv_system;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(376106);

-- chronic infectious disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;chronic_infect_disease;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4073287);

-- chronic pain (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;chronic_pain;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(436096);

-- patient currently pregnant (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;currently_pregnant;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4299535);

-- delivery and perinatal care (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;delivery_perinatal_care;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4038495);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;delivery_perinatal_care;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4237498);

-- demyelinating disease of CNS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;demyelinat;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(375801);

-- dermatological (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;dermatological;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4317258);

-- disorder of digestive tract (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;digestive;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4309188);

-- endocrinopathy (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;endocrinopathy;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(31821);

-- Disorder of glucose metabolism/regulation (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;glucose_metabolism_regulation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4130526);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;glucose_metabolism_regulation;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4130161);

-- HIV/AIDS (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;hiv_aids;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4241530);

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;hiv_aids;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4221489);

-- hypertension (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;hypertension;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(316866);

-- injury (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'S%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'T%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'V%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'W%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X[0-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'X[9][2-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'Y[2-3]%');

-- injury (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '8[0-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '9[0-8]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '99[0-5]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E[0-8]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E9[0-4]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;injury;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'E9[6-9]%');

-- kidney (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;kidney;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(198124);

-- liver disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;liver;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(194984);

-- metabolic (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;metabolic;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(436670);

-- Toxic effect of metals (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;metal;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code LIKE 'T56%');

-- Toxic effect of other metals (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;metal;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE '985%');

-- medication-induced movement disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;movement_medicat;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4186461);

-- musculoskeletal (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;musculoskeletal;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4244662);

-- neoplasm and/or hamartoma (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;neoplasm_hamartoma;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4266186);

-- nervous system (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;nerv_system;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(376337);

-- disorder of neuromuscular transmission (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;neuromusc_transmission;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4103776);

-- obesity, BMI 30 and more in adults (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'E66.[01289]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'Z68.[34]%');

-- obesity, BMI 30 and more in adults (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '278.00');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '278.01');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '278.03');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO 'V85.[34]%');

-- obesity     - SNOMED disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;obesity;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(433736);

-- overweight, BMI 25-29.9 in adults (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code = 'E66.3');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'Z68.2[5-9]%');

-- overweight, BMI 25-29.9 in adults (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code = '278.02');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code LIKE 'V85.2%');

-- overweight, BMI 25-29.9 in adults - finding from SNOMED (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;obesity_and_overweight;overweight;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(437525);

-- post-streptococcal disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;post_streptococ;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4051204);

-- pulmonary (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;pulmon;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(257907);

-- rheumatism (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;rheumatism;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(138845);

-- seizure disorder (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;seizure;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4029498);

-- sexually transmitted infectious disease (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;sexually_transmitted;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(440647);

-- thyroidism (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;thyroid;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(141253);

-- Toxic effects of substances chiefly nonmedicinal as to source (excluding alcohol) (ICD-10-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;toxic_effect;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'T5[2-9]%');

INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;toxic_effect;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD10%')
        AND (concept_code SIMILAR TO 'T6[0-5]%');

-- Toxic effects of substances chiefly nonmedicinal as to source (excluding alcohol) (ICD-9-CM)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;toxic_effect;',
            concept_id, concept_name
       FROM :cdm_schema.concept
      WHERE (vocabulary_id LIKE 'ICD9%')
        AND (concept_code SIMILAR TO '98.[1-9]%');

-- tuberculosis (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;tuberculosis;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(434557);

-- Urinary incontinence of non-organic origin (OMOP)
INSERT INTO :work_schema.concept_sets_of_interest
     SELECT 'Diagnoses;somatic_comorb;urinary_incontinence;',
            concept_id, concept_name
       FROM :work_schema.codex_concept_descendants(4172646);
